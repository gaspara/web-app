<?php
# default database
if (!isset($_REQUEST['project'])) {
    require_once("/etc/openbiomaps/system_vars.php.inc");
    if (defined('DEFAULT_PROJECT'))
        $_REQUEST['project'] = constant('DEFAULT_PROJECT');
    else
        $_REQUEST['project'] = 'sablon';
    define('FAKE_PROJECT',1);
}
define('PROJECT_DIR',$_REQUEST['project']);
define('CALL','/var/www/html/biomaps/root-site');
putenv('OB_LIB_DIR='.CALL.'/projects/'.PROJECT_DIR.'/includes/');
putenv('PROJECT_DIR='.CALL.'/projects/'.PROJECT_DIR.'/');
# authentication
require_once(CALL."/projects/".PROJECT_DIR."/oauth/resource.php");
# pds service
require_once(CALL."/projects/".PROJECT_DIR."/pds/service.php");
?>
