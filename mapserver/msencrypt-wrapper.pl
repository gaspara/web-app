#!/usr/bin/perl -wT
  
use warnings;
use strict;
use CGI::Fast;
my $code = "";
my $passwd = "";

while (my $q = CGI::Fast->new) {

    $passwd = $q->param("passwd");
    $code = `msencrypt -key /var/lib/openbiomaps/maps/access.key $passwd`;
               
    print $q->header( "text/plain" ),
               "$code";
}
