<?php
/* This is an AJAX call responder
 * functions.js
 *      LoadResults
 *      WFSGet depreceted
 *
 * */
 
require_once(getenv('OB_LIB_DIR').'db_funcs.php');

session_start();
if (!defined('STYLE_PATH')) {
    $style_def = constant('STYLE');
    $style_name = $style_def['template'];
    define('STYLE_PATH',"/styles/app/".$style_name);
}

$method = $_POST['method'];
// rolling output
if(isset($_SESSION['show_results']['type'])) {
    $method = $_SESSION['show_results']['type'];
} else {
    $_SESSION['show_results']['type'] = $method;
}

include_once(getenv('OB_LIB_DIR').'results_builder.php');
$r = new results_builder(['method'=>$method]);

if( $r->results_query() ) {
    if ($r->rq['results_count']) {
        $r->printOut();
    } else {
        echo common_message('error',str_no_results_found);
    }
    exit;
}
echo common_message('error',$r->error);
unset($_SESSION['roll_array']);

?>
