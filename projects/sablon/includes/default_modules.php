<?php

abstract class module {
    public $params;
    public $ajax_deps = [];
    public $ajax_echo_common_message = true;
    #public $main_table;
 
    abstract protected function moduleName();

    public function print_js($request) {

        $module = $this->moduleName();
        return (file_exists(getenv('OB_LIB_DIR')."modules/$module.js")) ? file_get_contents(getenv('OB_LIB_DIR') . "modules/$module.js") : '';

    }

    public function print_css($request) {

        $module = $this->moduleName();
        $mdule_header = "/* $module styles */\n";
        return (file_exists(getenv('OB_LIB_DIR')."modules/$module.css")) ? $mdule_header . file_get_contents(getenv('OB_LIB_DIR') . "modules/$module.css") : '';

    }

    public function module_params_interface($params, $pa) {
        return false;
    }
    
    public function init($params, $pa) {
        return;
    }
    
    public function destroy($params, $pa) {
        return true;
    }
    
    public function displayError() {
        log_action($this->error,__FILE__,__LINE__, $this->moduleName());
    }

    public function translations() {
        if (isset($this->strings)) {
            $translations = [];
            foreach ($this->strings as $string) {
                $translations[$string] = t("$string");
            }
            return $translations;
        }
        return [];
    }
    
    public function ajax($params,$request) {
        foreach ($this->ajax_deps as $filename) {
            $lib = getenv('OB_LIB_DIR') . "$filename.php";
            if (file_exists($lib)) {
                require_once($lib);
            }
        }
        
        if (!isset($request['action'])) {
            echo common_message('error', 'action missing');
            return;
        }
        $retval = "";
        $action = $request['action'];
        if (method_exists($this, $action)) {
            $result = $this->$action($params, $request);
            if ($this->error) {
                echo common_message('error', $this->error);
                return;
            }
            if ($this->ajax_echo_common_message) {
                echo common_message('ok', $result);
            }
            else {
                echo json_encode($result);
            }
            if ($result === 'clean_ob') {
                ob_clean();
            }
        }
        else {
            echo common_message('error', "invalid module action: {$request['m']}::{$request['action']}");
        }
        return;
    
    }
    
    public function external_dependencies($params, $request) {
      if (isset($this->external_dependencies)) {
        return $this->external_dependencies;
      }
    }
    
    public function get_params() {
        //return $this->params;
        return $this->split_params($this->params);
    }

    public function split_params($params,$type='simple') {

        $p = array('module params not processed');
        if ($type=='simple') {
            if (is_array($params))      // new_params
                $p = $params;
            else {
                // split simple list
                if (preg_match("/^JSON:(.*)$/",$params,$json)) {
                    $p = json_decode(base64_decode($json[1]),true);
                } else {
                    $p = explode(';',$params);
                }
            }
        } elseif ($type == 'pmi') {

            if (is_array($params))      // new_params
                $p = $params;
            else {
                if (preg_match("/^JSON:(.*)$/",$params,$json)) {
                    $p = json_decode(base64_decode($json[1]),true);
                } else {
                    # Array("modszer:shunt","hely:geom");
                    $p = array();
                    $pl = preg_split('/;/',$params);
                    foreach ($pl as $pm) {
                        $c = preg_split('/[:=]/',$pm);
                        if ($c[0] == '') continue;

                        $p[$c[0]] = $c[1];
                    }
                }
            }
        }
        
        //debug($this->moduleName(),__FILE__,__LINE__);
        //debug($p,__FILE__,__LINE__);

        return $p;
    }
}

#require_once('table_modules.php');
?>
