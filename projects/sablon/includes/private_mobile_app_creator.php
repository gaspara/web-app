# App start screen
# ?
android/app/src/main/res/drawable/screen.jpg
android/app/src/main/res/drawable/icon.png

# App background screen
# ??
src/assets/login_background.jpg

# Update sources
#
src/Constants.js
src/api/Client.js
src/navigators/MainNavigator/index.js
src/actions/measurement.js
src/screens/SettingsScreen/index.js
src/screens/HomeScreen/index.js

# Add custom pages
#
mkdir src/screens/CustomDescriptionScreen
index.js  styles.js

mkdir src/screen/CustomReloadPage/
index.js  styles.js
