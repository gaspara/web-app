<?php

echo "<div class='infotitle'>".wikilink('admin_pages.html#project-description',t('str_documentation'))."</div>";

$cmd = sprintf("SELECT * FROM project_descriptions WHERE projecttable= '%s' AND language='%s'",
    PROJECTTABLE,
    $_SESSION['LANG']);

$res = pg_query($BID,$cmd);
if (pg_num_rows($res)) {
    $row = pg_fetch_assoc($res);
}

echo "<form class='pure-form pure-form-stacked' style='width:60em'>";
echo "<fieldset>";
echo "<div class='pure-control-group'>";

echo '<label for="short_desc">'.str_language.":</label>
    <input class='pure_input pure-u-1-1' id='language' maxlength='2' readonly='readonly' required='required' value=".$_SESSION['LANG'].">";

echo '<label for="short_desc">'.str_short_project_description.":</label>
    <input class='pure_input pure-u-1-1' id='short_desc' maxlength='64' required='required' value='".$row['short']."'>";

echo '<label for="long_desc">'.str_long_project_description.":</label> "."<textarea class='pure_input pure-u-1' id='long_desc'>".$row['long']."</textarea>";

echo "<button id='update_project_description' class='pure-button button-warning'><i class='fa fa-lg fa-floppy-o fa-lg'></i></button>";
echo "</div>";
echo "<div class='pure-controls'></div>
    </fieldset></form>";
?>
