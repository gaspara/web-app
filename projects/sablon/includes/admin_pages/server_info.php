<?php
    echo "<div class='infotitle'>".wikilink('admin_pages.html#server-info',t('str_documentation'))."</div>";

    $files = array();

    $root_dir = OB_ROOT.'projects/'.PROJECTTABLE;
    $supervisor_time_log_file = $root_dir.'/supervisor_time_log_file';
    $supervisor_last_update = 0;
    if (is_file($supervisor_time_log_file))
        $supervisor_last_update = filemtime($supervisor_time_log_file);

    if (is_dir($root_dir.'/includes'))
    if ($handle = opendir($root_dir.'/includes')) {
        while (false !== ($file = readdir($handle))) {
            if (is_file($file)) {
                $modified = filemtime($file);
                if ($supervisor_last_update & $modified > $supervisor_last_update)
                    $files[$modified] = $file;
            }
        }
        closedir($handle);
    }
    if (is_dir($root_dir.'/includes/modules'))
    if ($handle = opendir($root_dir.'/includes/modules')) {
        while (false !== ($file = readdir($handle))) {
            if (is_file($file)) {
                $modified = filemtime($file);
                if ($supervisor_last_update & $modified > $supervisor_last_update)
                    $files[$modified] = $file;
            }
        }
        closedir($handle);
    }
    if (is_dir($root_dir.'/js'))
    if ($handle = opendir($root_dir.'/js')) {
        while (false !== ($file = readdir($handle))) {
            if (is_file($file)) {
                $modified = filemtime($file);
                if ($supervisor_last_update & $modified > $supervisor_last_update)
                    $files[$modified] = $file;
            }
        }
        closedir($handle);
    }
    if (is_dir($root_dir))
    if ($handle = opendir($root_dir)) {
        while (false !== ($file = readdir($handle))) {
            if (is_file($file)) {
                $modified = filemtime($file);
                if ($supervisor_last_update & $modified > $supervisor_last_update)
                    $files[$modified] = $file;
            }
        }
        closedir($handle);
    }
    if (is_dir($root_dir.'/css'))
    if ($handle = opendir($root_dir.'/css')) {
        while (false !== ($file = readdir($handle))) {
            if (is_file($file)) {
                $modified = filemtime($file);
                if ($supervisor_last_update & $modified > $supervisor_last_update)
                    $files[$modified] = $file;
            }
        }
        closedir($handle);
    }


    krsort($files);



    echo "<h3>Last updated files</h3>".date("F d, Y. H:i:s",$supervisor_last_update).'<br>';
    foreach ($files as $file) {
        echo $file."<br>";
    }
    $filename = getenv('PROJECT_DIR')."app.version";
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));
    fclose($handle);

    echo "<h3>Application version</h3> $contents";


    $f =  getenv('PROJECT_DIR').'local';
    $io = popen ( '/usr/bin/du -sk ' . $f, 'r' );
    $size = fgets ( $io, 4096);
    $size = substr ( $size, 0, strpos ( $size, "\t" ) );
    pclose ( $io );
    echo '<h3>Disk usage of project files (attachments, uploads and other non-system files)</h3>' . round($size / 1024) . 'Mb';


    $ncpu = 1;
    if (is_file('/proc/cpuinfo')) {
        $cpuinfo = file_get_contents('/proc/cpuinfo');
        preg_match_all('/^processor/m', $cpuinfo, $matches);
        $ncpu = count($matches[0]);
    }

    $load = sys_getloadavg();
    echo '<h3>Server load (1min 5min 15min)</h3>' . round($load[0]/$ncpu,2) .' / '. round($load[1]/$ncpu,2) .' / '. round($load[2]/$ncpu,2);

    echo '<h3>Server free memory</h3>'. round(memory_get_usage() / 1024) . 'Mb'; 

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $auth = genhash();
    echo sprintf('<h3>Project settings</h3> <a href="%s://%s/supervisor.php">Supervisor</a>',$protocol,URL);
?>
