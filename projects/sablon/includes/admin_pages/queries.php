<?php
    echo "<div class='infotitle'>".wikilink('admin_pages.html#sql-query-settings',t('str_documentation'))."</div>";

    // layers in the mapfile
    $map = getenv('PROJECT_DIR').'private/private.map';
    ob_start();
    passthru("grep LAYER $map -A 1|grep -i NAME");
    $var = ob_get_contents();
    ob_end_clean();
    $var = preg_replace('/NAME/i','',$var);
    $var = preg_replace('/\'/i','',$var);
    $var = preg_replace('/"/i','',$var);

    $map_layers = array_filter(preg_split('/\n|\r/',$var));

    // echo mapserver layers
    echo '<b>'.t(str_mapserver_layer)."</b><br>The following layers are available in your mapfile:";
    echo "<p style='padding-left:30px'>".implode($map_layers,'<br>').'</p>';

    // echo openlayers defined leyers
    /*$cmd = sprintf("SELECT layer_name FROM project_layers WHERE project_table='%s' ORDER BY 1",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $clayers = array();
    while ($row = pg_fetch_assoc($res)) {
        $clayers[] = $row['layer_name'];
    }

    if (count($clayers)) {
        echo "<p style='text-indent:30px'>";
        echo implode($clayers," - ");
        echo "</p>";
    } */
    //else
    //    echo '<b>'.t(str_webmap_layer).":</b><br>The following <i>".str_webmap_layer."</i> are available to  connect with <i>".str_mapserver_layer."</i>:";

    $tbl = new createTable();
    $tbl->def(['tid'=>'mytable','tclass'=>'admintable pure-form']);

    $layer_types = array('public','private','special');
    $layer_enabled = array('f'=>'disabled','t'=>'enabled');
    $cname_prefix = 'layer_data_';

    $cmd = "SELECT * FROM project_queries WHERE project_table='".PROJECTTABLE."' ORDER BY enabled,layer_cname";
    $res = pg_query($BID,$cmd);
    while ($row = pg_fetch_assoc($res)) {

        $cname_prefix = 'layer_data_';
        $m = array();
        if (preg_match('/^layer_data_(.+)/',$row['layer_cname'],$m)) {
            $cname = $m[1];
        } else
            $cname = $row['layer_cname'];


        $syne = '';
        if(preg_match('/WRONG/',$row['layer_query']) or $row['layer_query']=='') {
            $syne = 'orange';
        }
        $r = array();
        //array_push($r,sprintf("<select id='qnam-{$row['id']}'>%s</select>",selected_option($map_layers,$row['layer_name'])));
        array_push($r,sprintf("<textarea rows='8' cols=100 id='qquery-{$row['id']}' style='background-color:$syne'>%s</textarea><br>",$row['layer_query']));
        array_push($r,sprintf("<select id='qena-{$row['id']}' name='enabled'>".selected_option(array(str_true.'::t',str_false.'::f'),$row['enabled'])."</select>"));
        array_push($r,"<select id='qtype-{$row['id']}' style='width:120px' name='tipus'>".selected_option(array('','query & base::query','base'),$row['layer_type'])."</select>");
        array_push($r,sprintf("<select id='qrst-{$row['id']}' name='enabled'>".selected_option(array('Public::0','Private::1','Special::2'),$row['rst'])."</select>"));
        //array_push($r,sprintf("<select id='qgt-{$row['id']}'>".selected_option(array('','POINT','LINESTRING','POLYGON'),$row['geom_type'])."</select>"));
        array_push($r,sprintf("$cname_prefix<input id='qcname-{$row['id']}' value='$cname'>"));
        array_push($r,sprintf("<button name='qopld' class='button-warning button-xlarge pure-button q_update' id='qopld_".$row['id']."'>".str_modifyit."</button>"));
        //array_push($r,sprintf("<button class='button-warning button-xlarge pure-button'>".str_save."</button>"));
        $tbl->addRows($r);
    }

    $wms = "<span style='color:orange;font-family:sans-serif'>SELECT</span> obm_id, <span style='color:tomato'>%grid_geometry% AS</span>  obm_geometry <span style='color:deepskyblue'>%selected%</span><br>
<span style='color:orange'>FROM</span> <span style='color:deepskyblue'>%F%".PROJECTTABLE." ".substr(PROJECTTABLE,0,1)."%F%</span><br>
&nbsp; &nbsp; <span style='color:deepskyblue'>%uploading_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%rules_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%taxon_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%grid_join%</span><br>
&nbsp; &nbsp; <span style='color:tomato'>%search_join%</span><br>
&nbsp; &nbsp; <span style='color:deepskyblue'>%morefilter%</span><br>
<span style='color:orange'>WHERE</span> <span style='color:deepskyblue'>%geometry_type%</span> <span style='color:deepskyblue'>%envelope%</span> <span style='color:deepskyblue'>%qstr%</span>";

    $wmsq = "<br> <b>Example <i>query type</i> query</b><p style='padding:0px 0px 10px 10px'>$wms</p>";

    /* if ($_SESSION['st_col']['GEOM_C']=='') {
        $wms = '';
        $wmsq = '';
        echo "<div style='color:red'>SET YOUR COLUMNS FIRST! - and reload page</div>";
    } */

    echo $wmsq;

    $tbl->addRows(array(
        "<textarea id='qquery-new' style='width:100%;height:90px;background-color:#eFeFeD' placeholder='copy the example from above'></textarea>",
        "<select id='qena-new'><option>TRUE</option><option>FALSE</option></select>",
        "<select id='qtype-new'><option value='query'>query & base</option><option>base</option></select>",
        "<select id='qrst-new'><option value=0>public</option><option value=1>private</option><option value=2>special</option></select>",
        "$cname_prefix<input id='qcname-new' value='points'>",
        "<input type='button' name='aqd' class='button-success button-xlarge pure-button q_update' id='aqd_new' value='".str_add."'>"
    ));
        //sprintf("<select id='qnam-new'>%s</select>",selected_option($map_layers,' ')),
        //"<select id='qgt-new'><option></option><option>POINT</option><option>LINESTRING</option><option>POLYGON</option></select>",
    //layer_name 	layer_query 	enabled 	layer_type 	rst 	layer_cname
    $tbl->addHeader(array("SQL ".str_query,t(str_enabled),t(str_type),t(str_access),t(str_sql_reference),t(str_actions)));
    echo $tbl->printOut();

?>
