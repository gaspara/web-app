<?php 
/**
  * 
  * taxon_id = 0 -> special case - in case of hierarchical taxon table used for retrieving the 1st level taxons
  *
  */
/**
 * This class contains the methods related to the taxon table and of Taxon
 * collections
 */
class TaxonManager {
    
    public static function taxon_lang_selector_widget($selected) {
        $table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE;
        $st_col = st_col($table, 'array');
        if (!isset($_SESSION['Tid']) || !count($st_col['NATIONAL_C'])) {
            return "";
        }
        
        $User = new User($_SESSION['Tid']);
        $user_lang = (isset($User->options->language)) ? $User->options->language : false;
        
        $languages = [
            $st_col['SPECIES_C_SCI'] => str_sci_name,
        ];
        if ($user_lang) {
            $dbcolist = dbcolist('array');
            $col = $st_col['NATIONAL_C'][$user_lang];
            $languages[$col] = $dbcolist[$col];
        }
        
        if (count($languages) == 1) {
            $selected = array_values($languages)[0];
        }
        
        $out = "<select class='taxon-lang-selector'>";
        if ($selected == "") {
            $out .= "<option value='' selected disabled>" . t('str_please_choose') . "</option>";
        }
        foreach ($languages as $col=>$label) {
            $sel = ($col == $selected) ? "selected" : "" ;
            $label = ucfirst($label);
            $out .= "<option value='$col' $sel > $label </option>";
        }
        $out .= "</select>";
        return $out;
    }
    
    public static function taxon_columns() {
        $table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE;
        $st_col = st_col($table, 'array');
        $languages = [
            $st_col['SPECIES_C_SCI'] => $st_col['SPECIES_C_SCI']
        ];
        if ($st_col['SPECIES_C'] !== $st_col['SPECIES_C_SCI']) {
            $languages[$st_col['SPECIES_C']] = $st_col['SPECIES_C'];
        }
        foreach ($st_col['ALTERN_C'] as $col) {
            $languages[$col] = $col;
        }
        
        return array_unique($languages);
    }
    
    // to be renamed together with the same method of list_manager
    public static function get_tables_subjects() {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT '%s' as data_table, lang as subject, string_agg(DISTINCT status, ',') as status FROM %s_taxon GROUP BY lang;", PROJECTTABLE, PROJECTTABLE);
        if ( !$res = pg_query($ID, $cmd)) {
            log_action(__CLASS__ . "::" . __FUNCTION__ .' query error', __FILE__, __LINE__);
            return false;
        }
        if (pg_num_rows($res) === 0) {
            return [];
        }
        return pg_fetch_all($res);
    }
    
    public static function get_list($request) {
        global $ID;
        
        $limit = (isset($request['limit']) && is_numeric($request['limit'])) ? "LIMIT {$request['limit']}" : "";
        $offset = (isset($request['offset']) && is_numeric($request['offset'])) ? "OFFSET {$request['offset']}" : "";
        $orderby = "ORDER BY taxon_id";
        if (isset($request['orderby']) && $request['orderby']) {
            if ($request['orderby'] == 'newestFirst') {
                $request['orderby'] = ['status', 'wid'];
                $request['order'] = ['DESC', 'DESC'];
            }
            $orderby = prepareOrder($request['orderby'], $request['order'] ?? NULL);
        }
        
        $where = self::prepareWhere($request);
        
        $dist = "1 as dist";
        if ($request['term']) {
            if ($request['search_algoritm'] === 'like') {
                $dist = sprintf('CASE WHEN meta ILIKE %1$s OR meta ILIKE %3$s THEN 1 WHEN meta ILIKE %2$s THEN 2 ELSE 3 END AS dist', 
                    quote("{$request['term']}%"), quote("% {$request['term']}%"), quote("%/ {$request['term']}%")
                );
            }
            else {
                $dist = sprintf("1 - (lower(replace(word, ' ', ''))<->%s) AS dist", quote($request['term']));
            }
        }
        
        $taxon_table = PROJECTTABLE . "_taxon";
        if ($request['bilingual_search'] && $_SESSION['st_col']['SPECIES_C'] !== $_SESSION['st_col']['SPECIES_C_SCI']) {
            $taxon_table = sprintf('temporary_tables.%s_taxon_%s_%s', PROJECTTABLE, $_SESSION['st_col']['SPECIES_C'], $_SESSION['st_col']['SPECIES_C_SCI']);
        } 
        
        $cmd = sprintf("SELECT *, %s FROM %s %s %s %s %s;", $dist, $taxon_table, $where, $orderby, $limit, $offset);
        if (! $res = pg_query($ID, $cmd) ) {
            log_action(pg_last_error($ID));
            return [];
        }
        if (pg_num_rows($res) === 0) {
            return [];
        }

        return pg_fetch_all($res);
        
    }
    
    public static function total_count($request) {
        global $ID;

        $cmd = sprintf("SELECT  count(*) c FROM %s_taxon %s;", PROJECTTABLE, self::prepareWhere($request));
        if (! $res = pg_query($ID, $cmd) ) {
            log_action(pg_last_error($ID));
            return;
        }
        $asdf = pg_fetch_assoc($res);
        return $asdf['c'];
    }
    
    public function get_list_and_count($request) {
        return [
            'list' => self::get_list($request),
            'count' => self::total_count($request)
        ];
    }
    
    private static function prepareWhere($request) {
        $where = [];
        if ($request['colname'] && (!$request['bilingual_search'] || $_SESSION['st_col']['SPECIES_C'] === $_SESSION['st_col']['SPECIES_C_SCI'])) {
            $where[] = prepareFilter('lang', $request['colname']);
        }
        
        if ($request['excluded']) {
            $where[] = prepareFilter('taxon_id', $request['excluded'], 'NOT IN');
        }
        
        if ($request['status']) {
            $where[] = prepareFilter('status', $request['status']);
        }
        
        if ($request['term']) {
            $term = strtolower(remove_accents($request['term']));
            if ($request['search_algoritm'] === 'like') {
                $where[] = prepareFilter('unaccent(lower(word))', "%{$term}%", 'LIKE');
            }
            else {
                $where[] = prepareFilter("lower(replace(word, ' ', ''))<->".quote($term), 0.9, "<");
            }
        }
        
        if ($request['id']) {
            $where[] = prepareFilter('taxon_id', $request['id']);
        }
        
        if ($request['preFilter']) {
            $where[] = prepareFilter(
                $request['preFilter']['preFilterColumn'],
                $request['preFilter']['preFilterValue'],
                $request['preFilter']['preFilterRelation'] ?? null
            );
        }
        
        
        return (count($where)) ? "WHERE " . implode(" AND ", $where) : "";
    }
    
    /* not used yet */
    public function update_parent() {
        if (!is_numeric($request['taxon_id']) && $request['taxon_id'] !== "") {
            return common_message('error','Invalid taxon_id!');
        }
        if (!is_numeric($request['parentId']) && $request['parentId'] !== "") {
            return common_message('error','Invalid parentId!');
        }
        $t = new xTaxon((int)$request['taxon_id']);
        $t->parentId = (int)$request['parentId'];
        return ($t->update_parent()) 
          ? common_message('ok', 'ok')
          : common_message('error', 'Parent update failed!');
    }
}
 
/**
 * Class Taxon
 */
class Taxon extends Entity
{
    protected $tableSchema = 'public';
    protected $tableName = PROJECTTABLE . '_taxons';
    
    public $id;
    public $project_table;
    public $reference;
    public $comment;
    public $datum;
    public $access;
    public $user_id;
    public $status;
    public $sessionid;
    public $sum;
    public $mimetype;
    public $data_table;
    public $exif;
    public $slideshow;
    
    function __construct() {
        parent::__construct();
    }
    
} 

class xTaxon {
    public $taxon_id = null;
    public $parentId = null;
    public $ord = null;
    public $words = [];
    public $warnings = [];
    public $errors = [];
    public $rules = [];
    
    private $states = ['accepted','synonym','misspelled','undefined'];
    private $taxonMetaEnabled = false;
    
    function __construct($taxon_id) {
        global $ID;

        debug(__CLASS__, __FILE__, __LINE__);
        
        $default_table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
        $st_col = st_col($default_table, 'array');
        
        $taxon_table = PROJECTTABLE . "_taxon";
        
        $modules = new modules($default_table);
        $x_modules = new x_modules();
        $tm_cols = $tm_join = '';
        
        // temporarily disabled, because its causing errors:
        $this->taxonMetaEnabled = false; //($x_modules->is_enabled('taxon_meta'));
        
        // include taxonmeta module
        if ($this->taxonMetaEnabled) {
          $tm_cols = $x_modules->_include('taxon_meta', 'meta_columns');
          $tm_join = $x_modules->_include('taxon_meta', 'table_join', 'tx');
          $meta_columns = $x_modules->_include('taxon_meta', 'meta_columns', 'array');
        }
        
        if ($taxon_id === 0) {
          $this->taxon_id = 0;
          return;
        }
        elseif (is_numeric($taxon_id)) {
            $cmd = sprintf("SELECT tx.wid, tx.taxon_id, tx.word, tx.lang, tx.status, tx.taxon_db %s FROM %s tx %s WHERE tx.taxon_id = %d;", $tm_cols, $taxon_table, $tm_join, (int)$taxon_id);
        }
        elseif ($taxon_id === 'latest') {
            $cmd = sprintf("SELECT tx.wid, tx.taxon_id, tx.word, tx.lang, tx.status, tx.taxon_db %2\$s FROM %1\$s tx %3\$s WHERE tx.taxon_id IN (
              SELECT taxon_id FROM %1\$s WHERE status = 'undefined' ORDER BY taxon_db DESC LIMIT 1
            )", $taxon_table, $tm_cols, $tm_join);
        }
        else {
            $cmd = sprintf("SELECT tx.wid, tx.taxon_id, tx.word, tx.lang, tx.status, tx.taxon_db %1\$s FROM %2\$s tx %3\$s WHERE tx.taxon_id IN (SELECT taxon_id FROM %2\$s WHERE word = %s);", $tm_cols, $taxon_table, $tm_join, quote($taxon_id));
        }
        
        if (!$res = pg_query($ID, $cmd)) {
            log_action('Taxon request query error', __FILE__, __LINE__);
            log_action($cmd,__FILE__,__LINE__);
            return;
        }
        
        if (pg_num_rows($res) === 0) {
          $this->errors[] = 'Taxon does not exist';
          return;
        }
        $taxon_records = pg_fetch_all($res);
        
        // taxon id 
        $this->taxon_id = $taxon_records[0]['taxon_id'];
        
        // metadata from taxonmeta table
        if ($this->taxonMetaEnabled) {
          foreach ($meta_columns as $col) {
            if ($col === 'taxon_id') {
              continue;
            }
          }
        }
        
        foreach ($taxon_records as $t) {
            $t['status'] = (in_array($t['status'], $this->states)) ? $t['status'] : 'undefined';
            $this->words[] = new Word($t);    
        }
        
        // sorting the words array, so the scietific name be the first
        $langs = array_map(function($w) {return $w->lang; }, $this->words);
        array_multisort($langs, SORT_ASC, $this->words);
        $sci_idx = array_search($st_col['SPECIES_C_SCI'], $langs);
        if ($sci_idx !== false) {
            $sci_word = $this->words[$sci_idx];
            unset($this->words[$sci_idx]);
            array_unshift($this->words, $sci_word);
        }        
    }
    
    public function getWord($status, $lang) {
        $word = array_filter($this->words, function($w) use ($status, $lang) {
            return ($w->lang === $lang && $w->status === $status);
        });
        return (count($word)) ? $word[0] : false;
    }
    
    public function words() {
        return $this->words;
    }
    
    public function delete_taxon() {
        array_map(function($w) {
          return $w->delete();
        }, $this->words);  
    }
    
    public function update_parent() {
      global $ID;
        $modules = new modules();
      
      if ($this->taxon_id === 0) {
        return 0;
      }
      if ($modules->is_enabled('taxon_meta')) {
        $modules->_include('taxon_meta', 'set_meta', [
          'taxon_id' => (int)$this->taxon_id,
          'meta_name' => 'parent_id',
          'meta_value' => (int)$this->parentId,
        ]);
        return $this->parentId;
      }
      return false;
    }
  
    public function load_children( $limit = 2000, $offset = 0) {
        global $ID;
        if (!$this->taxonMetaEnabled) {
            log_action('taxon_meta module not enabled', __FILE__, __LINE__);
            return false;
        }
        $lim = ($limit) ? "LIMIT " . (int)$limit : "";
        $ofs = ($offset) ? "OFFSET " . (int)$offset : "";
        $cmd = ($this->taxon_id !== 0) 
        ? sprintf("SELECT DISTINCT taxon_id FROM %s_taxonmeta WHERE parent_id = %d %s %s;", PROJECTTABLE, (int)$this->taxon_id, $lim, $ofs)
        : sprintf("SELECT DISTINCT taxon_id FROM %s_taxonmeta WHERE parent_id IS NULL %s %s;", PROJECTTABLE, $lim, $ofs);
        if (!$res = pg_query($ID, $cmd)) {
            debug('Load children failed',__FILE__,__LINE__);
            return false;
        }
        if (pg_num_rows($res) === 0) {
            return [];
        }
        $results = pg_fetch_all($res);
        return array_map(function($t) {
            return new xTaxon($t['taxon_id']);
        }, $results);
    }
    
}

/**
* Single word in the taxon table
*/
class Word {
  private $newWord = false;
  private $newTaxon = false;
  private $props = ['wid', 'word', 'status', 'lang', 'taxon_id', 'taxon_db'];
  
  function __construct($w) {
    if (!$w['wid']) {
      $this->newWord = true;
    }
    if (!$w['taxon_id']) {
      $this->newTaxon = true;
    }
    
    foreach ($this->props as $p) {
        if (isset($w[$p]))
            $this->$p = $w[$p];
    }
    
  }
  
  public function save() {
    global $ID;
    
    if (!$this->word || !$this->lang) {
      return false;
    }
    if (!$this->newTaxon) {
      $cmd = ($this->newWord)
      ? sprintf("INSERT INTO %s_taxon (taxon_id, lang, word, status) VALUES (%d, %s, %s, %s) RETURNING wid, taxon_id;", PROJECTTABLE, (int)$this->taxon_id, quote($this->lang), quote($this->word), quote($this->status) )
      : sprintf("UPDATE %s_taxon SET lang = %s, word = %s, status = %s, taxon_id = %d WHERE wid = %d RETURNING wid, taxon_id;", PROJECTTABLE, quote($this->lang), quote($this->word), quote($this->status), (int)$this->taxon_id, (int)$this->wid );
    }
    else {
      $cmd = ($this->newWord)
      ? sprintf("INSERT INTO %s_taxon (lang, word, status) VALUES (%s, %s, %s) RETURNING wid, taxon_id;", PROJECTTABLE, quote($this->lang), quote($this->word), quote($this->status) )
      : sprintf("UPDATE %s_taxon SET lang = %s, word = %s, status = %s, taxon_id = %s WHERE wid = %d RETURNING wid, taxon_id;", PROJECTTABLE, quote($this->lang), quote($this->word), quote($this->status), 'DEFAULT', (int)$this->wid );
    }
    //debug($cmd,__FILE__,__LINE__);
    if (!$res = pg_query($ID, $cmd)) {
      debug('Word save failed',__FILE__,__LINE__);
      return false;
    }
    $results = pg_fetch_assoc($res);
    $this->wid = $results['wid'];
    $this->taxon_id = $results['taxon_id'];
    return true;
    
  }
  
  public function delete() {
    global $ID;
    if ($this->newWord) {
      return false;
    }
    $cmd = sprintf("DELETE FROM %s_taxon WHERE wid = %d;", PROJECTTABLE, (int)$this->wid );
    if (!$res = pg_query($ID, $cmd)) {
      debug('Word deletion failed',__FILE__,__LINE__);
      return false;
    }
    return true;
  }
}

?>
