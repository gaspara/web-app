-- init_tbl_exists
SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%1$s_data_exports');

-- init_seq
CREATE SEQUENCE %1$s_data_exports_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1; 

-- init_tbl
CREATE TABLE "public"."%1$s_data_exports" ( 
    "id" integer DEFAULT nextval('%1$s_data_exports_id_seq') NOT NULL, 
    "filename" character varying(128) NOT NULL, 
    "user_id" integer NOT NULL, 
    "status" varchar(32) NOT NULL,
    "message" text,
    "downloaded" integer DEFAULT 0, 
    "requested" timestamp, 
    "valid_until" timestamp,  
    CONSTRAINT "%1$s_data_exports_filename" UNIQUE ("filename"), 
    CONSTRAINT "%1$s_data_exports_id" PRIMARY KEY ("id")
);
