<?php
#' ---
#' Module:
#'   download_restricted
#' Files:
#'   [download_restricted.php, download_restricted.sql, download_restricted.js, download_restricted.css]
#' Description: >
#'   Restrict download of query results
#'   This module replaces the CSV and GPX download buttons with a form, asking for the
#'   purpose of the data download. It has an admin page, where the operators can approve
#'   or reject the requests.
#' Methods:
#'   [init, adminPage, isDownloadRestricted, delete_cache, approve, reject]
#' Module-type:
#'   project
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   1.0
class download_restricted extends module {
    var $error = '';
    var $retval;
    public $strings = ["str_explain_your_request", "str_submit", "str_cancel", "str_download_request_sent"];
    var $params;

    private $main_table;
    private $exports_dir;
    
    function __construct($action = null, $params = null,$pa = array(), $main_table = null) {
        global $BID;

        $params = $this->split_params($params);
        
        $this->exports_dir = getenv('PROJECT_DIR') . 'downloads/data_exports/';

        $this->main_table = $main_table ?? constant('PROJECTTABLE'); 

        if ($action) {
            $this->retval = $this->$action($params,$pa);
        }

    }

    protected function moduleName() {
        return __CLASS__;
    }
    
    public function modal_dialog($params,$pa) {
        if ($pa[0] == 'index') {
            return $this->dlr_dialog($params,$pa);
        }
    }

    public function dlr_dialog($params,$pa) {
        ob_start();
        ?>
        <form id="dlr-form" title="<?= t('str_download_request') ?>" style="display: none;">
            <fieldset>
                <label for="dlr-message"><?= t('str_explain_your_request') ?></label>
                <textarea id="dlr-message" name="dlr-message" rows="4" cols="50" required></textarea>                     
            </fieldset>
        </form>
        <?php 
        return ob_get_clean();
    }
    
    private function prepAdminPageData() {
        global $BID, $ID;
        
        $where = (!has_access('master')) ? sprintf("WHERE user_id = '%s'",$_SESSION['Tid']) : '';

        $cmd = sprintf("SELECT * FROM system.%s_data_exports r %s ORDER BY requested DESC;", constant('PROJECTTABLE'), $where);
        if ($res = pg_query($ID, $cmd)) {
            $requests_unfiltered = [];
            $requsters = [];
            while ($row = pg_fetch_assoc($res)) {
                $de = new DataExport();
                $de->setArr($row);
                $requests_unfiltered[] = $de;
                $requsters[] = $row['user_id'];
            }
        }


        $users = [];
        if (count($requsters)) {
            $cmd = sprintf("SELECT id, username, email FROM users WHERE id IN (%s);",implode(',',$requsters));
            if (!$res = pg_query($BID,$cmd)) {
                $this->error = "query error: $cmd";
            }
            while ($row = pg_fetch_assoc($res)) {
                $users[$row['id']] = $row;
            }
        }

        $requests = array_filter($requests_unfiltered, function($req) use ($users) {
            return (isset($users[$req->user_id]));
        });
        return compact('users', 'requests');
    }
    
    private function requestTable($requests, $users, $status) {
        $requests = array_filter($requests, function ($r) use ($status) {
            return $r->status == $status;
        });
        ob_start();
        if (!count($requests)) {
            return ob_get_clean();
        }
        ?>
        <h3><?= t("str_{$status}_requests")?></h3>
        <table id="<?= $status ?>Requests" class="pure-table pure-table-striped" style="width: 90%">
            <colgroup>
                <col style="width: 5%">
                <col style="width: 27%">
                <col style="width: 10%">
                <col style="width: 30%">
                <col style="width: 10%">
                <col style="width: 18%">
            </colgroup>
            <thead>
                <tr>
                    <th>id</th>
                    <th><?= t('str_filename') ?></th>
                    <th><?= t('str_requested_by') ?></th>
                    <th><?= t('str_message') ?></th>
                    <th><?= ($status === 'pending') ? t('str_actions') : t('str_valid_until') ?></th>
                    <th><?= t('str_download') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($requests as $r): ?>
                    <tr>
                        <td><?= $r->id ?></td>
                        <td>
                            <?= $r->filename ?>
                        </td>
                        <td><?= $users[$r->user_id]['username'] ?></td>
                        <td><?= $r->message ?></td>
                        <td>
                            <?php if (has_access('master') && $status == 'pending'): ?>
                                <a title="<?= t('str_approve') ?>" class="button-href pure-button button-success approve" data-request_id="<?= $r->id ?>"><i class="fa fa-thumbs-up"></i></a>
                                <a title="<?= t('str_reject') ?>" class="button-href pure-button button-error reject" data-request_id="<?= $r->id ?>"><i style="color: white" class="fa fa-thumbs-down"></i></a>
                            <?php else: ?>
                                <?= $r->valid_until ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (($r->is_downloadable() === true) && ($r->user_id === (int)$_SESSION['Tid'] || has_access('master')) && in_array($status, ['approved', 'ready'])): ?>
                                <a id='export_asCSV' class="pure-button button-small" href='http://<?= constant('URL') ?>/ajax?m=download_restricted&action=download&id=<?= $r->id ?>&format=csv'>.CSV</a>
                                <a id='export_asGPX' class="pure-button button-small" href='http://<?= constant('URL') ?>/ajax?m=download_restricted&action=download&id=<?= $r->id ?>&format=gpx'>.GPX</a>
                                <a id='export_asKML' class="pure-button button-small" href='http://<?= constant('URL') ?>/ajax?m=download_restricted&action=download&id=<?= $r->id ?>&format=kml'>.KML</a>
                                <a id='export_asSHP' class="pure-button button-small" href='http://<?= constant('URL') ?>/ajax?m=download_restricted&action=download&id=<?= $r->id ?>&format=shp'>.SHP</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php
        return ob_get_clean();
    }

    public function adminPage() {
        
        extract($this->prepAdminPageData());
        ob_start();
        ?>
        
        <div id="download_requests">
            <?= $this->requestTable($requests, $users, 'pending'); ?>
            <?= $this->requestTable($requests, $users, 'approved'); ?>
            <?= $this->requestTable($requests, $users, 'rejected'); ?>
            <?= $this->requestTable($requests, $users, 'ready'); ?>
        </div>

        <?php 
        
        return ob_get_clean();
    }
    
    public function isDownloadRestricted() {
        global $ID;
        
        $dlr = obm_cache('get',"dlr" . constant('PROJECTTABLE'), '', 300 );
        if ($dlr !== false) {
            return $dlr;
        }
        $dlr = false;
        $has_master_access = has_access('master');

        if ($has_master_access) {
            $dlr = 'no';
        }
        else {
            $tmp_table = sprintf('temp_query_%1$s_%2$s',constant('PROJECTTABLE'),session_id());
            $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'temporary_tables' AND table_name = %s);", quote($tmp_table));
            $res = pg_query($ID,$cmd);
            $result = pg_fetch_assoc($res);

            if ($result['exists'] == 't') {
            
                $cmd = sprintf('SELECT DISTINCT obm_id FROM temporary_tables.temp_query_%1$s_%2$s',constant('PROJECTTABLE'),session_id());
                
                if (!$res = pg_query($ID,$cmd)) {
                    $this->error = __FUNCTION__ . ' query error';
                    $dlr = 'yes';
                }
                elseif (pg_num_rows($res) === 0) {
                    $dlr = 'yes';
                }
                else {
                    $data_ids = array_column(pg_fetch_all($res), 'obm_id');
                    
                    $no_of_writable_rows = array_reduce($data_ids, function ($sum, $id) {
                        return $sum += rst('download', $id, $_SESSION['current_query_table'],false); 
                    }, 0);
                    
                    $dlr = (count($data_ids) !== $no_of_writable_rows) ? 'yes' : 'no';
                }
            }
        }
        
        obm_cache('set', "dlr" . constant('PROJECTTABLE'), $dlr);
        return $dlr;
    }
    
    public function delete_cache($params, $pa) {
        obm_cache('delete', 'dlr' . constant('PROJECTTABLE'));
    }

    public function approve($params, $request) {
        return $this->evaluateRequest($request['id'], 'approve');
    }

    public function reject($params, $request) {
        return $this->evaluateRequest($request['id'], 'reject');
    }
    
    private function evaluateRequest($id, $decision) {
        $de = new DataExport((int)$id);
        if (!$de->id) {
            $this->error = "request #{$id} not found";
            return false;
        }
        $de->$decision();
        return 'OK';
    }

    // Ezt ki használja és hogyan?
    private function getRequest($id) {
        global $ID, $BID;

        if ($res = pg_query($ID,sprintf("SELECT * FROM system.download_requests WHERE id = %d;", (int)$id))) {
            if ($request = pg_fetch_assoc($res)) {
                if ($res = pg_query($BID,sprintf("SELECT id, username, email FROM users WHERE id IN (%d);", (int)$request['user_id']))) {
                    if ($row2 = pg_fetch_assoc($res)) {
                        $request['email'] = $row2['email'];
                        $request['username'] = $row2['username'];
                        return $request;
                    }
                }
            }
            else
                return 'not found';
        }
    }

    public function init($params, $pa) {
        global $ID;
        $st_col = st_col($pa['mtable'], 'array');
            debug($st_col, __FILE__, __LINE__);
        if ($st_col['USE_RULES'] && $st_col['RULES_WORKS']) {
            // checking if the download column of rules table exists
            $cmd = sprintf(getSQL('init_rules_tbl_dl_column_exists', __CLASS__), constant('PROJECTTABLE'));
            if (!$res = pg_query($ID, $cmd)) {
                $this->error = "init query error!";
                return false;
            }
            $result = pg_fetch_assoc($res);
            if ($result['exists'] == 't') {
                return true;
            }
            
            // creating the download column in the rules table
            $cmd = sprintf(getSQL('init_alter_rules_table', __CLASS__), constant('PROJECTTABLE'));
                debug($cmd, __FILE__, __LINE__);
            if (!$res = pg_query($ID, $cmd)) {
                $this->error = "init query error!";
                return false;
            }
            return true;
        }
        return true;
    }
    
    public static function rst($params, $pa) {
        global $BID, $ID;
                
        $table = $pa['table'];
        $data_id = $pa['data_id'];
        $has_master_access = $pa['has_master_access'];
        
        // if needed, a separate level of download access can be defined in the local_vars file
        $acc_level = (defined('DL_LEVEL')) ? constant('DL_LEVEL') : constant('ACC_LEVEL');
        
        //check founder, project_managers and maintainers
        if ($has_master_access) {
            return TRUE;
        }
            
        // RIGHTS FOR DOWNLOADING DATA 
        if (in_array($acc_level, ['0', 'public']) ) {
            //ACCESSIBLE DATA FOR EVERYBODY
            return TRUE;
        }
        elseif (in_array($acc_level, ['1', 'login']) ) {
            
            if (function_exists('apache_getenv') and apache_getenv('VIEW_MODE')) return true;
            
            //ACCESSIBLE DATA FOR LOGINED USERS
            if (!isset($_SESSION['Tuser'])) {
                return FALSE;
            }
            
            $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
            $acc_cached = obm_cache('get',"rstdownload");
            if ($acc_cached !== FALSE) {
                return ($acc_cached == 'TRUE');
            }
            
            // parked users get false allways
            if (in_array($u->status, ['0', 'banned', 'parked'])) {
                obm_cache('set',"rstdownload", 'FALSE', 90);
                return FALSE;
            }
            obm_cache('set',"rstdownload", 'TRUE', 300);
            return TRUE;
        }
        elseif (in_array($acc_level, ['2', 'group']) ) {
            
            if (function_exists('apache_getenv') and apache_getenv('VIEW_MODE')) {
                return true;
            }
            
            //ACCESSIBLE DATA FOR SPECIFIED GROUPS
            if (isset($_SESSION['Tuser'])) {
                $u = unserialize($_SESSION['Tuser']);
                
                // parked users get false allways
                if (in_array($u->status, ['0', 'banned'])) {
                    return FALSE;
                }
                
                $login_groups =  (isset($_SESSION['Tgroups']) and !empty($_SESSION['Tgroups'])) ? $_SESSION['Tgroups'] : 0;
                
                if ($_SESSION['st_col']['USE_RULES'] and $data_id!='') {
                    
                    $cmd = sprintf(getSQL('check_dl_rules', __CLASS__),
                        $login_groups,
                        constant('PROJECTTABLE'),
                        (int)$data_id,
                        quote($table)
                    );
                    $res = pg_query($ID,$cmd);
                    
                    // no rules for data
                    if (!pg_num_rows($res)) {
                        return false;
                    }
                    
                    $data_acc_row = pg_fetch_assoc($res);
                    if ($data_acc_row['case'] == 1) {
                        return true;
                    }
                } 
                elseif ($data_id != '') {
                    // if there is no rules, lets see who is the uploader
                    // it is important because the base policy is FALSE, see below
                    $cmd = sprintf(getSQL('check_dl_no_rules', __CLASS__), $table, (int)$data_id );
                    $res = pg_query($ID,$cmd);
                    if ($row = pg_num_rows($res)) {
                        if ($_SESSION['Trole_id'] == $row['uploader_id']) {
                            return true;
                        }
                    }
                }
            } 
            //default non handled cases
            return false;
        }
    }
}

?>
