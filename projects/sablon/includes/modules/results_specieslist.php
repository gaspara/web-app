<?php
#' ---
#' Module:
#'   results_specieslist
#' Files:
#'   [results_specieslist.php, results_specieslist.css, results_specieslist.js]
#' Description: >
#'   Species list summary on query map page
#' Methods:
#'   [print_results_summary_specieslist, print_js, print_css, ajax]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   2.0
class results_specieslist extends module {
    var $error = '';
    var $retval;
    
    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;

        $this->params = $this->split_params($params);

        if (method_exists($this, $action)) {
          $this->retval = $this->$action($params,$pa);
        }

    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function print_results_summary_specieslist ($params, $pa) {
        return '<div id="results_specieslist"></div>';
    }
    
    public function translations() {
        $strings = ["str_specieslist", "str_no_of_taxons", "str_species", "str_no_records", "str_no_inds", "str_view", "str_download_specieslist",
        "str_alphabetical", "str_orderby_taxonomy", "str_inthisquerythefollowing", "str_order_of_results"];
        $translations = [];
        foreach ($strings as $string) {
            $translations[$string] = t("$string");
        }
        return $translations;
    }
    
    public function ajax($params, $request) {
        
        global $ID, $x_modules;
        /* mapview, filterbox
        * get dynamic list of filtered elements from a table
        * return JSON array of filtered elements
        * */
        if (isset($request['action'])) {
            switch ($request['action']) {
                case 'translations':
                    $translations = $this->translations();
                    
                    if ($x_modules->is_enabled('taxon_meta')) {
                        $translations = array_merge(
                            $translations, 
                            $x_modules->_include('taxon_meta', 'translations')
                        );
                    }
                    echo json_encode($translations);
                    break;
                    
                case 'get_species_list':
                    echo common_message('ok', $this->prepare_species_list($params));
                    break;
                    
                case 'get_cite_list':
                    echo common_message('ok', $this->prepare_cite_list($params));
                    break;

                default:
                    // code...
                    break;
            }
            exit;
            
        } 
    }
    
    private function prepare_species_list($params) {
        global $ID,$BID, $modules, $x_modules;
        
        $qtable = $_SESSION['current_query_table'];
        $st_col = st_col($qtable,'array');
        $species_cols = array_merge([$st_col['SPECIES_C'], $st_col['SPECIES_C_SCI']], $st_col['ALTERN_C']);
        $ACC_LEVEL = ACC_LEVEL;

        $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
        $SPECIES_C = (isset($params[0]) && $params[0]!='' && in_array($params[0], $species_cols)) ? $params[0] : $st_col['SPECIES_C'];
        
        if (!$SPECIES_C) {
            $data['error'] = "Invalid species column";
            return $data;
        }
        
        // initializing default values
        $data = [
            "species_list" => [],
            "error" => false
        ];
        $allowed_cols = $main_cols;

        //a subset of the main cols, depending on user's access rights
        if ($modules->is_enabled('allowed_columns')) {
            if (isset($_SESSION['load_loadquery']) and !$_SESSION['load_loadquery'])
                $allowed_cols = $modules->_include('allowed_columns','return_columns',$main_cols,true);
        } else {
            if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                $allowed_cols = array();
            }
        }

        // default integrated module
        // a fajnév mezőt berakja az uspecies tömbbe
        $SPECIES_C_colname = $SPECIES_C;
        if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$SPECIES_C,$m)) {
            $SPECIES_C_prefix = $m[1];
            $SPECIES_C_colname = $m[2];
        }

        if (isset($st_col['NUM_IND_C']) and $st_col['NUM_IND_C']!='')
            $sum_ind = sprintf('SUM(%s::integer)',$st_col['NUM_IND_C']);
        else
            $sum_ind = "'NA'";

        // snesitive data handling
        // query only for the allowed users or groups
        $tid = $_SESSION['Tid'] ?? 0;
        $tgroups = $_SESSION['Tgroups'] ?? 0;
        $super = (has_access('master'));

        // we do not overwrite the results of loaded queries!!!
        if (isset($_SESSION['load_loadquery']) and $_SESSION['load_loadquery'])
            $super = TRUE;

        if ($super) {
            $cmd = sprintf('SELECT DISTINCT ON (%1$s) %1$s AS species,COUNT(%1$s) AS counter,%4$s AS num FROM temporary_tables.temp_query_%2$s_%3$s GROUP BY %1$s',$SPECIES_C_colname,PROJECTTABLE,session_id(),$sum_ind);
        } else {

            if ($st_col['USE_RULES']) {

                $puy = 1;
                $intersected_allowed_columns_count = count(array_intersect($_SESSION['current_query_keys'],$allowed_cols));
                $query_column_count = count($_SESSION['current_query_keys']);
                if ($intersected_allowed_columns_count != $query_column_count) {
                    $puy = 0;
                }

                // exclude sensitive data if rules allowed
                $cmd = sprintf('SELECT DISTINCT ON (%1$s) %1$s AS species,COUNT(%1$s) AS counter,%4$s AS num FROM temporary_tables.temp_query_%2$s_%3$s
                    LEFT JOIN %2$s_rules r ON ("data_table"=\'%7$s\' AND obm_id=r.row_id)
                    WHERE
                      (sensitivity::varchar IN (\'1\',\'2\',\'3\',\'no-geom\',\'restricted\',\'sensitive\',\'only-own\') AND ARRAY[%5$s] && read) OR 
                      (sensitivity::varchar IN (\'1\',\'2\',\'no-geom\',\'restricted\',\'sensitive\') AND 1=%6$s ) OR 
                      (sensitivity::varchar IN (\'0\',\'public\')) OR 
                      sensitivity IS NULL
                    GROUP BY %1$s',$SPECIES_C_colname,PROJECTTABLE,session_id(),$sum_ind,$tgroups,$puy,$_SESSION['current_query_table']);

            } else {
                $cmd = sprintf('SELECT DISTINCT ON (%1$s) %1$s AS species,COUNT(%1$s) AS counter,%4$s AS num FROM temporary_tables.temp_query_%2$s_%3$s GROUP BY %1$s',$SPECIES_C_colname,PROJECTTABLE,session_id(),$sum_ind);
            }
        }


        $res = pg_query($ID,$cmd);
        if(pg_last_error($ID)) {
            log_action("specieslist module sql execution failed: $cmd",__FILE__,__LINE__);
            return;
        }
        $uspecies = array();
        $uspecies_counter = array();
        $uspecies_num = array();
        while($row = pg_fetch_assoc($res)) {
            $uspecies[] = $row['species'];
            $uspecies_counter[$row['species']] = $row['counter'];
            $uspecies_num[$row['species']] = $row['num'];
        }
        //$res2 = pg_query($ID,sprintf('SELECT %1$s,COUNT(%1$s) AS counter FROM temporary_tables.temp_query_%2$s_%3$s GROUP BY %1$s',$_SESSION['st_col']['SPECIES_C'],PROJECTTABLE,session_id()));

        //create specieslist div

        if (count($uspecies)) {

            $u = implode(',',array_map('quote',$uspecies));

            if (!isset($_SESSION['match']) || $_SESSION['match'] == 'all') {
                $cmd = sprintf("SELECT taxon_id FROM %s_taxon WHERE word IN (%s) GROUP BY taxon_id",PROJECTTABLE,$u);
                $res = pg_query($ID,$cmd);
            }
            else {
                $cmd = sprintf("SELECT meta FROM %s_taxon WHERE word IN (%s)",PROJECTTABLE,$u);
                $res = pg_query($ID,$cmd);
            }

            $k = '';
            $kn = 0;
            $tn = array();
            $taxon_names = array();
            $taxon_names_counter = array();
            $we = array();
            $results = (pg_num_rows($res) > 0) ? pg_fetch_all($res) : [];

            if (!isset($_SESSION['match']) || $_SESSION['match'] == 'all') {
                $taxon_ids = implode(',', array_column($results, 'taxon_id'));
                $cmd = sprintf("SELECT * FROM %s_taxon WHERE taxon_id IN (%s) AND lang='%s' ORDER BY status", PROJECTTABLE, $taxon_ids, $SPECIES_C_colname);
            }
            else {
                $metas = implode(',', array_map('quote', array_column($results, 'meta')));
                $cmd = sprintf("SELECT * FROM %s_taxon WHERE meta IN (%s) AND lang='%s' ORDER BY status", PROJECTTABLE, $metas, $SPECIES_C_colname);
            }
            $res2 = pg_query($ID,$cmd);
            $taxons = pg_fetch_all($res2);
            
            foreach ($results as $row) {
                ## more than one status name available
                $taxon = array_values(array_filter($taxons, function ($t) use ($row) {
                    return (!isset($_SESSION['match']) || $_SESSION['match'] == 'all') 
                        ? $t['taxon_id'] === $row['taxon_id']
                        : $t['meta'] === $row['meta'];
                }));

                $e = (count($taxon)>1);
                
                $trans = array();
                foreach ($taxon as $row2) {
                    if ($e) {
                        $we[$row2['word']] = $row['taxon_id'];

                        if (in_array($row2['status'], ['1', 'accepted'])) {
                            //status is 1
                            $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                            $trans[$row2['taxon_id']] = 1;
                            continue;
                        }
                        elseif(in_array($row2['status'], ['0', 'undefined'])) {
                            //status is 0
                            if ($key = array_search($row2['taxon_id'], array_column($tn, 'id'))) {
                                $tn[$key] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                            }
                            else
                            $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                            $trans[$row2['taxon_id']] = 0;
                        }
                        elseif(!isset($trans[$row2['taxon_id']]))  {
                            //status is 2,3
                            //only one alternative name can come into this array
                            $trans[$row2['taxon_id']] = 0;
                            $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                        }
                    }
                    else {

                        $tn[] = ['id' => $row2['taxon_id'], 'name' => $row2['word']];
                    }
                }
            }
            
            foreach ($tn as $key => $row) {
                $names[$key]  = $row['name'];
            }
            obm_set_locale();
            array_multisort($names, SORT_ASC, SORT_LOCALE_STRING, $tn);


            ## create species list DIV
            $kfin = array();

            foreach ($tn as $taxon_names) {
                // más státusz név esetén darabszám átmentése a másik névhez
                // {"Periparus ater":"265","Parus ater":"265","Chloris chloris":"40","Carduelis chloris":"40","Buteo buteo vulpinus":"397","Buteo buteo":"397","Poecile montanus":"332","Parus montanus":"332"}
                // nem értem, nem tudom, hogy ez jó-e valamire

                $id = $taxon_names['id'];
                $name = $taxon_names['name'];
                if (!isset($uspecies_counter[$name])) {
                    $weid = $we[$name];
                    $wekeys = array_keys($we);
                    foreach($wekeys as $key) {
                        $val = $we[$key];
                        if ($val == $weid and isset($uspecies_counter[$key])) {
                            $uspecies_counter[$name] = 0;
                            $uspecies_num[$name] = 0;
                        }
                    }

                }
                // ez így biztos jó!
                // joined names exists!
                // synonim names
                if (isset($we[$name])) {
                    foreach($we as $we_key=>$we_id) {
                        if ($id == $we_id and !in_array($we_key,$names) and isset($uspecies_counter[$we_key])) {
                            $uspecies_counter[$name] += $uspecies_counter[$we_key];
                            $uspecies_num[$name] += $uspecies_num[$we_key];
                        }
                    }

                }

                $species_list[] = [
                    "taxon_id" => $id,
                    "name" => $name,
                    "count" => $uspecies_counter[$name],
                    "num" => $uspecies_num[$name],
                ];

                $kn++;
            }

            $orders = [];
            if ($x_modules->_include('taxon_meta', 'is_submodule_enabled', ['TaxonOrder'])) {
                $species_list = array_map(function ($sp) {
                    $sp['word'] = $sp['name'];
                    return $sp;
                }, $species_list);
                $orders = $x_modules->_include('taxon_meta', 'include_submodule', ['TaxonOrder', 'get_orders', $species_list]);
                $species_list = array_map(function($sp) use ($orders) {
                    foreach ($orders['ordering_categories'] as $oby) {
                        $sp[$oby] = (int)$orders['order'][$sp['taxon_id']][$oby];
                    }
                    return $sp;
                }, $species_list);
                $data['ordering_categories'] = $orders['ordering_categories'];
                $data['selected_order'] = $x_modules->_include('taxon_meta', 'include_submodule', ['TaxonOrder', 'get_preferred_order']);
            }
            
            if ($x_modules->_include('taxon_meta', 'is_submodule_enabled', ['TaxonRank'])) {
                $taxon_rank_data = $x_modules->_include('taxon_meta', 'include_submodule', ['TaxonRank', 'filter_list', compact('species_list')]);
                $species_list = $taxon_rank_data['species_list'];      
                if (!$species_list) {
                   $data['error'] = 'taxon_meta TaxonRank submodule error';
                   return $data;
                }
                $data['ranks'] = $taxon_rank_data['ranks'];
            }

            $data['species_list'] = $species_list;
            return $data;
        }
    }

    private function prepare_cite_list($params) {
        global $ID;

        $cite_data = [
            'cite_list' => [],
            'errors' => false
        ];
        
        $st_col = st_col($_SESSION['current_query_table'],'array');

        $cite = array();
        //create data->cite
        if (!$res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%2$s_%3$s','obm_id',PROJECTTABLE,session_id()))) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            $cite_data['error'] = 'query error';
            return $cite_data;
        }
        while($row = pg_fetch_assoc($res)) {
            $row_keys = array_keys($row);
            foreach($row_keys as $i) {
                if (in_array($i,$st_col['CITE_C'])) {
                    $cite[] = $row[$i];
                }
            }
        }

        $names = array();
        foreach(array_unique($cite) as $n1){
            foreach( explode(',',$n1) as $n2) {
                $names[] = trim($n2);
            }
        }
        $names = array_filter(array_unique($names));
        asort($names);

        $cite_data['cite_list'] = array_values($names);
        return $cite_data;
    }
}
?>
