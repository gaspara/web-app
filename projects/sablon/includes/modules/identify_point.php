<?php
#' ---
#' Module:
#'   identify_point
#' Files:
#'   [identify_point.php, identify_point.css]
#' Description: >
#'   a map page extension to click on record, and show a modal with short description and data links
#' Methods:
#'   [return_data, print_panelbox, print_js, ajax, map_js]
#' Examples: >
#'   [
#'     "species",
#'     "date",
#'     "observer"
#'   ]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class identify_point extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    /* params:
     * point:
     * ce:
     * */
    function return_data ($params,$dynamic_params) {
        global $ID,$BID,$modules;

        $ACC_LEVEL = ACC_LEVEL;

        list($point,$ce) = $dynamic_params;

        $st_col = st_col($_SESSION['current_query_table'],'array');
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
        $tgroups = $u->roles ?? 0;

        $sqla = sql_aliases(1);
        $qtable_alias = $sqla['qtable_alias'];
        $geom_alias = $sqla['geom_alias'];
        $qtable = $sqla['qtable'];
        $FROM = $sqla['from'];
        $GEOM_C = $sqla['geom_col'];
        $GEOMETRY_COLUMN = $sqla['GEOMETRY_COLUMN'];
        $allowed_cols_module = false;
        $jtable_alias = $sqla['join_alias'];

        $ce = (array) json_decode($ce);

        $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
        $main_cols[] = 'obm_id';
        $allowed_cols = $main_cols;
        $columns = 'obm_id';
        $parameter_columns = array('obm_id');

        if ($params != '') {
            $parameter_columns = $this->split_params($params);
        }

        // visible columns
        extract(allowed_columns($modules,$_SESSION['current_query_table']));

        #$cmd = sprintf("SELECT St_Distance(St_GeomFromText('POINT(%f %f)'),St_GeomFromText('POINT(%f %f)'))",$ce['left'],$ce['bottom'],$ce['right'],$ce['top']);
        $cmd = sprintf("SELECT ST_Distance(
			    ST_GeomFromText('POINT(%f %f)',3857),
                            ST_GeomFromText('POINT(%f %f)',3857)
                        )",
                    $ce[0],$ce[1],$ce[2],$ce[3]);

        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        $diameter = $row['st_distance']/90;
        $rule = '';
        $rule_join = '';
        $grid_join = '';
        
        if ($modules->is_enabled('grid_view')) {
            if (isset($_SESSION['display_grid_for_map'])) {
                $grid_geom_col = $_SESSION['display_grid_for_map'];
                $GEOM_C = 'qgrids.'.$grid_geom_col.'';
            } else {
                $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
                $GEOM_C = 'qgrids.'.$grid_geom_col.'';
            }

            /* JOIN with grid table if module enabled */
            $grid_join = sprintf('LEFT JOIN %s_qgrids as qgrids ON ("%s".obm_id=qgrids.row_id)',
                                    $_SESSION['current_query_table'],$qtable_alias);
        }

        if ( !has_access('master') and $st_col['USE_RULES']) {

            $rule_join = sprintf('LEFT JOIN %1$s_rules r ON (r.data_table=\'%2$s\' AND %3$s.obm_id=r.row_id)',
                                    PROJECTTABLE,$qtable,$qtable_alias);

            $rule = " AND ( (sensitivity::varchar IN ('3','only-owner') AND ARRAY[$tgroups] && read) ";
            $rule .= " OR (sensitivity IS NULL OR sensitivity::varchar IN ('0','public') ) ";
            $rule .= " OR (sensitivity::varchar IN ('2','restricted','sensitive') AND ARRAY[$tgroups] && read)";
            $rule .= " OR (sensitivity::varchar IN ('1','no-geom'))";

            $current_query_keys = array('obm_geometry');
            $intersected_allowed_columns_count = count(array_intersect($current_query_keys,$allowed_cols));
            $query_column_count = count($current_query_keys);
            if ($intersected_allowed_columns_count != $query_column_count) {
                $rule .= " OR (sensitivity::varchar IN ('2','restricted','sensitive') AND 1=0 AND (read && ARRAY[$tgroups])=FALSE) ";
            } else {
                $rule .= " OR (sensitivity::varchar IN ('2','restricted','sensitive')) ";
            }
            $rule .= ") ";
        }
        $subfilter = "";
        $res4 = pg_query($ID,sprintf("SELECT EXISTS (
           SELECT 1
           FROM   information_schema.tables
           WHERE  table_schema = 'temporary_tables'
           AND    table_name = 'temp_%s_%s');",
        PROJECTTABLE,session_id()));
        $row = pg_fetch_assoc($res4);
        if ($row['exists']=='t') {
            $res5 = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_%1$s_%2$s',
                                    PROJECTTABLE,session_id()));
            if (pg_num_rows($res5))
                $subfilter = sprintf('INNER JOIN temporary_tables.temp_%1$s_%2$s t ON (%3$s.obm_id=t.obm_id)',
                                        PROJECTTABLE,session_id(),$qtable_alias);
        }

        //$cmd = sprintf('SELECT %9$s.obm_id,CONCAT_WS(\'<br>\',%3$s) as short_content,%10$s count(*) OVER() AS full_count %1$s %7$s %5$s
        $cmd = sprintf('SELECT *, count(*) OVER() AS full_count, %11$s.obm_id AS main_obm_id 
                            %1$s %7$s %10$s %5$s
                            WHERE ST_DWithin(ST_Transform(%9$s.%8$s,3857), ST_GeomFromText(%2$s,3857), %4$s) %6$s
                            ORDER BY ST_Distance(ST_Transform(%9$s.%8$s,3857),ST_GeomFromText(%2$s,3857)),%11$s.obm_id DESC 
                            LIMIT 30',
                    $FROM,$point,'',$diameter,$rule_join,$rule,$subfilter,$GEOMETRY_COLUMN,$geom_alias,$grid_join,$qtable_alias);

        $res = pg_query($ID,$cmd);
        $n = pg_num_rows($res);
        $e = '';
        $fc = '';
        $distinct_values = array();
        $has_master_access = has_access('master');
        while ($row2 = pg_fetch_assoc($res)) {
            if (!in_array($row2['main_obm_id'],$distinct_values))
                $distinct_values[] = $row2['main_obm_id'];
            else
                continue;
        
            $acc = rst('acc',$row2['main_obm_id'],$qtable, $has_master_access) ? true : false; 
            
            $cols = $parameter_columns;
            if (!$acc and $st_col['USE_RULES']) {
                
                if ($row2['sensitivity']!='0' or $row2['sensitivity']!='public')  {
                    $read = preg_replace('/[{}]/','',$row2['read']);
                    if (!count(array_intersect(explode(',',$tgroups),explode(',',$read)))) {
                        $cols = array_values(array_intersect($parameter_columns,$allowed_cols));
                    }
                }
            }
            if (!$acc and !$st_col['USE_RULES']) {
                $read = preg_replace('/[{}]/','',$row2['read']);
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$read)))) {
                    $cols = array_values(array_intersect($parameter_columns,$allowed_cols));
                }
            } 
            if (!isset($_SESSION['Tid']) and !$allowed_cols_module and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
                $cols = array('omb_id');
            }

            if (!$acc and !$allowed_cols_module and $st_col['USE_RULES'])
                $cols = array('omb_id');

            $connid = 0;
            $values = [];
            foreach ($cols as $column) {
                if ($column === 'obm_taxon') {
                  $column = $st_col['SPECIES_C'];
                }
                if ($column == 'obm_files_id' && $row2[$column] !== '') {
                    $connid = $row2[$column];
                    continue;
                }
                /* Hol van ilyen eset?
                 * Át kell írni a JSON definíciót is talán?
                 * vagy elég ennyi?
                if (is_array($column)) {
                 *
                if (preg_match("/^JSON:(.*)$/", $column, $o)) {
                    $j = json_decode(base64_decode($o[1]));
                    $err = "<div class='text-error'>%s</div>";
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        $values[] = sprintf($err,json_last_error_msg());
                        continue;
                    }
                    if (!isset($j->type)) {
                        $values[] = sprintf($err,"incomplete json: <a href='http://openbiomaps.org/documents/hu/modules.html' target='_blank'>read the documentation</a>");
                        continue;
                    }

                    if ($j->type == 'link') {
                        if (!isset($j->href) || !isset($j->label)) {
                            $values[] = sprintf($err,"incomplete json: <a href='http://openbiomaps.org/documents/hu/modules.html' target='_blank'>read the documentation</a>");
                            continue;
                        }
                        $label = (defined($j->label)) ? constant($j->label) : $j->label;
                        $params = (isset($j->params)) ? $j->params : [];
                        $class = (isset($j->class)) ? $j->class : '';
                        $target = (isset($j->target)) ? $j->target : '_blank';
                        $id = (isset($j->id)) ? $j->id : '';

                        for ($i = 0; $i < count($params); $i++) {
                            $j->href = preg_replace("/%" . ($i+1) . "%/",$row2[$params[$i]],$j->href);
                        }
                        $values[] = sprintf("<a href='%s' target='%s' id='%s' class='%s'>%s</a>",$j->href,$target,$id,$class,$label);
                    }


                }
                else
                 */
                    $values[] = $row2[$column];
            }

            $eurl = sprintf($protocol.'://'.URL.'/data/%s/%d/',$_SESSION['current_query_table'],$row2['main_obm_id']);

            $values = array_filter($values);
            if (!count($values)) $values[] = '???';

            $title_value = array_shift($values);
            $short_content = '<br>'.implode('<br>',$values);
            $photolink = ($connid) ? "<a href='getphoto?connection=$connid' class='photolink'><i class='fa fa-camera'></i></a>" : "";
            $e .= "<div class='identify_point_record'><a href='$eurl' target='_blank'><span style='font-size:120%'>$title_value</span></a>$photolink $short_content</div>";
            $fc = $row2['full_count'];
        }
        return json_encode(array("records"=>$n,"all_records"=>$fc,"content"=>$e,"str_listed"=>str_listed,"str_sumall"=>str_sumall));

    }

    function print_panelbox ($params) {
        global $ID;

        // Drop previous filters on loading page
        $res = pg_query($ID,
                sprintf("SELECT EXISTS (
                            SELECT 1
                            FROM   information_schema.tables
                            WHERE  table_schema = 'temporary_tables'
                            AND    table_name = 'temp_%s_%s');",PROJECTTABLE,session_id()));
            $row = pg_fetch_assoc($res);
        if ($row['exists']=='t') {
                $res = pg_query($ID,sprintf('DROP TABLE temporary_tables.temp_%1$s_%2$s',PROJECTTABLE,session_id()));
        }

        # no params processing...
        $out = "<span>".mb_convert_case(str_identify, MB_CASE_UPPER, "UTF-8").":&nbsp;</span>";
        $out .= "<button class='pure-button button-passive button-small' id='identify_point'><i class='fa fa-lg fa-info'></i></button>";
        return $out;
    }

    public function map_js ($params) {
    return '
var old_msc = map_single_click;
map_single_click = function(x) {
    old_msc.apply(this, arguments);
    if (singleClickListener == "on") {
        IdentifyPoint_action(x);
    }
}';
    }
    
    public function print_js ($params) {
        return '
function IdentifyPoint_action(evt) {
    var coords = evt.coordinate;
    var ce = map.getView().calculateExtent(map.getSize());
    $.post("ajax",{"m":"identify_point","lat":coords[1],"lon":coords[0],"ce":JSON.stringify(ce)},function(data){
        if (data != "") {
            v = JSON.parse(data);
            $( "#dialog" ).html(v["content"]);
            //$( "#dialog" ).dialog( "option", "position", { my: "left-3 top+3",of: evt } );
            $( "#dialog" ).dialog( "option", "title", ""+v["str_listed"]+": "+v["records"]+" ("+v["str_sumall"]+": "+v["all_records"]+")." );
            $( "#dialog" ).dialog( "option", "height", 200 );
            $( "#dialog" ).dialog( "option", "width", 400 );
            var isOpen = $( "#dialog" ).dialog( "isOpen" );
            if(!isOpen) $( "#dialog" ).dialog( "open" );
        }
    });
    }
';}

    public function ajax($params, $request) {

        /* Identify click on map
         *
         * */

        $point = quote("POINT(".$request['lon']." ".$request['lat'].")");
        $ce = $request['ce'];

        /* MODULE INCLUDES HERE */
        echo $this->return_data($params,array($point,$ce));

        exit;
    }
}
?>
