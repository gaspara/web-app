<?php
#' ---
#' Module:
#'   list_manager
#' Files:
#'   [list_manager.php, list_manager.js, list_manager.css, list_manager.sql]
#' Description: >
#'   Manage lists used for uploads and queries
#' Methods:
#'   [getMenuItem, adminPage, displayError, ajax, print_js, init, print_css]
#' Module-type:
#'   table
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   2.0.0
class list_manager extends module {

    public $params;
    public $main_table;

    public $error = '';
    public $retval;

    function __construct($action = null, $params = null, $pa = array(), $main_table = null) {
        global $BID;

        if (is_array($params)) {
            $this->params = $params;
        }
        else {
            $this->params = $this->split_params($params);
        }

        if ($main_table)
            $this->main_table = $main_table;

        if ($action)
            $this->retval = $this->$action($params,$pa);
        
    }
 
    protected function moduleName() {
        return __CLASS__;
    }

    public function getMenuItem() {
        return ['label' => 'List manager', 'url' => 'list_manager' ];
    }

    public function adminPage($params) {
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $out = '<button class="module_submenu button-success button-xlarge pure-button" data-url="includes/project_admin.php?options=list_manager"> <i class="fa fa-refresh"></i> </button>';


        return $out;
    }
    
    public function db_cols_header() {
        return 'Manage lists';
    }

    public function manage_list_buttons($params, $pa) {
        extract($pa);
        $defined_lists = $this->get_defined_lists($table);
        $buttons = [];
        foreach ($collist as $c) {
            $def = (in_array($c, $defined_lists)) ? 'button-success' : '';
            $buttons[$c] = "<button class='pure-button open-list-manager $def' data-table='$table' data-colname='$c'><i class='fa fa-list' aria-hidden='true'></i></button>";
        }
        return $buttons;
    }

    public function modal() {
        ob_start();
        ?>
        <div id="list-manager" style="display:none">
            <textarea id="list-manager-textarea" rows="20" cols="80"></textarea>
        </div>
        <?php
        return ob_get_clean();
    }

    public function upload_form_builder_widget() {
        ob_start();
        ?>
        <form id="list-manager-widget">
            <div class="lm-buttons">
                <div> <input type='radio' name='lm-list-elements' value='all' class='lm-list-elements'> All defined elements </div>
                <div> <input type='radio' name='lm-list-elements' value='select' class='lm-list-elements'> Select list elements </div>
                <div> <input type='radio' name='lm-list-elements' value='standard-editor' class='lm-list-elements'> Use the standard editor </div>
            </div>
            <div id="lm-list"> </div>
        </form>
        <?php
        return ob_get_clean();
    }

    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    /* AJAX FUNCTIONS */
    public function get_list($params, $request, $generate = false) {
        $list = new li5t($request, $generate);
        
        if (isset($request['list_and_count'])) {
            return $list->get_list_and_count();
        }
        return $list->get_list('term');
    }
    
    public function save_list($params, $request) {
      
        $l = explode("\n",$request['data']);
        $list = new li5t($request);

        $list->save_list($l);

        return 'saved';
    }
    
    public function get_term($params, $request) {
        return new xTerm([
            'data_table' => $request->table,
            'subject' => $request->subject,
            'term' => $request->word,
            'term_id' => $request->taxon_id,
            'status' => $request->status,
            'wid' => $request->id,
            'role_id' => $request->role_id ?? null
        ]);
    }
    
    public function generate_list($params, $request) {
        return $this->get_list($params, $request, true);
    }

    public function get_defined_lists($table) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT subject FROM %s_terms WHERE data_table = %s;", PROJECTTABLE, quote($table));
        if ( !$res = pg_query($ID, $cmd)) {
            $this->error = 'List manager - get_defined_lists query error';
            return false;
        }
        if (pg_num_rows($res) === 0) {
            return [];
        }
        $results = pg_fetch_all($res);
        return array_column($results, 'subject');
    }
    
    public function get_tables_subjects() {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT data_table, subject, string_agg(DISTINCT status, ',') as status FROM %s_terms GROUP BY data_table, subject;", PROJECTTABLE);
        if ( !$res = pg_query($ID, $cmd)) {
            $this->error = __CLASS__ . "::" . __FUNCTION__ .' query error';
            return false;
        }
        if (pg_num_rows($res) === 0) {
            return [];
        }
        $results = pg_fetch_all($res);
        
        // finding the cite columns to connect users and observers
        $default_table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE;
        $st_col = st_col($default_table, 'array');
        for ($i=0; $i < count($results); $i++) {
            $results[$i]['obm_type'] = false;
            if ($results[$i]['data_table'] === $default_table && in_array($results[$i]['subject'], $st_col['CITE_C'])) {
                $results[$i]['obm_type'] = 'cite';
            }
        }
        
        return $results;
    }
    
    public function init($params, $pa) {
        global $ID;

        //checking terms table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_terms');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);
        
        if ($result['exists'] == 'f') {
            $cmd = sprintf(getSQL('create_terms_table', 'list_manager'), PROJECTTABLE);
            if (!pg_query($ID,$cmd)) {
                $this->error = 'list_manager init failed: table creation error';
                return array('error'=>$this->error);
            }
            
            $cmd = sprintf(getSQL('create_terms_table_index', 'list_manager'), PROJECTTABLE );
            if (!pg_query($ID,$cmd)) {
                $this->error = 'list_manager init failed: index creation error';
                return array('error'=>$this->error);
            }

        }
    }
    
    public function exists_users_table($params, $pa) {
        global $ID;
        $cmd = "SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'users');";
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);
        return ($result['exists'] == 't');
    }

    public function get_tree($params, $request) {
        if (!isset($request['root_id'])) {
            return [];
        }
        $t = new Tree((int)$request['root_id'], $request['depth'] ?? null);
        return $t->tree;
    }

    public function get_roots($params, $request) {
        $t = new Tree(null);
        return $t->tree;
    }

    public function create_node($params, $request) {
        $term = new Term();
        $term->data_table = PROJECTTABLE;
        $term->subject = 'na';
        $term->term = $request['term'];
        $term->parent_id = $request['parent_id'];
        debug($term, __FILE__, __LINE__);
        return $term->save();
    }

    public function update_node($params, $request) {
        $term = Term::first(['term_id', $request['term_id']]);
        $term->term = $request['term'];
        $term->parent_id = $request['parent_id'];

        return $term->save();
    }

    public function delete_node($params, $request) {
        $t = new Tree((int)$request['term_id']);
        $t->deleteTreeTerms();
    }
}

class li5t {
    var $list = [];
    var $total_count = null;
    var $subject = '';
    var $status = "";
    var $table = '';
    var $limit = null;
    var $offset = null;
    var $where = "";
    var $dist = "1 as dist";
    var $meta = "lower(unaccent(term))";
    var $ordering = "ORDER BY term_id ASC";

    public function __construct($args = [], $generate = false ) {
        global $ID;

        $this->limit = (isset($args['limit']) && is_numeric($args['limit'])) ? "LIMIT {$args['limit']}" : "";
        $this->offset = (isset($args['offset']) && is_numeric($args['offset'])) ? "OFFSET {$args['offset']}" : "";
        
        if (isset($args['orderby']) && $args['orderby']) {
            if ($args['orderby'] == 'newestFirst') {
                $args['orderby'] = ['status', 'wid'];
                $args['order'] = ['DESC', 'DESC'];
            }
            $this->ordering = prepareOrder($args['orderby'], $args['order'] ?? NULL);
        }
        
        $where = [];
        if ($args['data_table']) {
            $where[] = prepareFilter('data_table', $args['data_table']);
            $this->table = $args['data_table'];
        }
        if ($args['colname']) {
            $where[] = prepareFilter('subject', $args['colname']);
            $this->subject = $args['colname'];
        }
        
        if ($args['status']) {
            $where[] = prepareFilter('status', $args['status']);
            $this->status = $args['status'];
        }
        
        if (isset($args['excluded']) && $args['excluded']) {
            $where[] = prepareFilter('term_id', $args['excluded'], 'NOT IN');
        }
        
        if (isset($args['term']) && $args['term']) {
            $term = strtolower(remove_accents($args['term']));
            if ($args['search_algoritm'] === 'like') {
                $this->dist = sprintf('
                    CASE 
                        WHEN %4$s ILIKE %1$s OR %4$s ILIKE %3$s THEN 1 
                        WHEN %4$s ILIKE %2$s THEN 2 
                        ELSE 3 
                    END AS dist', 
                    quote("{$args['term']}%"), quote("% {$args['term']}%"), quote("%/ {$args['term']}%"), $this->meta
                );
                $where[] = prepareFilter($this->meta, "%{$term}%", 'LIKE');
            }
            else {
                $this->dist = sprintf("1 - (lower(replace(term, ' ', ''))<->%s) AS dist", quote($args['term']));
                $where[] = prepareFilter("lower(replace(term, ' ', ''))<->".quote($term), 0.9, "<");
            }
        }
        
        if ($args['preFilter']) {
            $where[] = prepareFilter(
                $args['preFilter']['preFilterColumn'],
                $args['preFilter']['preFilterValue'],
                $args['preFilter']['preFilterRelation'] ?? null
            );
        }
        
        if (isset($args['id']) && $args['id']) {
            $where[] = prepareFilter('term_id', $args['id']);
        }
        
        $this->where = (count($where)) ? "WHERE " . implode(" AND ", $where) : "";

        if ($generate)
            $this->generate_list();
        else {
            $this->request_list();
            $this->total_list_count();
        }
        
    }

    private function request_list() {
        global $ID;

        $cmd = sprintf("SELECT *, %s as meta, %s FROM %s_terms %s %s %s %s;", $this->meta, $this->dist, PROJECTTABLE, $this->where, $this->ordering, $this->limit, $this->offset);
        if (! $res = pg_query($ID, $cmd) ) {
            log_action(pg_last_error($ID));
            return;
        }
        while ($row = pg_fetch_assoc($res)) {
            $this->list[] = new xTerm($row);
        }

        return;
    }
    
    private function total_list_count() {
        global $ID;

        $cmd = sprintf("SELECT count(*) c FROM %s_terms %s;", PROJECTTABLE, $this->where);
        if (! $res = pg_query($ID, $cmd) ) {
            log_action(pg_last_error($ID));
            return;
        }
        $asdf = pg_fetch_assoc($res);
        $this->total_count = $asdf['c'];

        return;
    }

    private function generate_list() {
        global $ID;

        $cmd = sprintf("SELECT DISTINCT %s FROM %s;", $this->subject,  $this->table);

        if (! $res = pg_query($ID, $cmd) ) {
            log_action(pg_last_error($ID));
            return;
        }
        $results = pg_fetch_all($res);
        $this->set_list(array_column($results, $this->subject));

        return;

    }

    public function set_subject($subject) {
        if ($subject)
            $this->subject = $subject;
    }
    
    public function set_table($table) {
        if ($table)
            $this->table = $table;
    }

    public function set_list($list) {
        $this->list = [];
        if (isset($this->table) && isset($this->subject)) {
            foreach ($list as $l) {
                if (is_null($l)) continue;
                $this->list[] = new xTerm([
                    'data_table' => $this->table,
                    'subject' => $this->subject,
                    'term' => $l,
                ]);
            }
        }
    }

    public function save_list($new_list) {
        $existing_terms = array_map(function ($t) {
            return $t->term;
        }, $this->list);
        
        $to_delete = array_filter($existing_terms, function ($t) use ($new_list) {
            return !in_array($t, $new_list);
        });
        foreach ($to_delete as $term) {
            $t = new xTerm([
                'data_table' => $this->table,
                'subject' => $this->subject,
                'term' => $term,
            ]);
            $t->delete();
        }
        
        $to_save = array_filter($new_list, function ($t) use ($existing_terms) {
            return !in_array($t, $existing_terms);
        });
        foreach ($to_save as $term) {
            $t = new xTerm([
                'data_table' => $this->table,
                'subject' => $this->subject,
                'term' => $term,
            ]);
            $t->save();
        }
        return true;
    }

    public function delete_list() {
        global $ID;

        $cmd = sprintf('DELETE FROM %s_terms WHERE data_table = %s AND subject = %s;', PROJECTTABLE, quote($this->table), quote($this->subject));
        if (! $res = pg_query($ID, $cmd)) {
            log_action('delete list error');
            return false;
        }
        return true;
    }

    public function count() {
        return count($this->list);
    }

    public function get_list($prop = null) {
        if (is_null($prop))
            return $this->list;
        elseif (!in_array($prop,['term','term_id']))
            return null;
        $ret = [];
        foreach ($this->list as $l) {
            $ret[] = $l->{"get_$prop"}();
        }
        return $ret;

    }
    
    public function get_list_and_count() {
        return [
            'list' => $this->get_list(),
            'count' => $this->total_count
        ];
    }

}

class xTerm {
    private $newTerm = false;
    private $newTaxon = false;
    private $props = [
        'data_table' => null, 
        'subject' => null,
        'term' => null,
        'term_id' => null,
        'taxon_db' => null,
        'status' => 'undefined',
        'parent' => null,
        'meta' => null,
        'wid' => null,
        'role_id' => null
    ];

    public function __construct($t) {
        if (!isset($t['wid']) || !$t['wid']) {
            $this->newTerm = true;
        }
        if (!isset($t['term_id']) || !$t['term_id']) {
            $this->newTaxon = true;
        }
        foreach (array_keys($this->props) as $p) {
            $this->$p = $t[$p] ?? $this->props[$p];
        }
    }

    public function get_term() {
        return $this->term;
    }

    public function get_taxon_db() {
        return $this->taxon_db;
    }

    public function get_term_id() {
        return $this->term_id;
    }

    public function save() {
        global $ID;
        
        if (!$this->term || !$this->subject) {
            return false;
        }
        if (!$this->newTaxon) {
            if ($this->newTerm) {
                $cmd = sprintf(
                    "INSERT INTO %s_terms (term_id, data_table, subject, term, status, role_id) VALUES (%d, %s, %s, %s, %s, %d) RETURNING wid, term_id;",
                    PROJECTTABLE, (int)$this->term_id, quote($this->data_table), quote($this->subject), quote($this->term), quote($this->status), quote($this->role_id) 
                );
            }
            else {
                $cmd = sprintf(
                    "UPDATE %s_terms SET data_table = %s, subject = %s, term = %s, status = %s, term_id = %d, role_id = %d WHERE wid = %d RETURNING wid, term_id;", 
                    PROJECTTABLE, quote($this->data_table), quote($this->subject), quote($this->term), quote($this->status), (int)$this->term_id, (int)$this->role_id, (int)$this->wid 
                );
            }
        }
        else {
            if ($this->newTerm) {
                $cmd = sprintf(
                    "INSERT INTO %s_terms (data_table, subject, term, status, role_id) VALUES (%s, %s, %s, %s, %d) RETURNING wid, term_id;", 
                    PROJECTTABLE, quote($this->data_table), quote($this->subject), quote($this->term), quote($this->status), quote($this->role_id)
                );
            }
            else {
                $cmd = sprintf(
                    "UPDATE %s_terms SET data_table = %s, subject = %s, term = %s, status = %s, term_id = %s, role_id = %d WHERE wid = %d RETURNING wid, term_id;", 
                    PROJECTTABLE, quote($this->data_table), quote($this->subject), quote($this->term), quote($this->status), 'DEFAULT', (int)$this->role_id, (int)$this->wid 
                );
            }
        }
        if (!$res = pg_query($ID, $cmd)) {
            log_action("Word save failed: $cmd",__FILE__,__LINE__);
            return false;
        }
        $results = pg_fetch_assoc($res);
        $this->wid = $results['wid'];
        $this->term_id = $results['term_id'];
        return true;
        
    }
    
    public function delete() {
        global $ID;
        if ($this->newTerm && $this->data_table && $this->subject && $this->term) {
            $cmd = sprintf("DELETE FROM %s_terms WHERE data_table = %s AND subject = %s AND term = %s;", 
                PROJECTTABLE, 
                quote($this->data_table),
                quote($this->subject),
                quote($this->term) 
            );
        }
        elseif (!$this->newTerm) {
            $cmd = sprintf("DELETE FROM %s_terms WHERE wid = %d;", PROJECTTABLE, (int)$this->wid );
        }
        else {
            return false;
        }
        if (!$res = pg_query($ID, $cmd)) {
            log_action('Word deletion failed',__FILE__,__LINE__);
            return false;
        }
        return true;
    }
}

class Term extends Entity {

    protected $tableName = PROJECTTABLE . '_terms';
    protected $mode = 'insert';
    protected $pk_col = 'term_id';

    public $term_id;
    public $data_table;
    public $subject;
    public $term;
    public $description;
    public $status;
    public $taxon_db;
    public $user_id;
    public $parent_id;
    
    function __construct() {
        parent::__construct();
    }
}

class Tree {

    public $tree;
    private $data;

    public function __construct($rootId, $depth = null)
    {
        $this->data = $this->fetchAllTermsRecursive($rootId, $depth);
        $this->tree = [$this->findRootElement($this->data, $rootId)];
        $this->tree[0]['children'] = $this->buildTree($this->data, $rootId, []);
    }

    public function getTree($rootId) {
        return $this->tree;
    }

    private function findRootElement($data, $rootId) {
        foreach ($data as $term) {
            if ($term['id'] == $rootId) {
                return $term;
            }
        }
        return null;
    }

    private function fetchAllTermsRecursive($rootId, $depth) {
        global $ID;
        $col_list = "t.term_id as id, t.term as label, t.parent_id";
        $depthCondition = $depth !== null ? sprintf("WHERE tt.depth <= %d", $depth) : "";
        if (is_null($rootId)) {
            $cmd = sprintf("SELECT %s FROM %s_terms t WHERE parent_id IS NULL;", $col_list, PROJECTTABLE);
        }
        else {
            $cmd = sprintf("WITH RECURSIVE term_tree AS (
                        SELECT %1\$s, 1 AS depth FROM %2\$s_terms t
                        WHERE term_id = %3\$d
                        UNION ALL
                        SELECT %1\$s, tt.depth + 1
                        FROM %2\$s_terms t
                        INNER JOIN term_tree tt ON tt.id = t.parent_id
                        %4\$s
                    )
                    SELECT * FROM term_tree;", $col_list, PROJECTTABLE, $rootId, $depthCondition);
        }
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            log_action(pg_last_error($ID), __FILE__, __LINE__);
            return [];
        }
        if (pg_num_rows($res) == 0) {
            return [];
        }
        $results = pg_fetch_all($res);
        return $results;
    }

    private function buildTree(array &$terms, $parentId, $currentPath) {
        $branch = [];
        
        foreach (array_filter($terms, function ($t) use ($parentId) { return $t['parent_id'] == $parentId; }) as &$term) {
            
            if ($term['parent_id'] == $parentId) {
                $path = array_merge($currentPath, [$term['id']]);
                $term['path'] = $path;

                $children = $this->buildTree($terms, $term['id'], $path);
                if ($children || is_null($parentId)) {
                    $term['children'] = $children;
                }
                $branch[] = $term;
                unset($term); // remove to optimize memory usage
            }
        }

        return $branch;
    }

    public function deleteTreeTerms() {
        foreach ($this->data as $term) {
            $t = Term::first(['term_id', $term['id']]);
            $t->delete();
        }
    }
}

?>
