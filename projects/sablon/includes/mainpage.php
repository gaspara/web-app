<?php

if (defined('ADMIN_MAINPAGE') && (has_access('master') || (isset(ADMIN_MAINPAGE['user_ids']) && in_array($_SESSION['Tid'], ADMIN_MAINPAGE['user_ids'])) )) {
    $mainpage = constant('ADMIN_MAINPAGE');

    $MAINPAGE_TEMPLATE = $mainpage['template'];

    $MAINPAGE_PATH = getenv('PROJECT_DIR')."styles/mainpage/$MAINPAGE_TEMPLATE/";
    $MAINPAGE_VARS = $mainpage;
    $MAINPAGE_CUSTOM_SKELETON = $mainpage['custom_skeleton'] ?? false;
    
}
elseif (defined('MAINPAGE')) {
    
    $mainpage = constant('MAINPAGE');

    $MAINPAGE_TEMPLATE = $mainpage['template'];

    $MAINPAGE_PATH = getenv('PROJECT_DIR')."styles/mainpage/$MAINPAGE_TEMPLATE/";
    $MAINPAGE_VARS = $mainpage;
    $MAINPAGE_CUSTOM_SKELETON = $mainpage['custom_skeleton'] ?? false;
}

function includeMainpage() {
    global $MAINPAGE_TEMPLATE;

    require(getenv('PROJECT_DIR')."styles/mainpage/$MAINPAGE_TEMPLATE/$MAINPAGE_TEMPLATE.php");

}

?>
