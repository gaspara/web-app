<?php

    if (!isset($_SESSION['Tid']))
        $out = login_box();
    else {


        if (isset($MAINPAGE_VARS['sidebar2'])) {
            $boxes = preg_split('/\|/',$MAINPAGE_VARS['sidebar2']);
            $box_params = array();
            foreach ($boxes as $x) {
                $p = '';
                $t = $x;
                if (preg_match('/(\w+)\((.+)\)$/',$x,$m)) {
                    $p = $m[2];
                    $t = $m[1];
                }
                $params[$t] = $p;
            }
        }
        else {
            $boxes = array('uploads','modifications','records','evals','freq');
            $params = array('uploads'=>'','modifications'=>'','records'=>'','evals'=>'','freq'=>'');
        }

        $boxes = array_keys($params);
        
        $mf = new mainpage_functions();
        $out = "<h2>".t(str_user_statistics)."</h2>";
        $dobj = json_decode(get_profile_data(PROJECTTABLE,$_SESSION['Tcrypt']));

        if (in_array('uploads',$boxes)) {
            $out .= "<div class='downbox'><h3>".str_uploadcount."</h3>";
            $out .= "<ul class='boxul'>";
            $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'uplcount'});
            $out .= "</ul></div>";
        }

        if (in_array('modifications',$boxes)) {
            $out .= "<div class='downbox'><h3>".str_modcount."</h3>";
            $out .= "<ul class='boxul'>";
            $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'modcount'});
            $out .= "</ul></div>";
        }

        if (in_array('records',$boxes)) {
            $out .= "<div class='downbox'><h3>".str_rowcount."</h3>";
            $out .= "<ul class='boxul'>";
            $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'upld'});
            $out .= "</ul></div>";
        }

        if (in_array('evals',$boxes)) {
            $out .= "<div class='downbox'><h3>".str_valuations."</h3>";
            $out .= "<ul class='boxul'>";
            $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'validation'});
            $out .= "</ul></div>";
        }

        if (in_array('freq',$boxes)) {
            $out .= "<div class='downbox'><h3>".str_most_frequent_species."</h3>";
            $out .= "<ul class='boxul'>";
            $out .= sprintf("<li>%s</li>",$mf->stat_species_user($params['freq']));
            $out .= "</ul></div>";
        }


    }
    echo $out;

?>
