<?php

function supervisor_log($status,$DATABASE,$REVISION,$CMD,$sqlid,$wd) {
    global $BID;

    $out = '';
    $cmd = sprintf("INSERT INTO supervisor (status,datum,database,revision,sqlcmd,sqlid,project) VALUES ('%s',now(),%s,%s,%s,%d,%s)",
        $status,quote($DATABASE),quote(trim($REVISION)),quote(trim($CMD)),$sqlid,quote($wd));

    $execute_status = pg_query($BID,$cmd);
    $e = pg_last_error($BID);
    if ($e) {
        $out .= "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
    }
    return $out;
}
function run_php($SQLC,$PROJECT,$DATABASE,$RUNPHP,$REVISION,$project) {

    $out = '';
    $sql_done = array();

    $sqlid = $SQLC;
    $pdir = "";
    if ($PROJECT!='') {
        if ($PROJECT=='all') 
            $pdir = $project;
        else
            $pdir = $PROJECT;

        include_once(OB_ROOT.'projects/'.$pdir.'/local_vars.php.inc');

        if (!$GID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
            $out .= "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to GIS database with biomaps.</div>";
    }
    
    $wd = "";
    if ($DATABASE=='biomaps') {
        # ...
    } elseif ($DATABASE=='gisdata') { 
        # ...
    }
    elseif ($DATABASE=='project') { 
        if ($PROJECT!='') {
            if ($PROJECT=='all') 
                $wd = $project;
            else
                $wd = $PROJECT;
        } 
    }
    chdir(OB_ROOT.'projects'.'/'.$wd);

    $h = fopen("run_supervisor_cmds.php","w");
    fwrite( $h, "<?php\n" );

    foreach(preg_split("/((\r?\n)|(\r\n?))/", $RUNPHP) as $line){
        fwrite( $h, $line."\n" );
    }
    fwrite( $h, "?>" );
    fclose($h);
    require("run_supervisor_cmds.php");

    if ($retval!==true) {
        $out .= "<div style='padding:10px;background-color:orange;color:white'>".json_encode($retval)."</div>";
        $status = 'false'; 
    } else {
        $status = 'true';
        $out .= "<div style='padding:10px;background-color:green;color:white'>cmd done.</div>";
    }
    
    $out .= supervisor_log($status,$DATABASE,$REVISION,$RUNPHP,$sqlid,$wd);

    return $out;
}
function save_php_status($SQLC,$PROJECT,$DATABASE,$RUNPHP,$REVISION,$project) {
    $out = '';

    $sqlid = $SQLC;
    $pdir = "";

    if ($DATABASE=='project') {
        if ($PROJECT!='') {
            if ($PROJECT=='all') 
                $pdir = $project;
            else
                $pdir = $PROJECT;
        }
    }

    if ($pdir!='')
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",
            quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
    else
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",
            quote($DATABASE),quote(trim($REVISION)),$sqlid);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        if ($pdir!='')
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",
                quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
        else
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",
                quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
    } else {

        $out .= supervisor_log('true',$DATABASE,$REVISION,$RUNPHP,$sqlid,$pdir);
    }

    $mark_as_read_status = pg_query($BID,$cmd);
    $e = pg_last_error($BID);

    if ($e) {
        $out .= "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
    } else {
        $out .= "<div style='padding:10px;background-color:green;color:white'>done</div>";
    
    }

    return $out;
}
function run_cmd($SQLC,$PROJECT,$DATABASE,$RUNCMD,$REVISION,$project) {
    global $BID;

    $out = '';
    $sql_done = array();

    $sqlid = $SQLC;
    $pdir = "";
    $wd = "";
    
    if ($DATABASE=='project') { 
        if ($PROJECT!='') {
            if ($PROJECT=='all') 
                $wd = $project;
            else
                $wd = $PROJECT;
            
        } 
        chdir(OB_ROOT.'projects'.'/'.$wd);
    } else {
        chdir(OB_ROOT_SITE);
    }

    $fp = fopen('run_supervisor_cmds.sh', 'w');
    foreach(preg_split("/((\r?\n)|(\r\n?))/", $RUNCMD) as $line){
        fwrite($fp,$line."\n");
    } 
    fclose($fp);

    $output=null;
    $retval=null;
    exec('sh ./run_supervisor_cmds.sh 2>&1', $output, $retval);

    if ($retval) {
         $out .= "<div style='padding:10px;background-color:orange;color:white'>".json_encode($output)."</div>";
        $status = 'false'; 
    } elseif ($retval===false) {
        $out .= "<div style='padding:10px;background-color:orange;color:white'>".json_encode($output)."</div>";
        $status = 'false'; 
    } else {
        $status = 'true';
        $out .= "<div style='padding:10px;background-color:green;color:white'>cmd done.</div>";
    }

    $out .= supervisor_log('true',$DATABASE,$REVISION,$RUNCMD,$sqlid,$wd);
        
    return $out;
}

function save_cmd_status($SQLC,$PROJECT,$DATABASE,$RUNCMD,$REVISION,$project) {
    global $BID;

    $out = "";
    $sqlid = $SQLC;
    $pdir = "";

    if ($DATABASE=='project') {
        if ($PROJECT!='') {
            if ($PROJECT=='all') 
                $pdir = $project;
            else
                $pdir = $PROJECT;
        }
    }

    if ($pdir!='')
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",
            quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
    else
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",
            quote($DATABASE),quote(trim($REVISION)),$sqlid);
    $res = pg_query($BID,$cmd);

    if (pg_num_rows($res)) {
        if ($pdir!='')
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",
                quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
        else
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",
                quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
    } else {

        $out .= supervisor_log('true',$DATABASE,$REVISION,$RUNCMD,$sqlid,$pdir); 
    }

    $mark_as_read_status = pg_query($BID,$cmd);
    $e = pg_last_error($BID);

    if ($e) {
        $out .= "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
    } else {
        $out .= "<div style='padding:10px;background-color:green;color:white'>done</div>";
    
    }

    return $out;
}
function run_sql($SQLC,$PROJECT,$DATABASE,$RUNSQL,$REVISION,$project,$CONNECTION) {
    global $BID, $GID;

    $sql_done = array();

    $sqlid = $SQLC;
    $pdir = "";
    $out = "";
    
    if ($DATABASE=='biomaps') {
        # System level commands
        $con = array($BID);
    } elseif ($DATABASE=='gisdata') { 
        # Project level commands, but running on different hosts
        $gisdb_hosts = gisdb_hosts;
        $gisdb_dbs = gisdata_dbs;

        $con = array();
        for($h=0;$h<count($gisdb_hosts);$h++) {
            $host = $gisdb_hosts[$h];
            $db = $gisdb_dbs[$h];
            if (!$GID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,$db,$host)) 
                $out .= "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to $db database on $host with ".biomapsdb_user.".</div>";
            $con[] = $GID;
        }
    } elseif ($DATABASE=='project') { 
        # Project level commands, but running on gisdb hosts

        if ($PROJECT!='') {
            if ($PROJECT=='all') 
                $pdir = $project;
            else
                $pdir = $PROJECT;
            include_once(OB_ROOT.'projects/'.$pdir.'/local_vars.php.inc');
            #if (!$ID = PGconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
            #    echo "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to GIS database with gisdata user.</div>";
            #$con = $ID;

            # it is better to use superuser...
            if (!$GID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
                $out .= "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to ".gisdb_name." database on ".gisdb_host." with ".biomapsdb_user.".</div>";
            $con = array($GID);
            
            # There is command to run on biomaps but called from a project
            if ($CONNECTION=='biomaps') { 
                $con = array($BID);
            }
        }
    }

    foreach ($con as $connection) {
        $res = pg_query($connection,$RUNSQL);
        $e = pg_last_error($connection);
        if ($e) {
            $out .= "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
            $status = 'false'; 
        } elseif ($e===false) {
            $out .= "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
            $status = 'false'; 
        } else {
            $status = 'true';
            $out .= "<div style='padding:10px;background-color:green;color:white'>sql done.</div>";
        }

        $out .= supervisor_log($status,$DATABASE,$REVISION,$RUNSQL,$sqlid,$pdir);
    }

    return $out;
}

function save_sql_status($SQLC,$PROJECT,$DATABASE,$RUNSQL,$REVISION,$project) {

    global $BID;

    $out = '';
    $sqlid = $SQLC;
    $pdir = "";
    if ($DATABASE=='project') {
        if ($PROJECT!='') {
            if ($PROJECT=='all') 
                $pdir = $project;
            else
                $pdir = $PROJECT;
        }
    }

    if ($pdir!='')
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",
            quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
    else
        $cmd = sprintf("SELECT 1 FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",
            quote($DATABASE),quote(trim($REVISION)),$sqlid);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        if ($pdir!='')
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project=%s",
                quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
        else
            $cmd = sprintf("UPDATE supervisor SET status='true' WHERE database=%s AND revision=%s AND sqlid=%d AND project IS NULL",
                quote($DATABASE),quote(trim($REVISION)),$sqlid,quote($pdir));
    } else {

        $out .= supervisor_log(true,$DATABASE,$REVISION,$RUNSQL,$sqlid,$pdir);
    }

    $mark_as_read_status = pg_query($BID,$cmd);
    $e = pg_last_error($BID);

    if ($e) {
        $out .= "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
    } else {
        $out .= "<div style='padding:10px;background-color:green;color:white'>done</div>";
    
    }
    return $out;
}

function project_settings_update($vars, $project) {
    global $BID;

    $cmd = sprintf('UPDATE projects SET %1$s WHERE project_table=%2$s',
        implode(",",$vars),quote( $project ));
    pg_query($BID,$cmd);
    $e = pg_last_error($BID);

    if ($e) {
        return "<div style='padding:10px;background-color:orange;color:white'>$e</div>";
    }
}

function message_template_update() {
    global $BID;

    $out = '';

    $t_url = 'https://raw.githubusercontent.com/OpenBioMaps/translations/master/message_templates/templates_list.csv';
    $data = file_get_contents($t_url);
    $t = preg_split("/\n/",$data);
    $langs = preg_split("/,/",array_shift($t));
    
    foreach ($langs as $ll) {
        foreach($t as $ta) {
            if (trim($ta)=='') continue;
            $t_url = "https://raw.githubusercontent.com/OpenBioMaps/translations/master/message_templates/$ll/$ta.html";
            $data = file_get_contents($t_url);
            $t_file = OB_RESOURCES . "../root-site/translations/message_templates/$ll/$ta.html";
            file_put_contents($t_file,$data);
        }
    }

    $dir = OB_RESOURCES . '../root-site/translations/message_templates/';
    if (!file_exists($dir)) {
        $out .= "<div style='padding:10px;background-color:orange;color:white'>templates dir not found</div>";
    }
    else {
        $languages = glob($dir . '*' , GLOB_ONLYDIR);
        $cmd = "BEGIN;";
        $cmd .= "DELETE FROM message_templates WHERE project_table IS NULL;";
        foreach ($languages as $lang_path) {
            $lang = substr($lang_path, -2);
            $templates = [];
            foreach (glob($lang_path . '/*') as $file) {
                preg_match('/\/(\w+)\.html$/m', $file, $matches);
                if (count($matches))
                    $templates[$matches[1]] = file_get_contents($file);
            }

            $templates = array_map(function($template) {

                if (preg_match('/<subject>(.*)<\/subject>(.*)$/s', $template, $matches)) { 
                    $subject = $matches[1];
                    $message = trim($matches[2]);

                    return compact('subject','message');
                }
                else {
                    return false;
                }
            }, $templates);

            foreach ($templates as $name => $t) {
                if ($t) {
                    $cmd .= sprintf("INSERT INTO message_templates (name, lang, subject, message) VALUES (%1\$s, %2\$s, %3\$s, %4\$s);",
                        quote($name),
                        quote($lang),
                        quote($t['subject']),
                        quote($t['message'])
                    );
                }
            }
        }
        $cmd .= "COMMIT;";
        pg_query($BID,$cmd);


        $out .= "<div style='padding:10px;background-color:green;color:white'>Message templates updated.</div>";
    }
    return $out;
}
function recurse_copy($src,$dst) {

    if (basename($dst) == 'private') {
        echo "<span style='background-color:orange'>$dst is protected against overwrite.</span><br>";
        return false;
    }

    $done = true;
    $dir = opendir($src); 
    @mkdir($dst); 


    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                $done = recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                #if (!$done)
                #    echo "<span style='background-color:orange'>Copy $file failed.</span> ";
            } 
            else { 
                if (file_exists($src . '/' . $file)) {
                    $done = copy($src . '/' . $file,$dst . '/' . $file); 
                    if (!$done)
                        echo "<span style='background-color:orange'>Copy $dst/$file failed.</span><br>";
                }
            } 
        } 
    } 
    closedir($dir);

    if ($done)
        return true;
    else
        return false;
}
function recurse_unlink($src) { 
    
    if (basename($src) == 'private') {
        echo "<span style='background-color:orange'>private directory cannot be unlinked.</span><br>";
        return false;
    }
    
    $dir = opendir($src);
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                $done = recurse_unlink($src . '/' . $file); 
                #if (!$done)
                #    echo "<span style='background-color:orange'>Delete directory $src/$file failed.</span> ";
            } 
            else { 
                $done = unlink($src . '/' . $file); 
                #if (!$done)
                #    echo "<span style='background-color:orange'>Unlink $src/$file failed.</span> ";
            } 
        } 
    } 
    closedir($dir); 
    $done = true;
    if (is_file($src))
        $done = unlink($src); 
    elseif(is_dir($src))
        $done = rmdir($src);


    if (!$done) {
        echo "<span style='background-color:red'>Unlink $src failed.</span><br>";
        return false;
    } else
        return true;
}
function preview_file($file)
{
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    if (in_array($ext,array('map','php','md','js','txt','inc','css','json','sql','version', 'mustache'))) {
        $lines = implode(range(1, count(file($file))), '<br />');
        $content = highlight_file($file, true);    
        echo "<table><tr><td class=\"num\" style='font-size:11px'>\n$lines\n</td><td><div style='font-size:11px;'>\n$content\n</td></tr></table>";
    }
    elseif (in_array($ext,array('gif','png','jpg','ico'))) {
        $imageData = base64_encode(file_get_contents($file));
        echo '<img src="data:image/jpeg;base64,'.$imageData.'">';
    } else {
        echo "No preview available<br>";
    }
}


/* Oauth status check
 * */
function oauth($result,$project) {
    global $BID;

    $j = json_decode($result);

    $cmd = sprintf("SELECT users.user,\"status\",email,id,username,terms_agree,user_status
        FROM users
            LEFT JOIN oauth_access_tokens oa ON (LOWER(oa.user_id)=LOWER(email))
            LEFT JOIN project_users pu ON (pu.user_id=id)
            WHERE access_token='%s' AND pu.project_table='%s' GROUP BY users.id, pu.user_status",
        $j->{'access_token'},
        $project);

    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);

        // parked users!!
        if ($row['status'] == '0') {
            return false;
        }

        if ( $row['user_status'] == 'master' )
            return true;
    }

    return false;
}


/* Validate JSON
 * used in geomtest.php
 * */
function is_json($str){
    return json_decode($str) != null;
}
/* A simple web-get function */
function wget($url,$header) {
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $result = curl_exec($curl);
        curl_close($curl);
    } else {
        $result = common_message('error','Curl error');
    }
    return $result;
}
function quote($text) {
    if ($text=='NULL' or trim($text)=='') return "NULL";

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $random_delimeter = substr( str_shuffle( $chars ), 0, 16 );
    $text = '$'.$random_delimeter.'$'.$text.'$'.$random_delimeter.'$';

    return $text;
}
/* utf8 ucfirst
 * glue arbitray arguments into a string
 * */
function t() {
    $encoding='UTF-8';
    mb_internal_encoding($encoding);
    $s = func_get_args();
    $string = implode(' ',$s);
    if ($string=='') {
        return str_undefined;
    }
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    if ($firstChar!='')
        return mb_strtoupper($firstChar, $encoding) . $then;
    else {
        if (is_array($s)) return "";
        else return $s;
    }
}
// Postgre SQL functions
function PGconnectSQL($db_user,$db_pass,$db_name,$db_host) {
  #global $db_user, $db_pass, $db_name, $db_host;
  $port = (defined('POSTGRES_PORT')) ? POSTGRES_PORT : '5432';
  $conn=pg_connect("host=$db_host port=$port user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
  pg_set_client_encoding( $conn, 'UTF8' );
  return $conn;
}
function has_access($action, $table = PROJECTTABLE) {

    if (!isset($_SESSION['Tid'])) {
        return FALSE;
    }

    $user_class = $_SESSION['Tstatus'];
    $roles = $_SESSION['Tgroups'];

    if (is_founder($table)) {
        return TRUE;
    }

    // master allways has access to everythings
    if ($user_class == 'master') {
        return TRUE;
    }

    return FALSE;

}
function is_founder($table = PROJECTTABLE) {
    global $BID;

    if (!isset($_SESSION['Tid'])) {
        return FALSE;
    }
    $cmd = sprintf("SELECT 1 FROM projects WHERE admin_uid='%d' AND project_table='%s'",$_SESSION['Tid'],$table);
    $res = pg_query($BID,$cmd);
    return (pg_num_rows($res));
}

function identifyHash($hash) {
    if (strpos($hash, '$2y$') === 0 || strpos($hash, '$2b$') === 0) {
        return 'bcrypt';
    } elseif (strpos($hash, '$1$') === 0) {
        return 'md5';
    } elseif (strpos($hash, '$5$') === 0) {
        return 'sha-256';
    } elseif (strpos($hash, '$6$') === 0) {
        return 'sha-512';
    } else {
        return 'unknown';
    }
}

function apache_md5($password, $salt) {
    $len = strlen($password);
    $context = $password . '$apr1$' . $salt;
    $binary = pack('H32', md5($password . $salt . $password));

    for ($i = $len; $i > 0; $i -= 16) {
        $context .= substr($binary, 0, min(16, $i));
    }

    for ($i = $len; $i > 0; $i >>= 1) {
        $context .= ($i & 1) ? chr(0) : $password[0];
    }

    $binary = pack('H32', md5($context));

    for ($i = 0; $i < 1000; $i++) {
        $new = ($i & 1) ? $password : substr($binary, 0, 16);
        if ($i % 3) $new .= $salt;
        if ($i % 7) $new .= $password;
        $new .= ($i & 1) ? substr($binary, 0, 16) : $password;
        $binary = pack('H32', md5($new));
    }

    $passwd = '';
    $passwd .= to64((ord($binary[0]) << 16) | (ord($binary[6]) << 8) | ord($binary[12]), 4);
    $passwd .= to64((ord($binary[1]) << 16) | (ord($binary[7]) << 8) | ord($binary[13]), 4);
    $passwd .= to64((ord($binary[2]) << 16) | (ord($binary[8]) << 8) | ord($binary[14]), 4);
    $passwd .= to64((ord($binary[3]) << 16) | (ord($binary[9]) << 8) | ord($binary[15]), 4);
    $passwd .= to64((ord($binary[4]) << 16) | (ord($binary[10]) << 8) | ord($binary[5]), 4);
    $passwd .= to64(ord($binary[11]), 2);

    return '$apr1$' . $salt . '$' . $passwd;
}

function to64($v, $n) {
    $ITOA64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    $ret = '';
    while (--$n >= 0) {
        $ret .= $ITOA64[$v & 0x3f];
        $v >>= 6;
    }
    return $ret;
}
?>
