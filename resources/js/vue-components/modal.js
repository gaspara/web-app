const vModal = Vue.component('v-modal', {
  props: {
    id: String,
    cls: Array,
    hideSaveBtn: {
        type: Boolean,
        default: false
    }, 
    saveBtnLabel: {
        type: String,
        default: 'Save'
    },
    hideCancelBtn: {
        type: Boolean,
        default: false
    }, 
    cancelBtnLabel: {
        type: String,
        default: 'Cancel'
    },
  },
  computed: {
    clas () {
      let str = 'modal2-mask '
      if (this.cls) {
        str += this.cls.join(' ')
      }
      return str
    }
  },
  template: `
    <transition name="modal2">
        <div :id="id" :class="clas">
          <div class="modal2-wrapper">
            <div class="modal2-container">
              <div class="modal2-header">
                <slot name="header"> default header </slot>
              </div>
              <div class="modal2-body">
                <slot name="body"> default body </slot>
              </div>
              <div class="modal2-footer">
                <slot name="footer">
                </slot>
                <button v-if="!hideCancelBtn" type="button" class="pure-button" @click="$emit('close')"> {{ cancelBtnLabel }} </button> 
                <button v-if="!hideSaveBtn" type="button" class="pure-button button-success" @click="$emit('save')"> {{ saveBtnLabel }} </button>
              </div>
            </div>
          </div>
        </div>
    </transition>
    `
})

export { vModal }
