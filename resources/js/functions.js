/* uploader.js 
 * paging if uploded file rows more than 200
 * */
function storeTblValues(id)
{
    let TableData = new Array();
    let columns = $('#'+id).children('.thead').children('.tr:first').find('.th').length;
    //var header = $('#'+id).children('thead').children('tr:first');
    let i=0;
    let oList = new Array();
    let tList = new Array();

    //$("#upload-data-table > .thead > .tr").each(function(){
    //$("#upload-data-table > .tbody > .tr").each(function(){
    $('#'+id+' .tr').each(function(row, tr) {

        let p = {}; //new Array();
        let autoskip = 0;
        if (row=='2') {return;} // skip autofill row
        for (let i = 0; i < columns; i++) {
            
            //console.log($(tr).children('.td:eq('+i+')').data('column'));

            if (row=='0') {
                tList.push($(tr).children('.th:eq('+i+')').html());
            } 
            else if (row=='1') {
                oList.push($(tr).children('.th:eq('+i+')').html());
            } 
            else {
                if (i==0) {
                    if ( $(tr).children('.td:eq('+i+')').find('input').is( ":checked" ) )
                        //p.push('1');
                        p['skip_row'] = '1';
                    else
                        //p.push('0');
                        p['skip_row'] = '0';

                    if ( $(tr).children('.td:eq('+i+')').find('.skip').val()==1)
                        autoskip = 1;
                } else {
                    var type = $(tr).children('.td:eq('+i+')').find(":input").getType();
                    if (type=='text') {

                        p[$(tr).children('.td:eq('+i+')').data('column')] = $(tr).children('.td:eq('+i+')').find('input').val();
                        //p.push($(tr).children('.td:eq('+i+')').find('input').val());
                    }
                    else if (type=='select') {
                        if (typeof $(tr).children('.td:eq('+i+')').find('select > option:selected').val() === 'undefined')
                            p[$(tr).children('.td:eq('+i+')').data('column')] = $(tr).children('.td:eq('+i+')').find('select > option:eq(0)').val();
                            //p.push($(tr).children('.td:eq('+i+')').find('select > option:eq(0)').val());
                        else
                            p[$(tr).children('.td:eq('+i+')').data('column')] = $(tr).children('.td:eq('+i+')').find('select').val();
                            //p.push($(tr).children('.td:eq('+i+')').find('select').val());

                    }
                    else {
                        //console.log(type);
                        //non handled input type yet
                        p.push('');
                    }
                }
            }
        }
        if (autoskip == '1') {
            return;
        }

        if (row>2) {
            // a header 3 sorát eldobjuk
            let n = row-3
            TableData[n]=p;
        }
    }); 

    //TableData.shift();  // first row will be empty - so remove
    const v = {
        title:tList,
        header:oList,
        data:TableData
    }
    return v;
}
/* :input type type
 *
 * */
$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
/**/
var rightTableWith = $("#rightTable").width();

function sortevent(){
    $( "ul.oList" ).sortable({
        items: "li:not(.ui-state-disabled)",
        cancel: ".oListDisabled",
        connectWith: "ul",
        dropOnEmpty: true,
        snap: "ul",
        placeholder: "drop-highlight",
        helper: function( event ) {
            let text = $(this).find('.field-title').text();
            return $( "<div style='width:auto;height:auto;padding:2px;border:1px solid gray;background:white;opacity:0.5'>"+text+"</div>" );
        },
        cursorAt: { bottom:0,left:0 },
        start: function (event, ui ) {
            $(this).closest('.th').find('.drophere').show();
        
        },
        receive: function( event, ui ) {
            let k=1;
            let p;

            $(this).closest('.th').find('.drophere').hide();
            //overdrop: put back element to end of list
            $(this).children().each(function(){
                let attr = $(this).attr('id');
                if (typeof attr !== typeof undefined && attr !== false) {
                    if ($(this).attr('id')!=ui.item.attr('id')) {
                        let text = $(this).html();
                        let id = $(this).attr('id');
                        $(".sList").append("<li id='"+ id +"'>"+ text +"</li>");
                    }
                }
            });
            //put element
            $(this).html("<li id='"+ ui.item.attr('id') +"'>"+ ui.item.html() +"</li>");
 
            let colIndex = $(this).closest('.th').index()+1;
            let colName = $("#upload-data-table > .thead > .tr:eq(0) > .th:nth-child("+colIndex+")").find('.colName').text();
            // e.g. {"wkt":"obm_geometry","ele":"hely"} or null
            let a_theader = JSON.parse( $("#a_theader").val() );
            if (a_theader == null) {
                a_theader = {};
            }
            
            let index = getKeyByValue(a_theader,ui.item.attr('id'));
            
            a_theader[colName] = ui.item.attr('id');
            if (typeof index !== typeof undefined)
                a_theader[index] = "";

            $("#a_theader").val(JSON.stringify(a_theader));

            paging(event,0);
            $("#sheetDataPage_counter").val(0); // set current page number
        }, 
        stop: function( event, ui ) {
            if ($("ul.sList li").length>0) {
                $("#leftTable").show();
                if ( rightTableWith !== null)
                    $("#rightTable").width(eval(rightTableWidth + $("#leftTable").width()));
            }

        }
    });
}
function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}
function ConvertDMSToDD(degrees, minutes, seconds, direction) {
    let dd = Number(degrees) + Number(minutes)/60 + Number(seconds)/(60*60);

    if (direction == "S" || direction == "W") {
        dd = dd * -1;
    } // Don't do anything for N or E
    return dd;
}
/* main.js
 * upload picture response function
 * */
function UplResponse(data) {
    const retval = jsendp(data);
    if (retval['status']=='error') {
        $( "#dialog" ).dialog( "open" );
        $( "#error" ).html(retval['message']);
    } else if (retval['status']=='fail') {
        $( "#dialog" ).dialog( "open" );
        $( "#dialog" ).text("An error occured.");
    } else if (retval['status']=='success') {
        let data = retval['data'];

        let m = data['file_names'];
        let r = data['err'];
        for(k = 0; k < m.length; k++) {
            //get photo thumbnail url
            if (typeof r[k] !== 'undefined' && r[k]!=''){
                // check_file_exist() return 0
                // file exists
                //continue;
                //console.log(m[k]+": "+r[k]);
            }
            $.post("getphoto", {ref:m[k],getthumbnailimage:1,url:1}, function(imgurl){
                let j = JSON.parse(imgurl);
                let fp = j['file_properties'];
                for (let f = 0; f < fp.length; f++) {
                    let img = document.createElement('img');
                    let upf = fp[f];
                    // should be changed to local
                    img.src = projecturl + '/images/32px_none.png';
                    $.ajax({
                        url: 'ajax',
                        data: {
                            'get_mime': 1,
                            'thumb': 'yes',
                            'file': upf['filename'],
                            'size': 32
                        },
                        type: 'POST'
                    }).done(function(r){
                          img.src = r;
                    }).fail(function(){
                          console.log('ajax mime get failed');
                    });

                    img.setAttribute('class', 'thumb');
                    img.setAttribute('alt', upf['filename']);
                    if (typeof upf['exif'] !== 'undefined' && upf['exif']!='') {
                        let coords = extraxt_XYcoordinates_from_exif(upf['exif']);
                        img.setAttribute('data-coords',coords.join());
                        img.setAttribute('data-satellites',extraxt_satellites_from_exif(upf['exif']));
                    }

                    $("#uresponse").append("<a href='"+upf['url']+'/'+upf['filename']+"' class='photolink' id='gf_"+upf['id']+"'></a>");
                    $("#uresponse").find('a').last().html(img);
                    $("#uresponse").append(' ');
                    $("#uresponse").find('a').last().draggable({
                        revert: "invalid",
                        containment: "document",
                        helper: "clone",
                        cursor: "move"
                    });
                }
            });
        }
    } else {
        $( "#dialog" ).dialog( "open" );
        $( "#dialog" ).text("An error occured.");

    }
}
function extraxt_satellites_from_exif(exif) {
    const j = JSON.parse(exif);
    let satellites = '';
    if (typeof j['GPSSatellites'] !== 'undefined') {
        satellites = j['GPSSatellites'];
    }
    return satellites;
}
function extraxt_XYcoordinates_from_exif(exif) {
    const j = JSON.parse(exif);
    let coords = new Array();
    if (typeof j['GPSLongitude'] !== 'undefined') {
        let dp = new Array();
        for (let i=0;i<j['GPSLongitude'].length;i++) {
            dp.push(eval(j['GPSLongitude'][i]));
        }
        //25° 35' 13.11" E
        coords.push(dp[0]+'° '+dp[1]+"' "+dp[2]+'" '+j['GPSLongitudeRef']);
    }
    if (typeof j['GPSLatitude'] !== 'undefined') {
        let dp = new Array();
        for (let i=0;i<j['GPSLatitude'].length;i++) {
            dp.push(eval(j['GPSLatitude'][i]));
        }
        //25 deg 35' 13.11" E
        coords.push(dp[0]+'° '+dp[1]+"' "+dp[2]+'" '+j['GPSLatitudeRef']);
    }
    return coords;
}
function updateProgress (oEvent) {
  if (oEvent.lengthComputable) {
    let percentComplete = oEvent.loaded / oEvent.total;
    //console.log(percentComplete);
    // ...
  } else {
    // Unable to compute progress information since the total size is unknown
    //console.log(0);
  }
}
function transferComplete(evt) {
  //console.log("The transfer is complete.");
  // is it compatible with the any browser??
}
function transferFailed(evt) {
  //console.log("An error occurred while transferring the file.");
}
function transferCanceled(evt) {
  //console.log("The transfer has been canceled by the user.");
}
function alertresult(result) {
    const retval = jsendp(result);
    if (retval['status']=='error') {
        alert(retval['message']);
    } else if (retval['status']=='fail') {
        console.log(retval['data']);
        alert(retval['message']);
    } else if (retval['status']=='success') {
        alert('ok');
    } else {
        //bad answer
        alert(result);
    }
}

/* main.js
 * uploader.js
 * */
var Uploader = function () {
// file upload class
}
Uploader.prototype.after = function(arg){
    //do something after transfer complete
    this.doAfter = arg;
}
Uploader.prototype.complete = function(arg){
    //transfer complete function
    this.completeFunction = arg;
}
Uploader.prototype.button = function(id){
    //uploader button function
    this.buttonId = id;
    this.uc = $("#"+id).find('i').attr('class');
    $("#"+id).find('i').removeClass();
    $("#"+id).find('i').addClass('fa fa-spinner fa-spin');
}
Uploader.prototype.param = function(arg){
    //optional form paramter value
    this.optParam = arg;
}
Uploader.prototype.param2 = function(arg){
    //optional form paramter value
    this.optParam2 = arg;
}
Uploader.prototype.param3 = function(arg){
    //optional form paramter value
    this.optParam3 = arg;
}

Uploader.prototype.file_upload = function(files, ext, renameFiles = false){
    let responseFun = this.completeFunction || "";
    let button = this.buttonId || "";
    let uc = this.uc || "";
    let doAfter = this.doAfter || "";
    let optParam = this.optParam || "";
    let optParam2 = this.optParam2 || "";
    let optParam3 = this.optParam3 || "";
    
    // xhr version 
    let formData = new FormData();
    Object.keys(files).forEach((i) => {
        const filename = (renameFiles) ? replaceInvalidFilenameCharacters(files[i].name) : files[i].name;
        formData.append("files[]", files[i], filename);
    })
    formData.append("load-files", 1);
    formData.append("ext", optParam);
    formData.append("exu", optParam2);
    formData.append("pr3", optParam3);
    let xhr = new XMLHttpRequest();
    xhr.addEventListener("progress", updateProgress);
    xhr.addEventListener("load", transferComplete);
    xhr.addEventListener("error", transferFailed);
    xhr.addEventListener("abort", transferCanceled);
    xhr.open("POST", 'ajax');
    xhr.onreadystatechange = function (r) {
        let R = {'file_names':'','err':'','message':'','status':''};
        if (this.responseText!='') {
            try {
                R = JSON.parse(this.responseText);
            }
            catch(e) { 
                console.error('wrong formed response');
            }
	}
        if (xhr.readyState == 4) {
            //request finished and response is ready
            if (this.response == 2) {
                if (responseFun!='') {
                    //give AJAX response to function (R)
                    window[responseFun](R);
                    $("#"+button).find('i').removeClass();
                    $("#"+button).find('i').addClass('fa fa-refresh fa-spin');
                } else {
                    alert('Upload failed:' + "\n" + R['data']['file_names'].join("\n")+"\n"+R['message'].join("\n"));
                }
            } else {
                //'File received!';
                if (responseFun!=''){
                    window[responseFun](R);
                    $("#"+button).find('i').removeClass();
                    $("#"+button).find('i').addClass(uc);
                    eval(doAfter)
                } else {
                    //alert('Upload failed:' + "\n" + R['data']['file_names'].join("\n")+"\n"+R['message'].join("\n"));
                }
            }
        } else if (xhr.readyState == 0) {
            //request not initialized
            if (responseFun!='') {
                window[responseFun](R['data']['file_names']);
                $("#"+button).find('i').removeClass();
                $("#"+button).find('i').addClass(uc);
            } else {
                alert('Upload failed:' + "\n" + R['data']['file_names'].join("\n")+"\n"+R['message'].join("\n"));
            }
        } else {
            if (R['status']=='fail' || R['status']=='error') {
                console.log('Upload failed:' + "\n" + R['message']);
            }
            //'Processing...';
        }
    };
    xhr.send(formData);
}

/* uploader.js
 * web form geometry from map selection
 * */
function parseWKTStrings(ps,T) {
    T = T || new Array();
    // linestring length validation??
    let i, j, lat, lng, tmp, tmpArr,arr=[],o=0;
    let type = ps.match(/^[a-zA-Z]+/);
    if (T.length == 0)
        T = new Array("point", "linestring", "polygon", "multipoint", "multilinestring", "multipolygon");

    if(type!=null) {
        for (i=0;i<T.length;i++) {
            if(T[i].toUpperCase() === type[0].toUpperCase()) o = 1;   
        }
    } else return arr;

    if (o==0) return arr;

    //match '(' and ')' plus contents between them which contain anything other than '(' or ')'
    m = ps.match(/\([^\(\)]+\)/g);
    if (m !== null) {
        for (i = 0; i < m.length; i++) {
            //match all numeric strings
            tmp = m[i].match(/-?\d+\.?\d*/g);
            if (tmp !== null) {
                //convert all the coordinate sets in tmp from strings to Numbers and convert to LatLng objects
                for (j = 0, tmpArr = []; j < tmp.length; j+=2) {
                    lat = Number(tmp[j]);
                    lng = Number(tmp[j + 1]);
                    //tmpArr.push(new google.maps.LatLng(lat, lng));
                    tmpArr.push([lat,lng])
                }
                arr.push(tmpArr);
            }
        }
    }
    //array of arrays of LatLng objects, or empty array
    return arr;
}
/* ******************************************************************************
 *                                                                              *
 * maps.js functions                                                            *
 *                                                                              *
 * *****************************************************************************/
/*
 *
 * */
Array.prototype.contains = function(k) {
    for(let i=0; i < this.length; i++){
        if(this[i] === k) {
            return true;
        }
    }
    return false;
}
/* maps.js
 * upload - webform - geometry from map selection
 * set geometry sting ...
 * */
function setValue(val) {
    if(val=='') return false;

    let widgetId=geom_populate.substring(geom_populate.indexOf('_')+1,geom_populate.length);
    //js v.17
    //var [target,pos] = widgetId.split('-');
    let spl = widgetId.split('-');
    let target=spl[0];
    let pos=spl[1];
    if(target=='default')
        $("#default-"+pos).val(val);
    else if(target=='header') {
        //js v 1.7
        //var [rowIndex,colIndex] = pos.split(',');
        let spl=pos.split(',');
        let rowIndex=spl[0];
        let colIndex=spl[1];
        $("#upload-data-table .thead .tr:eq("+rowIndex+") .th:nth-child("+colIndex+")").find('input').val(val);
        //$("#upload-data-table thead tr:eq("+rowIndex+") th:nth-child("+colIndex+")").find('input').trigger('paste');
        let input = $("#upload-data-table .thead .tr:eq("+rowIndex+") .th:nth-child("+colIndex+")").find('input')
        fillfunction(input,'fill');
    } else if(target=='in') {
        /*
         * */
        $("#dc-"+pos).val(val);
    } else if(target=='sheet') {
        /*
         * */
        $("#dc-"+pos).val(val);
    } else {
        //normal
        //js v.17
        //var [rowIndex,colIndex] = pos.split(',');
        let spl=pos.split(',');
        let rowIndex=spl[0];
        let colIndex=spl[1];
        $("#upload-data-table .tbody .tr:eq("+rowIndex+") .td:nth-child("+colIndex+")").find('input').val(val);

        let this_autoskip = $("#upload-data-table .tbody .tr:eq("+rowIndex+")").find(".autoskip");
        if (this_autoskip.val()==1 & $("#upload-data-table .tbody tr:eq("+rowIndex+") .td:nth-child("+colIndex+")").find('input').val()!='') {
           this_autoskip.val(0);
           this_autoskip.removeClass('autoskip');
        }
    }
    $("#gpm").hide();
    mapwindow.close();
    return true;
}
/* Send query from MAP page (map-page.php)
 * Refreshing WMS query map layer after text ot spatial query (drawn or loaded)
 * */
function loadQueryMap(params) {

    $( "#dialog" ).dialog( "option", "position", { my: 'center', of: window } );
    $( "#dialog" ).dialog( "option", "title", obj.message );
    $( "#dialog" ).text(obj.processing_query+'...');
    let isOpen = $( "#dialog" ).dialog( "isOpen" );
    if(!isOpen) $( "#dialog" ).dialog( "open" );

    /* Sending query paramters to query_builder. It is returning with the data extent
     * */
    // disable wheelzoom after click query
    /* should be custom
     * if($("#wz").find('i').hasClass('fa-toggle-on')) {
            //console.log('a')
            $('#wz').find('i').toggleClass('fa-toggle-on');    
            $('#wz').find('i').toggleClass('fa-toggle-off');    
            $('#wz').removeClass('button-success');
            nav.disableZoomWheel();
    }*/

    $.post(projecturl+"includes/query_builder.php", params,
        function(data){

            /* clean previous results*/
            $('#error').html('');
            $('#matrix').html('');
            $('#scrollbar_header').hide().html("");
            $('#scrollbar').hide().html("");
            $("#scrollbar-header").hide();
            $('#scrollbar').hide();

            /* new results */
            if (data=='1') {
                // query id list ?and bbox? of drawn geoms
                $.post(projecturl+"includes/results_query.php", {'doo':1},
                    function(data) {
                    if (data >= 1) {
                        //if ($("#apptq").find('i').hasClass('fa-toggle-on')) {
                        //    LoadResults(); 
                        //}
                        //document.location.href='includes/queries.php?csv=1.csv';
                        LoadResults();
                        $('#clear-filters').css('background-color','darkorange');

                    } else {
                        //"There were no data in the query, try to change the query terms!"
                        //
                        let no_results_extension = '';
                        //var jo = JSON.parse('{"enabled":"true","text":"<br>Add data <a href=\\"localhost\/biomaps\/projects\/dinpiupload\/?form=57&type=web&set_fields=@jgf@\\">here?<\/a>"}');
                        let jo = JSON.parse(obj.no_res_exts);
                        if (jo['enabled']=='true') {
                            let gf = {
                                'obm_geometry': geom_here,
                            }
                            jgf = JSON.stringify(gf);
                            no_results_extension = jo['text'].replace('@jgf@',jgf);
                            no_results_extension = no_results_extension.replace(/#/g,"'");
                        }
                        $( "#dialog" ).html(obj.no_results_for_the_query + no_results_extension);
                    }
                });

            } else {
                $( "#dialog" ).html(obj.failed_to_assemble_query_string+' :('+"<br>"+data.replace(/\n/g,"<br>"));
            }
    });
}

// loading response data in various ways
// Loading 
//      - Summary
//      - Save as option
//      - Roll outputs
function LoadResults() {

    $( "#dialog" ).text( obj.preparation_of_results+'...' );

    // processing id list to create different kind of outputs
    $.post(projecturl+"includes/load_results.php",{'method':'default_view_result_method'},function(data) {
        /* Expected income
           {
            html: html content
            map: [ "17.4793833333333", "46.7897", "17.5394666666667", "46.89155" ]
            rolltable: {}
           }
        */
        let retval = jsendp(data);
        $("#matrix").html("");

        if (retval['status']=='error') {
            $( "#dialog" ).dialog( "close" );
            $( "#error" ).html(retval['message']);
            $( "#matrix" ).html('');
        } else if (retval['status']=='fail') {
            $( "#dialog" ).dialog( "close" );
            $( "#dialog" ).text("An error occured while loading the attributes.");
            $( "#matrix" ).html('');
        } else if (retval['status']=='success') {
            let v = retval['data'];
            let rolltable = v.rolltable;

            $( "#dialog" ).dialog( "close" );

            /* Render dynamic sized output for very long results
            */
            if (rolltable.length !== 0) {
                rr.set({
                    rowHeight:rolltable.rowHeight,
                    cellWidth:rolltable.cellWidth,
                    borderRight:rolltable.borderRight,
                    borderBottom:rolltable.borderBottom,
                    arrayType:rolltable.arrayType});
                let va = {w:'',i:''};
                try {
                    va = JSON.parse(rolltable.scrollbar_header);
                    rr.prepare(va['w'],va['i']);
                }
                catch(e) {
                    rr.prepare(500,'');
                }
                rr.render();
                $("#scrollbar").show();
                if ($(".scrollbar").height() <= 400) {
                    $("#drbc").height(600);
                } else {
                    $("#drbc").height(eval($(".scrollbar").height()+100));
                }
            }

            /* Refresh map's data layer source
             */
            for (let i=0;i<dataLayers.length;i++) {
                dataLayers[i].refresh();
            }
                
            /* Refresh map, zoom to result area
            */
            if (v.map.length && v.map[0] != '') {
                let ext = new ol.extent.boundingExtent([[v.map[0],v.map[1]],[v.map[2],v.map[3]]]);
                ext = new ol.proj.transformExtent(ext, ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857'));
                /*var x = ext.map(function(n){return n*0.0002});
                ext[0] = ext[0] - x[0];
                ext[1] = ext[1] + x[1];
                ext[2] = ext[2] + x[2];
                ext[3] = ext[3] - x[3];*/
                map.getView().fit(ext,{padding: [20, 20, 20, 20]});
            }
                
            /* Load static html contents: summary, download options, ... 
             * */
            $('#matrix').html(v.html);

            /* Load right side floating navigator
             * */
            $('#navbuttons').show();
            $("#navigator").show();
            let $div2blink = $("#navbuttons").find(".button-href");
            let blink_counter1 = 21;
            let backgroundInterval = setInterval(function(){
                $div2blink.toggleClass("button-success");
                blink_counter1--;
                if (blink_counter1==0) {
                    clearInterval(backgroundInterval);
                }
            },1100);
            let $div2blink3 = $("#mapfilters-roller");
            let blink_counter3 = 21;
            let backgroundInterval3 = setInterval(function(){
                $div2blink3.toggleClass("button-success");
                blink_counter3--;
                if (blink_counter3==0) {
                    clearInterval(backgroundInterval3);
                }
            },600);
            let $div2blink2 = $("#scrolldown");
            let blink_counter2 = 21;
            let backgroundInterval2 = setInterval(function(){
                $div2blink2.toggleClass("button-secondary");
                blink_counter2--;
                if (blink_counter2==0) {
                    clearInterval(backgroundInterval2);
                }
            },1100)
            //$("#sendQuery").toggleClass("button-success");

            /* ?
             * */
            $(document).trigger({
                type: 'LoadResults_ready',
            });
        }
    });
}
/* Megcsinál egy WFS lekérdezést
 * ---1. betölti a results_builder által visszaadott adatokat az adatmezőbe
   ---2. körül rajzolja a WFS query response pontokat 
   Ez most már csak a loadQuery ágban van használva
   A main.js.php-ból van meghívva
   */
function WFSGet(response) {
    let skip_processing = this.skip_processing;
    $( "#dialog" ).text("Loading data...");
    let isOpen = $( "#dialog" ).dialog( "isOpen" );
    if (!isOpen) $( "#dialog" ).dialog( "open" );
    //}
    // clear results area
    $('#error').html('');
    $('#matrix').html('');
    vectorLayer.getSource().clear();
    for (let i=0;i<dataLayers.length;i++) {
        /* Nem tudom az ImageWMS rétegeket lekapcsolni
        dataLayers[i].visibility = false;
        dataLayers[i].refresh();
        */
    }

    skip_processing = 'no';
    
    /*if (typeof xy != "undefined" && xy.lon) {
        var xylonlat = xy.lon+' '+xy.lat;
    } else 
        var xylonlat = '';*/
    //sending a request for processes wfs xml data
    $.post(projecturl+"includes/wfs_processing.php",{'skip_processing':skip_processing},
        function(data){
            //very long response???
            let v = {geom:'',error:''};
            try {
                v = JSON.parse(data);
            }
            catch(e) { 
                $( "#dialog" ).text("Invalid WFS XML response received.");
            }
            if(v.geom!='') {
                highlightFeatures(v.geom);
            } 

            /*
            {
                html: html content
                map: [ "17.4793833333333", "46.7897", "17.5394666666667", "46.89155" ]
                rolltable: {}
            }
            */
            if (v.error!='') {
                // Error handling
                if (v.error==='0')
                    $( "#dialog" ).text("No matching records found.");
                else {
                    $( "#dialog" ).text("While processing WFS XML response some errors occured.");
                    if (v.error.isArray) {
                        $.each(v.error,function(i, value){
                            $('#error').append(value + '<br>');
                        })
                    } else {
                        $('#error').html(v.error + '<br>');
                    }
                }
            } else {
                /* normal query on the map: text or geometry
                 * show results in the selected view type
                 * */
                $( "#dialog" ).text( "Loading attributes..." );
                $.post(projecturl+"includes/load_results.php",{method:'default_view_result_method'},function(data){                    
                    let retval = jsendp(data);
                    if (retval['status']=='error') {
                        $( "#dialog" ).dialog( "close" );
                        $( "#error" ).html(retval['message']);
                        $( "#matrix" ).html('');
                    } else if (retval['status']=='fail') {
                        //$( "#dialog" ).text(retval['message']);
                        $( "#dialog" ).dialog( "close" );
                        $( "#dialog" ).text("An error occured while loading the attributes.");
                        $( "#matrix" ).html('');
                    } else if (retval['status']=='success') {
                        v = retval['data']['rolltable'];
                        $( "#dialog" ).dialog( "close" );

                        rr.set({rowHeight:v.rowHeight,cellWidth:v.cellWidth,borderRight:v.borderRight,borderBottom:v.borderBottom,arrayType:v.arrayType});
                        let va = JSON.parse(v.scrollbar_header)
                        rr.prepare(va['w'],va['i']);
                        rr.render();
                        $("#scrollbar").show();
                        $("#navigator").show();
                        $('#matrix').html(v.res);
                        if ($(".scrollbar").height() <= 400)
                            $("#drbc").height(600);
                        else
                            $("#drbc").height(eval($(".scrollbar").height()+100));

                        $(document).trigger({
                            type: 'LoadResults_ready',
                        });
                    }
            });
        }
    });
}
/* create results object from wfs respond object 
 * it is removing the null values                  */
function wfsDataDump(f) {
    let out = {};
    for (let i in f) {
        if (new String(f[i]) != 'null') {
            out[i] = f[i];
        }
    }
    return out;
}
/* not used
 * */
function lbuf_reread() {
    $.post("ajax", {lbuf_reread:1},
        function(data){
            $('#load_selection').html(data);
    });
}
/* uploader.js*/

//autofill events for filling the current rows
//$('#upload-data-table').on("change paste propertychange",".fill",function(event){
function fillfunction(t,fun) {

    modal_ajax_stop('start');

    var idx = 1+(+t.closest('.th').index()); // post execute on SESSION['sheetData'] [nth col]
    if (idx == 1) {
        $( "#dialog" ).text('Invalid header request');
        var isOpen = $( "#dialog" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog" ).dialog( "open" );
        return;
    }

    var rowIndex;
    var autofill_data = t.val();

    var pattern = /`apply:\[(.+)\]/;
    var idmatch = new Array('','');

    if (fun!='fill') {
        if (pattern.test(fun)) {
            //set column index by header cell name
            var idmatch = pattern.exec(fun);
        }
    }

    if ($("#form_page_type").val() == 'web') {
        /* web form */
        var rowIndex_array = new Array();
        var t_array = new Array();
        $("#upload-data-table .td:nth-child("+idx+")").each(function() {

            if ( ! $(this).closest('.tr').hasClass('skippedRow')) {
                
                // uncheck autoskipped rows
                if ( $(this).closest('.tr').find('.autoskip').val()==1) {
                    $(this).closest('.tr').find('.autoskip').val(0);
                    $(this).closest('.tr').find('.autoskip').removeClass('autoskip');
                }

                rowIndex_array.push($(this).closest('.tr').index());
                t_array.push($(this));
                // update only one cell
            } else {
                console.log(3);
            }
        });
        $.post("ajax", {
            'upload_fill_function':fun,
            'autofill_data':autofill_data,
            'upload_fill_onecell':1,
            'rowindex':rowIndex_array,
            'colindex':idx,
            'idmatch':idmatch[1],
            'u_theader':$("#u_theader").val()
        }, 
            function(return_data) {
                j = JSON.parse(return_data);
                for (var i = 0; i<j.length ; i++) {
                    var data = j[i];
                    var t = t_array[i];
                    if (t.find('.ci').prop('type')=='text') {
                        t.find('.ci').val(data)
                    } else {
                        // label based value set for select menus
                        t.find('.ci option').each(function() {
                            if ($(this).text()==data) {
                                data = $(this).val();
                            }
                        });
                        t.find('.ci').val(data);
                    }
                    getNestedOptions(t.find('.ci'),'table');
                }

                modal_ajax_stop('end');
                auto_save_wait = 0;
        });

        return;
    }


    $('#upload-data-table,input').css( 'cursor', 'progress' );

    // update all cell values in the background
    $.post("ajax", {
        'upload_fill_function':fun,
        'upload_fill_column-id':idx,
        'idmatch':idmatch[1],
        'autofill_data':autofill_data,
        'a_theader':$('#a_theader').val(),
        'u_theader':$('#u_theader').val()
    }, function(data){
        $('#upload-data-table,input').css( 'cursor', 'initial' );
        var page = $("#sheetDataPage_counter").val(); // get current page number
        paging('',page,0);
        modal_ajax_stop('end');
        auto_save_wait = 0;
    });
}
/* obfun fill+ functions
 * */
function fillplus_on_selected(fun) {

    var pattern = /`apply:\[.*\]/;
    if (pattern.test(fun)) {
        var pattern = /`apply:\[.*\]([a-zA-Z0-9-.]+):?(.+)?/;
    } else {
        var pattern = /`apply:([a-zA-Z0-9-.]+):?(.+)?/;
    }
    if (pattern.test(fun)) {
        matches = pattern.exec(fun);
        
        $('#obfun-name').val(matches[1]);
        $('#obfun-params').val(matches[2]);
    
        $("#obfun-params").prop('disabled', true);
        $("#obfun-params").val('');
        if (matches[1]=='replace.match' || matches[1]=='replace.all') {
            $("#obfun-params").prop('disabled', false);
            $("#obfun-params").val('///');
        }
    }

    $('#apply').show();
    $('#fillit').hide();
    $('#obfun-close').show();

    $('#longin-function-editor').css('display','inline-block');
    $('#longin-function-editor').show();
    $('#longin-function-editor').focus();
    $('#longin').hide();
    return; 
}

/* populate nested select inputs
 * */

//proto functions
function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}
function isLetter(str) {
  return str.length === 1 && str.match(/[a-z]/i);
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
/* JSEND style AJAX message checking and parsing
 *
 * */
function jsendp (message) {
    // xhr abort message object
    if ( typeof message === 'object' ) {
        if ( typeof message['responseText'] !== 'undefined' ) {
            message = message['responseText'];
        } else {
            message = JSON.stringify(message);
        }
    }
    // message is JSON text
    try {
        j = JSON.parse(message);
        if (j==null) {
            return {'status':'fail','data':'message parsing error: '+message};
        } else {
            return j;
        }
    }
    catch(e) { 
        return {'status':'fail','data':'message parsing error: '+message};
    }
}

// function for requesting list elements and final value for the dynamic select flow
function getNestedOptions(s,wh) {
    const form_type = $('input[name=selected_form_type]').val();

    const selected = $(s).find(':selected');
    if ($(s).data('nested')) {

        // form field with default value
        if (wh == 'default') {
            var id=$(s).attr('id');
            var condition = '';
            var lav = $(s).data('lav');

            var pos = id.substring(id.indexOf('-')+1,id.length);
            //const condition = ($(s).hasClass('genlist')) ?  $(s).val() : selected.data('fkey');
            if (lav === true && typeof(selected.data('fkey'))!='undefined') {
                 condition = selected.data('fkey')
            } else if (lav === true && typeof($(s).data('fkey'))!='undefined') {
                 condition = $(s).data('fkey')
            } else {
                 condition = $(s).val();
            }  

            $.post("ajax", {'dynamic_upload_list':pos,'sfid':$('#sfid').val(),'condition':condition}, function (data) {
                if (data!='') {
                    j = JSON.parse(data);

                    for (var i = 0; i<j.length ; i++) {

                        var col = j[i]['col'];
                        var opt = j[i]['opt'];
                        var type = j[i]['type'];
                        var target = '#default-'+col;

                        if ($(target).length != 0) {
                            if ($(target).is("input")) { 
                                if (type == 'autocomplete')
                                    $(target).data('list',opt);
                                else if (type == 'list')
                                    $(target).empty().append(opt);
                                else
                                    $(target).val(opt); 
                            } else {
                                $(target).empty().append(opt);
                            }
                        }
                        // if the child element is in the table: 
                        else {
                            var idx = $('#'+col).closest('.th').index();
                            var rows = $("#upload-data-table > .tbody > .tr").each(function() {
                                //if ($(target).length != 0)
                                if (type == 'text') {
                                    target = $(this).find('.td:eq(' + (idx) + ') input');
                                    $(target).val(opt);

                                } else if (type=='list') {
                                    target = $(this).find('.td:eq(' + (idx) + ') select');
                                    $(target).empty().append(opt);

                                } else if (type == 'autocomplete') {
                                    target = $(this).find('.td:eq(' + (idx) + ') input');
                                    target.data('list',opt);

                                }
                            });
                        }
                    }
                }
            });
        }
        // table fields
        else if (wh == 'table') {
            var col = $(s).closest('.td').index() ;
            var row = $(s).closest('.tr').index();
            var condition = '';
            var lav = $(s).data('lav');

            var pos = $("#upload-data-table > .thead > .tr:eq(1) > .th:eq("+(col)+") ul li").attr('id');
            //const condition = (lav == 'false' || $(s).hasClass('genlist')) ?  $(s).val() : selected.data('fkey');

            if (lav === true && typeof(selected.data('fkey'))!='undefined') {
                 condition = selected.data('fkey')
            } else if (lav === true && typeof($(s).data('fkey'))!='undefined') {
                 condition = $(s).data('fkey')
            } else {
                 condition = $(s).val();
            }  

            $.post("ajax", {'dynamic_upload_list':pos,'sfid':$('#sfid').val(),'condition': condition}, function (data) {
                if (data!='') {
                    j = JSON.parse(data);

                    for (var i = 0; i<j.length ; i++) {
                        var col = j[i]['col'];
                        var opt = j[i]['opt'];
                        var type = j[i]['type'];
                        var idx = $('#'+col).closest('.th').index();

                        if (type == 'text') {
                            var target = $("#upload-data-table > .tbody > .tr:eq(" + (row) + ") > .td:eq("+(idx)+") input");
                            $(target).val(opt);

                        } else if (type=='list') {
                            var target = $("#upload-data-table > .tbody > .tr:eq(" + (row) + ") > .td:eq("+(idx)+") select");
                            $(target).empty().append(opt);

                        } else if (type == 'autocomplete') {
                            var target = $("#upload-data-table > .tbody > .tr:eq(" + (row) + ") > .td:eq("+(idx)+") input");
                            target.data('list',opt);

                        }
                    }
                }
            });
        }
    }
}
function modal_ajax_stop(s) {
    if (s == 'start')
        $("#modal-overlay").show();
    else
        $("#modal-overlay").hide();
}

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}

const replaceInvalidFilenameCharacters = (str) => {
    const re = invalidFilenameCharacters;
    return str.replace(re, '_');
}

const invalidFilenameCharacters = /[^0-9A-Za-zÀ-ÖØ-öø-ÿ\-._]/g;
    
const openConfirmDialog = (content = "", buttons = [], title = "Confirm", height = "auto", width = "auto") => {
    $( "#dialog-confirm" ).dialog({ "buttons": buttons, "title": title, "height": height, "width": width});
    $( "#dialog-confirm" ).html(content);
    if (! $("#dialog-confirm").dialog("isOpen")) {
        $( "#dialog-confirm" ).dialog( "open" );
    }
}

/* General async content load for POST ajax
 * Az async nem is kell az elejére... anélkül is async
 * */
async function afoo(callback, data) {
    $.ajax({
        url: 'ajax',
        type: 'POST',
        data: data,
        success: function(response) {
            callback(response)
        }
    });
}
/* Content loader: JSON viewer */
function json_viewer(data,target) {
    if (data) {
        const j = JSON.parse(data);
        $("#"+target).empty().append(
            renderjson.set_icons("+","-")
                (j.data)
        );
    }
}
var dynamic_content = {};
/* Content loader: simple */
function content_loader(data,target) {
    const j = JSON.parse(data);
    $("#"+target).empty().append(j.content);
    dynamic_content[target] = j.data;
}

