<h2><?php echo t(str_project_statistics)?></h2>


<?php

global $BID, $MAINPAGE_VARS;

$mf = new mainpage_functions();

if (isset($MAINPAGE_VARS['sidebar1'])) {
    $boxes = preg_split('/\|/',$MAINPAGE_VARS['sidebar1']);
    $box_params = array();
    foreach ($boxes as $x) {
        $p = '';
        $t = $x;
        if (preg_match('/(\w+)\((.+)\)$/',$x,$m)) {
            $p = $m[2];
            $t = $m[1];
        }
        $params[$t] = $p;
    }
}
else {
    $boxes = array('members','uploads','data','species','species_stat');
    $params = array('members'=>'','uploads'=>'','data'=>'','species'=>1,'species_stat'=>'');
}

$boxes = array_keys($params);

if (in_array('members',$boxes)) {
    echo "
<div class='leftbox boxborder'><h3>". t(str_members) ."</h3>
    <ul class='boxul'>
    <li class='blarge'>"; 
    echo $mf->count_members($params['members']); 
    echo "</li>
    </ul>
</div>";
}

if (in_array('uploads',$boxes)) {
    $mf->count_uploads_cache = 5;
    echo "
<div class='leftbox boxborder'><h3>". t(str_uploads) ."</h3>
    <ul class='boxul'>
        <li class='blarge'>"; 
    echo $mf->count_uploads($params['uploads']);
    echo "</li>
    </ul>
</div>";

}

if (in_array('data',$boxes)) {
    $mf->table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE ;
    echo "
<div class='leftbox boxborder'><h3>". t(str_data) ."</h3>
    <ul class='boxul'>
        <li class='blarge'>";
    echo $mf->count_data($params['data']); 
    echo "</li>
    </ul>
</div>";
}

if (in_array('species',$boxes)) {
    $mf->count_species_cache = 5;
    echo "
<div class='leftbox boxborder'><h3>". t(str_species) ."</h3>
    <ul class='boxul'>
        <li class='blarge'><a href='?specieslist'>";
    echo $mf->count_species($params['species']); 
    echo "</a></li>
    </ul>
</div>";
}

if (in_array('species_stat',$boxes)) {
    echo "
<div class='leftbox boxborder'><h3>". t(str_species_stat) ."</h3>
    <ul class='boxul'>
        <li>";
    echo $mf->stat_species($params['species_stat']);
    echo "</li>
    </ul>
</div>";
}

if (in_array('hotlinks',$boxes)) {
    echo "
<div class='leftbox boxborder'><h3>". t(str_hotlinks) ."</h3>
    <ul class='boxul'>
        <li>";
    echo $mf->view_table_links($params['hotlinks']);
    echo "</li>
    </ul>
</div>";
}

// Custom column stats
foreach ($boxes as $b) {
    if (preg_match('/^column_(.+)\.([a-zA-Z0-9_]+):?(.+)?/',$b,$m)) {
        
        $cmd = sprintf("SELECT short_name FROM project_metaname WHERE project_table='%s' AND column_name='%s'",$m[1],$m[2]);
        $res = pg_query($BID,$cmd);
        $description = $m[2];
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $description = t($row['short_name']);
        }
        if (isset($m[3])) {
            $description = $m[3];
        }
        echo "
        <div class='leftbox boxborder'><h3>". t($description) ."</h3>
        <ul class='boxul'>
            <li class='blarge'>";
        echo $mf->stat_custom_column($m[1],$m[2]);
        echo "</li>
        </ul>
        </div>";

    }
    elseif (preg_match('/^columnsummary_(.+)\.([a-zA-Z0-9_]+):?(.+)?/',$b,$m)) {
        $cmd = sprintf("SELECT short_name FROM project_metaname WHERE project_table='%s' AND column_name='%s'",$m[1],$m[2]);
        $res = pg_query($BID,$cmd);
        $description = $m[2];
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $description = t($row['short_name']);
        }
        if (isset($m[3])) {
            $description = $m[3];
        }
        echo "
        <div class='leftbox boxborder'><h3>". t($description) ."</h3>
        <ul class='boxul'>
            <li><div class='tbl pure-u-1'>";
            $json_text = $mf->stat_custom_column_summary($m[1],$m[2]);
            $json = json_decode($json_text,true);
            foreach ($json as $k) {
                echo "<div class='tbl-row'><div class='tbl-cell'><span style='font-weight:bold'>{$k[$m[2]]}</span></div><div class='tbl-cell' style='text-align:right'>&nbsp;".$k['count'].'</div></div>';
            }
        echo "</div></li>
        </ul>
        </div>";
    }
}



// Custom function inclusions
foreach ($boxes as $b) {
    if (preg_match('/^custom_(.+)/',$b,$m)) {
        $custom_function = $m[1];
        if (file_exists("private/".$custom_function.".php"))
            include_once("private/".$custom_function.".php");
    }
}

?>
