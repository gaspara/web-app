<?php

/**
 * Class Healthcheck
 *
 * Checks the existence of different database objects 
 */
class Healthcheck
{
    /*****************
     * HEALTHCHECK FUNCTIONS
     *****************/


    /**
     * 
     * @param table string name of the table
     * @return boolean
     */
    public static function table_exists($table, $schema = 'public')
    {
        return self::check_existence('table', $table, $schema);
    }



    /**
     * Checks the existence of a function.
     *
     * @return boolean
     */
    public static function func_exists($fun_name, $schema = 'public')
    {
        return self::check_existence('function', $fun_name, $schema);
    }



    /**
     * Checks the existence of a trigger on a table 
     *
     * @return boolean
     */
    public static function trigger_exists($tg_name, $table, $schema = 'public')
    {
        return self::check_existence('trigger', [$tg_name, $table]);
    }



    /**
     * Checks if a trigger is enabled or not
     *
     * @return boolean
     */
    public static function trigger_enabled($tg_name, $table, $schema = 'public')
    {
        global $ID;

        $cmd = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname = %s AND tgrelid::regclass::varchar = %s;", quote($tg_name), quote($table));
        if (!$res = pg_query($ID, $cmd)) {
            throw new Exception('query error');
        }
        $result = pg_fetch_assoc($res);
        return ($result['tgenabled'] == 'O');
    }


    private static function check_existence($objecttype, $name, $schema = 'public')
    {
        global $ID;

        switch ($objecttype) {
            case 'table':
                $cmd = sprintf(
                    "SELECT EXISTS(SELECT * FROM information_schema.tables WHERE table_schema = %s AND table_name = %s);",
                    quote($schema),
                    quote($name)
                );
                break;
            case 'function':
                $cmd = sprintf(
                    "SELECT EXISTS(SELECT * FROM pg_proc p LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace WHERE proname = %s AND n.nspname = %s);",
                    quote($name),
                    quote($schema)
                );

                break;

            case 'trigger':
                $cmd = sprintf(
                    "SELECT EXISTS(SELECT 1 FROM pg_trigger WHERE tgname = %s AND tgrelid::regclass::varchar = %s);",
                    quote($name[0]),
                    quote($name[1])
                );

                break;
            default:

                break;
        }

        if (!$res = pg_query($ID, $cmd)) {
            throw new Exception('query error');
        }

        $result = pg_fetch_assoc($res);
        $r = ($result['exists'] == 't');
        return $r;
    }
}
