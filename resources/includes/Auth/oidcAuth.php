<?php
require_once(getenv('OB_LIB_DIR') . '/models/oidc_token.php');

class OidcAuth extends Auth
{

    public $provider = null;

    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    private static function setUpOidc($provider)
    {
        require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        if (!defined('OPENID_CONNECT')) {
            return false;
        }
        $OPENID_CONNECT = constant('OPENID_CONNECT');

        $provider = $provider ?? $_SESSION['openid_provider'] ?? null;

        if (!in_array($provider, array_keys($OPENID_CONNECT))) {
            return false;
        }

        $_SESSION['openid_provider'] = $provider;

        $client_id = $OPENID_CONNECT[$provider]['client_id'];
        $client_secret = $OPENID_CONNECT[$provider]['client_secret'] ?? null;
        $provider_url = $OPENID_CONNECT[$provider]['provider_url'];

        // This is the registered callback address on the provider's API. 
        // E.g. https://console.cloud.google.com/apis/credentials/oauthclient/
        // .... Authorized redirect URIs
        // The _SESSION['login_callback_address'] is the url which will be used to redirect from the openid.php to your application
        $redirect_uri =  $protocol . '://' . constant('URL') . '/includes/openid.php';

        $oidc = new Jumbojett\OpenIDConnectClient(
            $provider_url,
            $client_id,
            $client_secret
        );
        $certPath = defined('OPENID_CONNECT_CERT_PATH') ? constant('OPENID_CONNECT_CERT_PATH') : '/etc/pki/tls/certs/ca-bundle.crt';
        $oidc->setCertPath($certPath);

        $oidc->setRedirectURL($redirect_uri);
        return $oidc;
    }

    public static function openid_authentication($provider = null, $remember_me = null)
    {
        $provider = $provider ?? $_SESSION['openid_provider'] ?? null;
        $remember_me = $remember_me ?? $_SESSION['remember_me'] ?? false;
        
        $_SESSION['remember_me'] = $remember_me;

        $oidc = self::setUpOidc($provider);

        $oidc->addScope(['profile', 'email']);
        $oidc->addAuthParam(['access_type' => 'offline']);
        #$oidc->addAuthParam(['prompt' => 'consent']);

        $oidc->authenticate();

        // return from the provider
        $openid_userinfo = $oidc->requestUserInfo();

        $U = new User($openid_userinfo->email);

        $password = self::genpwd();
        if (is_null($U->user_id)) {
            $userinfo = [
                'name' => $openid_userinfo->family_name . ' ' . $openid_userinfo->given_name,
                'email' => $openid_userinfo->email,
                'provider' => $provider,
                'password' => $password
            ];
            if (!$U->register((object) $userinfo)) {
                // User create error
            }
        } else {
            if (!$U->update(["password" => $password])) {
                // User update error
            }
        }
        $url = (defined('OAUTHURL')) ? constant('OAUTHURL') . '/oauth/token.php' : protocol() . '://' . constant('URL') . '/oauth/token.php';

        // át kellene venni a bejelentkezés jelenlegi scope-jait!
        $scopeRequired = "webprofile get_profile put_data get_data";

        $data = [
            'username' => $openid_userinfo->email,
            'password' => $password,
            'client_id' => 'web',
            'client_secret' => constant('WEB_CLIENT_SECRET'),
            'scope' => $scopeRequired,
            'grant_type' => 'password',
            'remember_me' => $remember_me
        ];
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        $token = json_decode($response);
        $token->remember_me = $remember_me;

        if (isset($token->access_token)) {
            //$request->request['access_token'] = $token->access_token;

            $U->login();
            self::set_token_cookies($token);
            return true;
        } else {
            #$token['error_description'] .= ": " . $scopeRequired;
            log_action($token, __FILE__, __LINE__);
            // return error somehow...
            // {"error":"invalid_scope","error_description":"The scope requested is invalid for this client"}
        }
    }

    public static function generate_openid_button($provider, $simple = false)
    {

        if (!defined('OPENID_CONNECT')) {
            return "";
        }
        $OPENID_CONNECT = constant('OPENID_CONNECT');

        if (!isset($OPENID_CONNECT[$provider]['client_id'])) {
            log_action("OpenId connect configuration error: client ID is missing for service $provider");
            return "";
        }

        $btnClass = "{$provider}SignInBtn";
        $btn = new $btnClass();

        return $btn->html($simple);
    }

    private static function genpwd()
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        return substr(str_shuffle($chars), 0, 16);
    }
}
