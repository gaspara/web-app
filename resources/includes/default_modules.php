<?php

abstract class module {
    public $params;
    public $error = false;
    public $external_dependencies = [];
    public $strings = [];


    public $ajax_deps = [];
    public $ajax_echo_common_message = true;
    #public $main_table;
 
    abstract protected function moduleName();

    public function print_js($request) {

        $module = $this->moduleName();
        return (file_exists(getenv('OB_LIB_DIR')."modules/$module.js")) ? file_get_contents(getenv('OB_LIB_DIR') . "modules/$module.js") : '';

    }

    public function print_css($request) {

        $module = $this->moduleName();
        $mdule_header = "/* $module styles */\n";
        return (file_exists(getenv('OB_LIB_DIR')."modules/$module.css")) ? $mdule_header . file_get_contents(getenv('OB_LIB_DIR') . "modules/$module.css") : '';

    }

    public function module_params_interface($params, $pa) {
        return false;
    }
    
    public function init($params, $pa) {
        return;
    }
    
    public function destroy($params, $pa) {
        return true;
    }
    
    public function displayError() {
        log_action($this->error,__FILE__,__LINE__, $this->moduleName());
    }

    public function translations() {
        $translations = [];
        foreach ($this->strings as $string) {
            $translations[$string] = t("$string");
        }
        return $translations;
    }

    public function ajax($params, $request)
    {
        try {
            foreach ($this->ajax_deps as $filename) {
                $lib = getenv('OB_LIB_DIR') . "$filename.php";
                if (file_exists($lib)) {
                    require_once($lib);
                }
            }

            if (!isset($request['action'])) {
                throw new Exception("action missing", 1);
            }
            $retval = "";
            $action = $request['action'];
            
            if (!method_exists($this, $action)) {
                throw new Exception("invalid module action: {$request['m']}::{$request['action']}", 1);
            }

            $result = $this->$action($params, $request);
            
            if ($this->error) {
                throw new Exception($this->error, 1);
            }

            if ($this->ajax_echo_common_message) {
                echo common_message('ok', $result);
            } else {
                echo json_encode($result);
            }
            // ez nem valami szép megoldás .... 
            if ($result === 'clean_ob') {
                ob_clean();
            }
            return;
        } catch (\Throwable $th) {
            echo common_message('error', $th->getMessage());
            return;
        }
    }
    
    public function external_dependencies($params, $request) {
        return $this->external_dependencies;
    }
    
    public function get_params() {
        //return $this->params;
        return $this->split_params($this->params);
    }

    public function split_params($params,$type='simple') {

        $p = array('module params not processed');
        if ($type=='simple') {
            if (is_array($params))      // new_params
                $p = $params;
            else {
                // split simple list
                if (preg_match("/^JSON:(.*)$/",$params,$json)) {
                    $p = json_decode(base64_decode($json[1]),true);
                } else {
                    $p = explode(';',$params);
                }
            }
        } elseif ($type == 'pmi') {

            if (is_array($params))      // new_params
                $p = $params;
            else {
                if (preg_match("/^JSON:(.*)$/",$params,$json)) {
                    $p = json_decode(base64_decode($json[1]),true);
                } else {
                    # Array("modszer:shunt","hely:geom");
                    $p = array();
                    $pl = preg_split('/;/',$params);
                    foreach ($pl as $pm) {
                        $c = preg_split('/[:=]/',$pm);
                        if ($c[0] == '') continue;

                        $p[$c[0]] = $c[1];
                    }
                }
            }
        }
        
        //debug($this->moduleName(),__FILE__,__LINE__);
        //debug($p,__FILE__,__LINE__);

        return $p;
    }
}

#require_once('table_modules.php');
?>
