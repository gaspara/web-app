<?php
#' ---
#' Module:
#'   read_table
#' Files:
#'   [read_table.php]
#' Description: >
#'   Read a table or view into a dynamic html table and give an encrypted link to it
#' Methods:
#'   [check_table, list_links, adminPage, getMenuItem]
#' Examples: >
#'   [
#'   "public.dinpi:faj DESC,szamossag"
#'   ]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   0.1
class read_table extends module {

    private $hash;
    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);

        if (defined('MyHASH'))
            $this->hash = MyHASH;
        else
            $this->hash = PROJECTTABLE.gisdb_user.gisdb_pass;
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function check_table($params,$data) {
        
        foreach ($params as $a) {
             if ($data == $a['table']) {
                 return true;
             }   
        }
        return false;
    }

    public function list_links($p) {
        
        $params = $this->split_params($p);
        $links = array();

        foreach ($params as $t) {
            if (!isset($t['orderby'])) $t['orderby'] = 'asc';
            $links[] = array("link"=>encode($t['table'],$this->hash),"label"=>$t['label'],"order"=>$t['orderby']);
        }

        return $links;
    }

    public function decode_link($params,$data) {
        
        list($schema,$table) = preg_split('/\./',decode($data,$this->hash));
        return array("schema"=>$schema,"table"=>$table);

    }

    public function adminPage() {

        if (!has_access('master')) return;

        global $ID,$BID;


        $modules = new modules();
        $l = $modules->_include('read_table','list_links');

        $em = "<h2>".t(str_read_table)."</h2><div class='shortdesc'>sharable links</div>";

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $links = array();

        //$l = $this->list_links;
        foreach ($l as $link) {
            $links[] = sprintf('<li><a href="%1$s://%2$s/view-table/%3$s/" target="_blank">[%4$s]: %1$s://%2$s/view-table/%3$s/</a></li>', 
                                $protocol, URL, $link['link'], $link['label'] );
        }
        $em .= "<ul>".implode('',$links)."</ul>";

        return $em;
    }

    public function getMenuItem() {
        return ['label' => 'shareable table links', 'url' => 'read_table' ];
    }

}

?>
