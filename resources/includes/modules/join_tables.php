<?php
#' ---
#' Module:
#'   join_tables
#' Files:
#'   [join_tables.php]
#' Description: >
#'   Deprecated, not used!
#'   RETURN: join command and column list and visible names list
#'   [0] column name , separated list
#'   [1] prefixed column names array: all column which defined in the database columns
#'   [2] visible names array of array by JOIN
#' Methods:
#'   [load_joined_data]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class join_tables extends module {
    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action) {
            $this->retval = $this->$action($static_params,$dynamic_params);
        }
    }
    
    protected function moduleName() {
        return __CLASS__;
    }
    

    public function load_joined_data($static_params, $pa) {
        global $ID;
        require_once(getenv('OB_LIB_DIR').'results_builder.php');

        $data = [];
        $mtable = $pa['mtable'] ?? PROJECTTABLE;
        $whereclause = (isset($pa['filter'])) ? implode(' AND ', array_map(function ($c) {
            return prepareFilter($c[0], $c[1], $c[2] ?? NULL, "m");
        }, $pa['filter'])) : "1=1";
        $cqt_backup = $_SESSION['current_query_table'];
        foreach ($static_params as $join) {
            $_SESSION['current_query_table'] = $join['table'];

            $join_on = implode(' AND ', array_map(function ($c) {
                return "m.{$c['ref_field']} " . ($c['relation'] ?? '=') . " t.{$c['join_field']}";
            }, $join['join_on']));
            $cmd = sprintf("SELECT t.obm_id FROM %s m LEFT JOIN %s t ON %s WHERE %s", $mtable, $join['table'], $join_on, $whereclause);
            if (!$res = pg_query($ID, $cmd)) {
                log_action('Query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
                return [];
            }
            if (!pg_num_rows($res)) {
                $data[$join['table']] = [];
            }
            else {
                $ids = array_column(pg_fetch_all($res), 'obm_id');
                $rb = new results_builder();
                if ($rb->results_query($ids,false)) {
                    $result = pg_query($ID,$rb->rq['query']);
                    if (!pg_num_rows($result)) {
                        $data[$join['table']] = [];
                    }
                    else {
                        $data[$join['table']] = pg_fetch_all($result);
                    }
                } else {
                    $_SESSION['current_query_table'] = $cqt_backup;
                    log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
                    return;
                }

            }
        }
        $_SESSION['current_query_table'] = $cqt_backup;
        return $data;
    }
}
?>
