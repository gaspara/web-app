<?php

// replace "your_module_name" and rename this file to it
// add module call function to default_modules.php
// add your module's functions call to ...php files

class your_module_name extends module {
    
    public $error = '';
    
    // array of translatable strings related to the module
    public $strings = [];
    
    // ajax dependencied: an array of filenames from the includes eg.: 'taxon' will include includes/taxon.php 
    public $ajax_deps = ['taxon'];
    
    // if defined, and external_dependencies method name is defined in default_modules.php, then they will be loaded in deck.php and view.php, before the modules.js.php
    public $external_dependencies = [
        '<link href="https://example.com/stylesheet.css" rel="stylesheet">',
        '<script type="text/javascript" src="https://example.com/javascript.js"></script>',
    ];
    
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }
    
    // optional
    // returns true|['error' => 'error message']
    public function init($static_params,$dynamic_params) {

        //example> checking terms table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'table_name');");
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        if ($result['exists'] == 'f') {
            $cmd = 'execute table creaton and other sql queries';
            if (!pg_query($cmd)) {
                return ['error' => 'error message'];
            }
            return true;
        }
        
    }
    
    // optional
    // If exists, this content will be loaded in modules page clicking on the green "cog" icon button
    public function adminPage($static_params,$dynamic_params) {

        if (!has_access('master')) return;

        return "<div>Admin page content</div>";
    }

    // optional
    // If exists, this will be acitvate in modules page the green "cog" icon button in the module line
    // the "url" module will be called and its adminPage will be shown
    public function getMenuItem() {
        return ['label' => str_all_polygons, 'url' => 'official_module_template' ];
    }

    // optional
    // if exists, will be automatically included in modules.js.php 
    public function print_js($request) {
       echo "
        $(document).ready(function() {
            alert('example module');

            $.post('ajax',{'m':'official_module_template','doSomething':1},function(data){
            
            });
        });";
    }

    // optional
    // if exists, this function will be executed on .profilelink click, and content will be loaded into profile tab
    public function profilePage() {
        echo "content";
    }

    // optional
    // if exists, will be automatically included in profile page
    public function profileItem() {
        return [
            'label' => 'A module item in profile page',
            'fa' => 'fa-cubes',
            'item' => '<tr><td>...</td></tr>'
        ];
    }
    
    // optional
    // If exists, this table box will be shown on map page
    public function print_box ($static_params,$dynamic_params) {
        return sprintf("<table class='mapfb'>%s</table>",'template-module');
    }

    // optional
    // If exist, will be included afuncs.php
    // m=module_name should be part of the request
    public function ajax($static_params,$dynamic_params) {

        $this->params = $this->split_params($static_params);

        print $this->ajax_fun($par);
        return;
    
    }

    // optional
    // called by ajax()
    private function ajax_fun($par) {
        return 1;
    }
    
}
?>
