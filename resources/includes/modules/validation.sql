-- create_validation_rules_table
CREATE TABLE "public"."%1$s_validation_rules" ( 
    id SERIAL PRIMARY KEY, 
    taxon_id integer NOT NULL,
    target character varying NOT NULL, 
    col character varying NOT NULL,
    relation character varying NOT NULL,
    val text NOT NULL,
    match_value character varying NOT NULL,
    else_value character varying NOT NULL
) WITH (oids = false);
