<?php
/*  User, Group settings on projects
 *  
 *  
 * Ajax function 
 *  
 *  
 * */
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
session_start();

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

//* only for logined users
if(!isset($_SESSION['Tid'])) {
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}

/* update members / users 
 * POST user_id
 * POST groups
 * POST user_status
 * */
if (isset($_POST['user_id']) and isset($_POST['groups'])) {
    if(!has_access('members')) exit;

    $groups = explode(',',$_POST['groups']);
    $user_id = $_POST['user_id'];
    $user_status = $_POST['user_status'];
    
    if ($user_id == '') exit;

    $cmd = sprintf("UPDATE project_users SET user_status=%s WHERE user_id=%s AND project_table='%s'",quote($user_status),quote($user_id),PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    if (!pg_affected_rows($res)) {
        echo common_message('error','error, see the logs');
        log_action("Update project_user's user_status failed for $user_id",__FILE__,__LINE__);
    }

    /*
    $cmd = sprintf("SELECT role_id FROM project_roles WHERE user_id=%s AND project_table='%s'",quote($user_id),PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $role_id = $row['role_id'];

        // remove user's role from the roles which includes it.
        $cmd = sprintf("UPDATE project_roles SET container = array_remove(container, %d) WHERE project_table='%s' AND (user_id IS NULL OR user_id!=%s)",$role_id,PROJECTTABLE,quote($user_id));
        $res = pg_query($BID,$cmd);

        foreach ($groups as $role) {
            if ($role == '') continue;
            # SELECT ARRAY(SELECT DISTINCT UNNEST(container || %d) FROM project_roles WHERE role_id=201 ORDER BY 1);
            $cmd = sprintf('UPDATE project_roles SET container = ARRAY(SELECT DISTINCT UNNEST(container || %1$d) FROM project_roles WHERE role_id=%2$s AND project_table=\'%3$s\' ORDER BY 1)  WHERE role_id=%2$s AND project_table=\'%3$s\'',$role_id,$role,PROJECTTABLE);
            $res = pg_query($BID,$cmd);
            if (!pg_affected_rows($res)) {
                log_action("Update project_roles' container failed for $user_id",__FILE__,__LINE__);
            } 
        }
        if ($user_id === $_SESSION['Tid']) {
            $r = read_groups($user_id);
            $_SESSION['Tgroups'] = $r['roles'];
            $_SESSION['Trole_id'] = $r['cont'];
        }
    }

    $cmd = sprintf("SELECT array_to_string(array_agg(role_id),',') AS groups FROM project_roles WHERE %s=ANY(container) AND project_table='%s'",quote($role_id),PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_assoc($res);
    print $row['groups'];*/
    echo common_message('ok','ok');
    exit;
} 
/* Create new role (group) 
 *
 * */
if (isset($_POST['new_group'])) {
    if(!has_access('groups')) exit;

    mb_internal_encoding("UTF-8");
    mb_regex_encoding("UTF-8");    
    $new_group = mb_ereg_replace('[^a-zöüóéáűíA-ZÖÜÓŐÚÉŰÍ0-9_]','',$_POST['new_group']);
    if ($new_group=='') exit;

    $cmd = sprintf("INSERT INTO project_roles (project_table,description) VALUES ('%s',%s)",PROJECTTABLE,quote($new_group));
    if(!pg_query($BID,$cmd)) {
        print 'failed';
        log_action('Create role (group) failed',__FILE__,__LINE__);
    } else
        print 'ok';
    exit;
}
/* drop groups*/
if (isset($_POST['drop_group_id'])) {
    if(!has_access('groups')) exit;

    $group = $_POST['drop_group_id'];
    // drop only empty roles !!!
    $cmd = sprintf('DELETE FROM project_roles WHERE role_id=%s AND (container IS NULL OR array_length(container,1) IS NULL OR (array_length(container,1)=1 AND container[1]=role_id))',quote($group)); 
    $res = pg_query($BID,$cmd);
    if (!$res) {
        print 'failed';
        log_action('Drop role (group) failed',__FILE__,__LINE__);
    } else {
        if (pg_affected_rows($res))
            print 'ok';
        else
            print 'non empty group';
    }
    exit;

}
/* Update group's rights
 *
 * */
if (isset($_POST['group_id']) and isset($_POST['nested'])) {
    if(!has_access('groups')) exit;

    $group = $_POST['group_id'];
    if ($group == '') exit;
    
    $cmd = sprintf('UPDATE project_roles SET container=\'{%s}\' WHERE role_id=%s',$_POST['nested'],quote($group)); 

    if(!pg_query($BID,$cmd))
        print 'failed';
    else {
        read_groups($_SESSION['Tid']);
        print 'ok';
    }

    exit;
}
// assign column type and name to columns
if (isset($_POST['t_post'])) {
    if(!has_access('db_cols')) exit;
    unset($_SESSION['update_table_error']);

    $j = json_decode($_POST['t_post']);
    #$j->{'colname'};
    #$j->{'name'};
    $n = -1;
    $data = array();
    list($schema,$table) = preg_split("/\./",$j->{'table'});
    $schema = preg_replace("/[^a-z0-9_]/i","",$j->{'scheme'}); // overwrite & name check
    $table = preg_replace("/[^a-z0-9_]/i","",$table); // nam check
    $alters = array();
    $f_x_column = '';
    $f_y_column = '';
    #$f_id_column = '';
    $f_date_column = array();
    $f_cite_person = array();
    $f_quantity_column = '';
    $f_species_column = '';
    $f_geom_column = '';
    $f_geom_srid = 4326;
    $f_use_rules = '';
    $f_attachment_columns = array();
    $f_utmzone_column = '';
    $set_rules = 0;
    $set_srid = 0;
    $drop_column = [];
    $rename_column = [];
    $old_name = [];
    $comments = array();
    $commands = array();
    $obm_columns = array('obm_id','obm_uploading_id','obm_validation','obm_modifier_id','obm_comments');
    $drop_table = false;
    
    foreach($j->{'type'} as $t) {
        $n++;

        $comments[$j->{'colname'}[$n]] = $j->{'comment'}[$n];
        $commands[$j->{'colname'}[$n]] = $j->{'command'}[$n];

        // Drop column
        if ($t!='obm-column' and preg_match('/^DROP$/',$j->{'command'}[$n],$m)) {
            $drop_column[] = $j->{'colname'}[$n];
            continue;
        }
        // Drop table
        if ($j->{'colname'}[$n]=='obm_id' and preg_match('/^DROP-TABLE$/',$j->{'command'}[$n],$m)) {
            $drop_table = true;
        }
        // Rename column
        if ($t!='obm-column' and preg_match('/^RENAME:(.+)/',$j->{'command'}[$n],$m)) {
            $x = strtolower(preg_replace("/[^a-z0-9_]/i","",$m[1]));
            $rename_column[] = $x;
            $old_name[] = $j->{'colname'}[$n];

            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $x;
            $data[$x] = $j->{'name'}[$n];
            $comments[$x] = $comments[$j->{'colname'}[$n]];

            continue;
        }

        // If no column type, column is not processed
        if ($t=='') {
            continue;
        }


        if ($t=='data') {
            #$data[]='"'.$j->{'colname'}[$n].'"=>"'.$j->{'name'}[$n].'"';
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif (substr($t, 0, 10) == 'alternames') {
            $t_split = preg_split('/@/', $t);
            $lang = $t_split[1] ?? null;
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $alters[]= $j->{'colname'}[$n] . (($lang) ? "@$lang" : "");
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='species') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_species_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='numind') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_quantity_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='datum') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_date_column[] = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        #} elseif ($t=='id') {
        #    $f_id_column = $j->{'colname'}[$n];
        } elseif ($t=='Xc') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_x_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='Yc') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_y_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='cp') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_cite_person[] = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='geometry') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_geom_column = $j->{'colname'}[$n];
            if (preg_match('/^SET srid:(\d+)/',$j->{'command'}[$n],$m)) {
                $set_srid = 1;
                $f_geom_srid = $m[1];
            }
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='attachment') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_attachment_columns[] = $table;
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='utmzone') {
            if ($j->{'name'}[$n]=='') 
                $j->{'name'}[$n] = $j->{'colname'}[$n];
            $f_utmzone_column = $j->{'colname'}[$n];
            $data[$j->{'colname'}[$n]] = $j->{'name'}[$n];
        } elseif ($t=='obm-column') {
            if (preg_match('/^SET use_rules:(.+)/',$j->{'command'}[$n],$m)) {
                $set_rules = 1;
                if ($m[1]=='0' or $m[1]=='false' or $m[1]=='-1' or $m[1]=='f' or $m[1]=='no' or $m[1]=='off') 
                    $f_use_rules = 0;
                else
                    $f_use_rules = 1;
            }
        }
    }

    //order
    $n=-1;
    $order = array();
    foreach($j->{'order'} as $o) {
        $n++;
        if ($o=='') continue;
        if (!isset($order[$table])) {
            $order[$table] = array();
        }
        //$order[$o] = $j->{'colname'}[$n];
        // Skip delete column
        if (in_array($j->{'colname'}[$n],$drop_column))
            continue;

        $order[$table][$j->{'colname'}[$n]] = $o;
    }
    $os = array();
    foreach($order as $k=>$v) {
        asort($v);
        $os[$k] = $v;
    }
    $order = json_encode($order);
    $order = json_encode($os);

    // Clean metaname table
    $error = '';
    $cmd = sprintf('BEGIN;DELETE FROM project_metaname WHERE project_table=\'%s\' AND project_schema=\'%s\'',$table,$schema);
    pg_query($BID,$cmd);

    if ($drop_table) {
        $cmd = sprintf('DELETE FROM header_names WHERE f_project_schema=\'%s\' AND f_project_name=\'%s\' AND f_project_table=\'%s\'',$schema,PROJECTTABLE,$table);
        pg_query($BID,$cmd);

        if (pg_last_error($BID)) {
            $error = 1;
        } else {
            $cmd = sprintf('BEGIN;SELECT exists(SELECT * FROM %s.%s)',$schema,  $table);
            $res = pg_query($GID,$cmd);
            $row = pg_fetch_assoc($res);
            if ($row['exists']=='f') {
                $cmd = sprintf('DROP TABLE %s.%s',$schema,$table);
                $res = pg_query($GID,$cmd);
                if (pg_last_error($GID)) {
                    $error = pg_last_error($GID);
                }
            }
        }
        if ($error == '') {
            pg_query($BID,"COMMIT");
            pg_query($GID,"COMMIT");
            print $schema.'.'.PROJECTTABLE;
            exit;
        } else {
            pg_query($BID,"ROLLBACK");
            pg_query($GID,"ROLLBACK");
            print $schema.'.'.$table;
            exit;
        }
    }

    // check header_names entry exists
    $cmd = sprintf("SELECT f_project_name FROM header_names WHERE f_project_name='%s' AND f_project_table=%s",PROJECTTABLE,quote($table));
    $res = pg_query($BID,$cmd);
    if (!pg_num_rows($res)) {
        $cmd = sprintf("INSERT INTO header_names (f_project_schema, f_project_name,f_project_table,f_id_column) VALUES (%s,'%s',%s,'obm_id')",
            quote($schema),PROJECTTABLE,quote($table));
        $res = pg_query($BID,$cmd);
    }

    
    // update columns' order
    $cmd = sprintf('UPDATE header_names SET f_order_columns=%s WHERE f_project_name=\'%s\' AND f_project_table=%s',
        quote($order),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    
    $n = 0;
    foreach ($data as $k=>$v) {
        if ($v=='') {
            echo "$k:empty value";
            continue;
        }

        if (in_array($j->{'colname'}[$n],$drop_column))
            continue;

        $cmd = sprintf('INSERT INTO project_metaname (project_table,column_name,short_name,rights,description,project,project_schema) 
            VALUES (%s,%s,%s,\'%s\',%s,\'%s\',%s)',
            quote($table),quote($k),quote($v),'{0}',quote($comments[$k]),PROJECTTABLE,quote($schema));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);

        //$cmd = sprintf('COMMENT ON COLUMN %s.%s IS \'%s\'',$table,$k,$comments[$k]);
        //pg_query($ID,$cmd);
        //$error .= pg_last_error($ID);
        $n++;
    }

    // Delete column
    if (count($drop_column)) {
        for ($i=0;$i<count($drop_column);$i++) {
            $cmd = sprintf('SELECT "%1$s", count(*) AS "count"
                FROM "%3$s"."%2$s" GROUP BY "%1$s" ORDER BY "%1$s"',$drop_column[$i],$table,$schema);
            $res = pg_query($GID,$cmd);
            if (pg_num_rows($res) == 1) {
                // non-empty table
                $row = pg_fetch_assoc($res);
                if ($row[$drop_column[$i]] == NULL) {
                    // empty column
                    $cmd = sprintf('ALTER TABLE "%1$s"."%2$s" DROP COLUMN IF EXISTS "%3$s"',$schema,$table,$drop_column[$i]);
                    pg_query($GID,$cmd);
                    $error .= pg_last_error($GID);
                } else
                    $error .= "Only empty columns can be dropped!";
            } elseif (!pg_num_rows($res)) {
                // empty table
                $cmd = sprintf('ALTER TABLE "%1$s"."%2$s" DROP COLUMN IF EXISTS "%3$s"',$schema,$table,$drop_column[$i]);
                pg_query($GID,$cmd);
                $error .= pg_last_error($GID);
            } else {
                $error .= "Only empty columns can be dropped!";
            }
        }
    }

    // Rename column
    if (count($rename_column)) {
        for ($i=0;$i<count($rename_column);$i++) {
            $cmd = sprintf('ALTER TABLE "%4$s"."%1$s" RENAME COLUMN "%2$s" TO "%3$s"',$table,$old_name[$i],$rename_column[$i],$schema);
            pg_query($GID,$cmd);
            $error .= pg_last_error($GID);
        }
    }

    #
    $alters = implode(",",$alters);
    $cmd = sprintf('UPDATE header_names SET f_alter_speciesname_columns=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote("{".$alters."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $cmd = sprintf('UPDATE header_names SET f_species_column=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote($f_species_column),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $f_date_column = implode(",",$f_date_column);
    $cmd = sprintf('UPDATE header_names SET f_date_columns=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote("{".$f_date_column."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    $cmd = sprintf('UPDATE header_names SET f_quantity_column=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote($f_quantity_column),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);
    #
    #$cmd = sprintf('UPDATE header_names SET f_id_column=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote($f_id_column),PROJECTTABLE,quote($table));
    #pg_query($BID,$cmd);
    #$error .= pg_last_error($BID);
    #
    if (isset($f_y_column)) {
        $r = $f_y_column;
        $cmd = sprintf('UPDATE header_names SET f_y_column=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote($r),PROJECTTABLE,quote($table));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);
    }
    #
    if (isset($f_x_column)) {
        $r = $f_x_column;
        $cmd = sprintf('UPDATE header_names SET f_x_column=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote($r),PROJECTTABLE,quote($table));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);
    }
    # geometry
    $r = $f_geom_column;
    $cmd = sprintf('UPDATE header_names SET f_geom_column=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote($r),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);

    # geometry srid
    $r = $f_geom_srid;
    if ($set_srid) {
        $cmd = sprintf('UPDATE header_names SET f_srid=%1$d WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',$r,PROJECTTABLE,quote($table));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);
    }

    # utm zone
    $r = $f_utmzone_column;
    $cmd = sprintf('UPDATE header_names SET f_utmzone_column=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote($r),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);

    # cite person
    $f_cite_person = implode(",",$f_cite_person);
    $cmd = sprintf('UPDATE header_names SET f_cite_person_columns=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote("{".$f_cite_person."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);

    # attachments
    $f_attachment_columns = implode(",",$f_attachment_columns);
    // file connection trigger
    if ($f_attachment_columns != '') {
        
    }
    $cmd = sprintf('UPDATE header_names SET f_file_id_columns=%1$s WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',quote("{".$f_attachment_columns."}"),PROJECTTABLE,quote($table));
    pg_query($BID,$cmd);
    $error .= pg_last_error($BID);

    # use rules
    if (has_access('master') and $set_rules) {
        $r = $f_use_rules;
        $cmd = sprintf('UPDATE header_names SET f_use_rules=%1$d WHERE f_project_name=\'%2$s\' AND f_project_table=%3$s',$r,PROJECTTABLE,quote($table));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);

        // Ha nem létezik, létrejön a function 
        $cmd = Create_Project_SQL_functions($schema,$table,'rules',gisdb_user);
        if (!pg_query($ID,$cmd)) { 
            $error = pg_last_error($ID);
        }
        
        // Létrehozza, vagy letiltja a triggert, létrehozza a _rules táblát is
        $cmd = Create_Project_SQL_functions($schema,$table,'rules_trigger',gisdb_user);
        if (!pg_query($ID,$cmd)) { 
            $error = pg_last_error($ID);
        }
        
        // Engedélyezzük a triggert, ha use_rules = 1
        if ($f_use_rules === 1) {
            //pg_query(): Query failed: ERROR:  trigger 'checkitout_rules' for table 'checkitout_visegrad' does not exist
            $cmd = sprintf('ALTER TABLE "%3$s"."%1$s" ENABLE TRIGGER "%2$s"',$table,'rules_'.$table,$schema);
            if (!pg_query($ID,$cmd)) {
                $error = pg_last_error($ID);
            }
        } else {
            $cmd = sprintf('ALTER TABLE "%3$s"."%1$s" DISABLE TRIGGER "%2$s"',$table,"rules_".$table,$schema);
            if (!pg_query($ID,$cmd)) {
                $error = pg_last_error($ID);
            }

        }
    }

    #commit or not
    if ($error=='') {
        $result = pg_query($BID,"COMMIT");

        // Reread table columns into session 
        st_col($table,'session',false);
        print $schema.'.'.$table;
    } else {
        log_action($error,__FILE__,__LINE__);
        $_SESSION['update_table_error'] = $error;
        $result = pg_query($BID,"ROLLBACK");
        print $schema.'.'.$table;
    }
    exit;
}
// adding new column 
if (isset($_POST['new_column'])) {
    if (!has_access('db_cols')) exit;
    
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");

    $error = '';

    list($schema,$table) = preg_split("/\./",$_POST['table']);

    $cmd = sprintf("SELECT f_project_name FROM header_names WHERE f_project_name='%s' AND f_project_table=%s AND f_project_schema=%s",
        PROJECTTABLE,quote($table),quote($schema));
    $res = pg_query($BID,$cmd);
    if (!pg_num_rows($res)) {
        exit;
    }

    $name = strtolower($_POST['nc_name']);
    $type = $_POST['nc_type'];
    $length = $_POST['nc_length'];
    $array = $_POST['nc_array'];
    $default = $_POST['nc_default'];
    $check = $_POST['nc_check'];

    $label = $_POST['nc_label'];
    $comment = $_POST['nc_comment'];

    $def = sprintf('"%s" %s',$name,$type);

    if ($length!='' and $type=='character varying')
        $def .= sprintf("(%d)",$length);

    //ALTER TABLE dinpi ADD column asd character varying(4)[] default '{asda}'

    if ($array!='')
        $def .= '[]';

    if ($default!='') {
        $def .= ' DEFAULT ';
        if ($array!='')
            $def .= sprintf('{%s}',preg_replace("/[\"']/",'',$default));
        else
            $def .= $default;
    }

    pg_query($ID,'BEGIN');
    pg_query($BID,'BEGIN');

    # adding column
    $cmd = sprintf("ALTER TABLE \"%s\".\"%s\" ADD COLUMN %s",preg_replace("[^a-z0-9_]","",$schema),preg_replace("[^a-z0-9_]","",$table),$def);
    pg_query($ID,$cmd);
    $error .= pg_last_error($ID);

    # create metaname entry
    if ($label != '') {
        $cmd = sprintf('INSERT INTO project_metaname (project_schema,project_table,column_name,short_name,rights,description,project) 
                        VALUES (%s,%s,%s,%s,\'%s\',%s,%s)',
                        quote($schema),quote($table),quote($name),quote($label),'{0}',quote($comment),quote(PROJECTTABLE));
        pg_query($BID,$cmd);
        $error .= pg_last_error($BID);
    }

    # adding comment
    if ($comment!='') {
        //COMMENT ON COLUMN dinpi.eov_x IS 'EOV-ban az nem az mint WGS-ben';
        $cmd = sprintf('COMMENT ON COLUMN "%s"."%s"."%s" IS \'%s\'',$schema,$table,$name,preg_replace("/[\"']/",'',$comment));
        pg_query($ID,$cmd);
        $error .= pg_last_error($ID);
    }

    # adding check constraint
    if ($check!='' and $check!='()') {
        //ALTER TABLE prices_list ADD CONSTRAINT price_discount_check CHECK (...)
        //ALTER TABLE link ADD CHECK (target IN ('_self', '_blank', '_parent', '_top'));
        $cmd = sprintf('ALTER TABLE "%3$s"."%1$s" ADD CHECK %2$s',$table,$check,$schema);
        pg_query($ID,$cmd);
        $error .= pg_last_error($ID);
    }

    #commit or not
    if ($error=='') {
        $result = pg_query($ID,"COMMIT");
        $result = pg_query($BID,"COMMIT");
        echo common_message('ok', $_POST['table'] );

        // Reread table columns into session 
        st_col($table,'session',false);
    } else {
        log_action($error,__FILE__,__LINE__);
        $result = pg_query($ID,"ROLLBACK");
        $result = pg_query($BID,"ROLLBACK");
        echo common_message('error', $error );
    }
    exit;
}

/* create / update upload form / forms kitöltő ív, feltöltő űrlap
 *
 *
 * */
if (isset($_POST['upload_form'])) {
    if(!has_access('upload_forms')) exit;

    $j = json_decode($_POST['upload_form']);

    $form_action = $j->{'method'}; // draft, publish, update
    $draft_version = '';
    $published_form_id = NULL;
    $edit_form_id = $j->{'edit_form_id'};
    $form_base_name = $j->{'form_name'};
    list($form_schema, $form_table) = preg_split("/\./",$j->{'form_table'});

    $schema = preg_replace("/[^a-z0-9_]/i","",$form_schema); // name check
    $table = preg_replace("/[^a-z0-9_]/i","",$form_table); // name check

    if (!isset($j->{'form_data_access_read'})) $j->{'form_data_access_read'} = array();
    if (!isset($j->{'form_data_access_write'})) $j->{'form_data_access_write'} = array();

    // Existing form 
    if ($edit_form_id != '') {

        // check form is exists
        $cmd = sprintf("SELECT form_name,published_form_id FROM project_forms WHERE form_id=%d",$j->{'edit_form_id'});
        $r = pg_query($BID,$cmd);
        if (pg_num_rows($r)) {
            $row = pg_fetch_assoc($r);
            $published_form_id = ($row['published_form_id']!='') ? $row['published_form_id'] : $j->{'edit_form_id'}; //backward compatbility

            //draft form
            if ($j->{'method'} == 'draft') {
                $form_action = 'new-draft-form';
                if (preg_match("/(.+)(:draft)?(:draft_save)$/U",$j->{'form_name'},$m)) {
                    $form_base_name = $m[1];
                }
                $j->{'form_name'} = $form_base_name.":draft";
            }
            
            // update published form
            if ($j->{'method'} == 'update') {

                $form_action = 'update-form';
            }

            // publish a draft-version
            if ($j->{'method'} == 'publish') {
                
                $form_action = 'publish-draft-version';
            
                if (preg_match("/(.+):(draft)$/U",$j->{'form_name'},$m)) {
                    $form_base_name = $m[1];
                }
                $j->{'form_name'} = $form_base_name;
            }        
        }
    } else {

        if ($j->{'method'} == 'draft') {
            // New draft form
            $form_action = 'new-draft-form';
            if (preg_match("/(.+)(:draft)?(:draft_save)$/U",$j->{'form_name'},$m)) {
                $form_base_name = $m[1];
            }
            $j->{'form_name'} = $form_base_name.":draft";

        } elseif ($j->{'method'} == 'publish') {
            // New form
            $form_action = 'new-published-form';
        }
    }



    if (!is_array($j->{'form_type'})) $j->{'form_type'} = array($j->{'form_type'});
    if (!is_array($j->{'form_group_access'})) $j->{'form_group_access'} = array($j->{'form_group_access'});
    if (!is_array($j->{'form_data_access_read'})) $j->{'form_data_access_read'} = array($j->{'form_data_access_read'});
    if (!is_array($j->{'form_data_access_write'})) $j->{'form_data_access_write'} = array($j->{'form_data_access_write'});

    $tracklog_mode = $j->{'tracklog_mode'};
    $observationlist_mode = $j->{'observationlist_mode'};
    $observationlist_time = sprintf('%d',$j->{'observationlist_time'});
    $periodic_notification = sprintf('%d',$j->{'periodic_notification'});

    pg_query($BID,'BEGIN');

    $error = 0;
    if ($form_action == 'update-form') {
        //debug('update-form');
        $cmd = sprintf("INSERT INTO project_forms (user_id,project_table, form_name,form_type, form_access,active, groups,description, destination_table,srid, data_groups,data_owners,
                                published_form_id,observationlist_mode, observationlist_time,tracklog_mode, periodic_notification, project_schema) 
                        VALUES (%d,'%s', %s,%s, %s,1, %s,%s, %s,%s, %s,%s, %d,%s, %d,%s, %d, %s) RETURNING form_id",
                       $_SESSION['Tid'],PROJECTTABLE,quote($j->{'form_name'}),quote('{'.implode(',',$j->{'form_type'}).'}'),quote($j->{'form_access'}),quote('{'.implode(',',$j->{'form_group_access'}).'}'),
                        quote($j->{'form_description'}),quote($table),quote('{'.$j->{'form_srid'}.'}'),quote('{'.implode(',',$j->{'form_data_access_read'}).'}'),
                        quote('{'.implode(',',$j->{'form_data_access_write'}).'}'),$published_form_id,quote($observationlist_mode),$observationlist_time,quote($tracklog_mode),$periodic_notification,quote($schema));
        $res = pg_query($BID,$cmd);
        $form_id = '';
        if ($res) {
            $row = pg_fetch_assoc($res);
            $form_id = $row['form_id'];
        } else { 
            $error++;
            log_action('Update form failed',__FILE__,__LINE__);
            log_action($cmd,__FILE__,__LINE__);
        }

    } 
    elseif ($form_action == 'publish-draft-version') {
        //debug('publish-draft-version');
        # create new published version from a draft version
        // no published timestamp
        $cmd = sprintf("INSERT INTO project_forms (user_id,project_table, form_name,form_type, form_access,active, groups,description, destination_table,srid, data_groups,data_owners,
                                published_form_id,observationlist_mode, observationlist_time,tracklog_mode, periodic_notification, project_schema) 
                        VALUES (%d,'%s', %s,%s, %s,1, %s,%s, %s,%s, %s,%s, %d,%s, %d,%s, %d, %s) RETURNING form_id",
                       $_SESSION['Tid'],PROJECTTABLE,quote($j->{'form_name'}),quote('{'.implode(',',$j->{'form_type'}).'}'),quote($j->{'form_access'}),quote('{'.implode(',',$j->{'form_group_access'}).'}'),
                        quote($j->{'form_description'}),quote($table),quote('{'.$j->{'form_srid'}.'}'),quote('{'.implode(',',$j->{'form_data_access_read'}).'}'),
                        quote('{'.implode(',',$j->{'form_data_access_write'}).'}'),$published_form_id,quote($observationlist_mode),$observationlist_time,quote($tracklog_mode),$periodic_notification,quote($schema));
        $res = pg_query($BID,$cmd);
        $form_id = '';
        if ($res) {
            $row = pg_fetch_assoc($res);
            $form_id = $row['form_id'];
            // Drop the draft version
            pg_query($BID,sprintf("DELETE FROM project_forms WHERE form_id=%d",$edit_form_id));
        } else { 
            $error++;
            log_action('Creating form failed',__FILE__,__LINE__);
            log_action($cmd,__FILE__,__LINE__);
        }

    }
    elseif ($form_action == 'new-published-form') {
        //debug('new-form');
        # create new form

        // published_form_id created by the trigger
        $cmd = sprintf("INSERT INTO project_forms (user_id,project_table, form_name,form_type, form_access,active, groups,description, destination_table,srid, data_groups,data_owners,
                                observationlist_mode,observationlist_time, tracklog_mode,periodic_notification, project_schema) 
                        VALUES (%d,'%s', %s,%s, %s,1, %s,%s, %s,%s, %s,%s, %s,%d, %s, %d, %s) RETURNING form_id",
                        $_SESSION['Tid'],PROJECTTABLE, quote($j->{'form_name'}),quote('{'.implode(',',$j->{'form_type'}).'}'), quote($j->{'form_access'}), quote('{'.implode(',',$j->{'form_group_access'}).'}'),
                        quote($j->{'form_description'}), quote($table),quote('{'.$j->{'form_srid'}.'}'), quote('{'.implode(',',$j->{'form_data_access_read'}).'}'),
                        quote('{'.implode(',',$j->{'form_data_access_write'}).'}'),quote($observationlist_mode), $observationlist_time,quote($tracklog_mode),$periodic_notification,quote($schema));
        $res = pg_query($BID,$cmd);
        $form_id = '';
        if ($res) {
            $row = pg_fetch_assoc($res);
            $form_id = $row['form_id'];
        } else { 
            $error++;
            log_action('Creating new form failed',__FILE__,__LINE__);
            log_action($cmd,__FILE__,__LINE__);
        }
    } 
    elseif ($form_action == 'new-draft-form') {
        //debug('new-draft-form');
        # create new draft form

        // no published timestamp
        $cmd = sprintf("INSERT INTO project_forms (user_id,project_table, form_name,form_type, form_access,active, groups,description, destination_table,srid, data_groups,data_owners,
                                published_form_id,observationlist_mode, observationlist_time,tracklog_mode, periodic_notification, draft, project_schema) 
                        VALUES (%d,'%s', %s,%s, %s,1, %s,%s, %s,%s, %s,%s, %d,%s, %d,%s, %d, true, %s) RETURNING form_id",
                       $_SESSION['Tid'],PROJECTTABLE,quote($j->{'form_name'}),quote('{'.implode(',',$j->{'form_type'}).'}'),quote($j->{'form_access'}),quote('{'.implode(',',$j->{'form_group_access'}).'}'),
                        quote($j->{'form_description'}),quote($table),quote('{'.$j->{'form_srid'}.'}'),quote('{'.implode(',',$j->{'form_data_access_read'}).'}'),
                        quote('{'.implode(',',$j->{'form_data_access_write'}).'}'),$published_form_id,quote($observationlist_mode),$observationlist_time,quote($tracklog_mode),$periodic_notification,quote($schema));
        $res = pg_query($BID,$cmd);
        $form_id = '';
        if ($res) {
            $row = pg_fetch_assoc($res);
            $form_id = $row['form_id'];
            // Drop the previos draft version if exists
            if ($edit_form_id != '')
                pg_query($BID,sprintf("DELETE FROM project_forms WHERE draft=true AND form_id=%d",$edit_form_id));
        } else { 
            $error++;
            log_action('Creating form failed',__FILE__,__LINE__);
            log_action($cmd,__FILE__,__LINE__);
        }
    } 
        


    // insert into project_forms_data
    if ($form_id!='' and $error==0) {
        // update form group
        if ($j->{'form_list_group'} != '') {
            $res = pg_query($BID,sprintf("SELECT max(c_order)+1 AS c FROM project_forms_groups WHERE project_table='%s'",PROJECTTABLE));
            $c_order = 1;
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $c_order = ($row['c'] == 0) ? 1 : $row['c'];
            }

            $res = pg_query($BID,sprintf("SELECT 1 FROM project_forms_groups WHERE name=%s AND project_table='%s'",quote($j->{'form_list_group'}),PROJECTTABLE));
            if (pg_num_rows($res)) {
                $cmd = sprintf("UPDATE project_forms_groups SET form_id = ARRAY_REMOVE(form_id, %d::smallint) WHERE project_table='%s' AND name=%s",$j->{'edit_form_id'},PROJECTTABLE,quote($j->{'form_list_group'}));
                $res = pg_query($BID,$cmd);
                $cmd = sprintf("UPDATE project_forms_groups SET form_id = form_id || %d::smallint WHERE project_table='%s' AND name=%s",$form_id,PROJECTTABLE,quote($j->{'form_list_group'}));

            } else {

                $cmd = sprintf("INSERT INTO project_forms_groups (name, form_id, c_order, project_table) VALUES (%s,%s,%d,'%s')",quote($j->{'form_list_group'}),"'{".$form_id."}'",$c_order,PROJECTTABLE);
            
            }
            $res = pg_query($BID,$cmd);
        }

        
        $dbcolist = dbcolist('array',$table, $schema);
        $n = 0;
        foreach($j->{'id'} as $id) {
            $pl = '';
            //autocomplete
            if ($j->{'type'}[$n]=='autocomplete') {
                // old style to new
                if (!preg_match('/^\{/',$j->{'list'}[$n])) {
                    $m = preg_split("/\./",$j->{'list'}[$n]);
                    $j->{'list'}[$n] = '{"optionsTable":"'.$m[0].'","valueColumn":"'.$m[1].'","labelColumn":""}';
                }
                $pl = $j->{'list'}[$n];
            }
            elseif ($j->{'type'}[$n]=='list' or $j->{'list'}[$n]!='') {
                
                // old style processing
                if (!preg_match('/^\{/',$j->{'list'}[$n])) {
                    $PATTERN = '/SELECT:(\w+)\[?(\w+(?:(?:,\w+)+)?)?\]?\.(\w+):?(\w+)?/'; //pattern with filter (eg. SELECT:table[filter_col,filter_value].value:label)
                    if (preg_match($PATTERN,$j->{'list'}[$n],$m)) {
                        $label = '';
                        if (isset($m[4]))
                            $label = $m[4];

                        if ($m[2]!='') {
                            $pmp = preg_split('/,/',$m[2]);
                            $preFilterColumn = $pmp[1];
                            $preFilterValue = $pmp[2];
                            $list_definition = array('optionsTable'=>$m[1],'preFilterColumn'=>$preFilterColumn,'preFilterValue'=>$preFilterValue,'valueColumn'=>$m[3],'labelColumn'=>$label);
                        } else {
                            $list_definition = array('optionsTable'=>$m[1],'valueColumn'=>$m[3],'labelColumn'=>$label);
                        
                        }

                    } else {
                        $list_definition = array();
                        // Simple new line separated list processing
                        $x = preg_split("/\n/",$j->{'list'}[$n]);
                        $a = array(""=>[]); // Adding the empty element at the beginning by default
                        foreach ($x as $elem) {
                                $a[$elem] = array($elem);
                        }
                        /* What is this?                        
                        $list = preg_split('/,/',$j->{'list'}[$n]);
                        foreach ($list as $l) {
                            $lv = preg_split('/:/',$l);
                            if ($lv[0] === '_empty_') $lv[0] = '';
                            if (count($lv)>1)
                                $list_definition[trim($lv[1])] = array_map('trim',explode('#',$lv[0]));
                            else
                                $list_definition[trim($lv[0])] = array();
                        }*/

                        $list_definition = array("list"=>$a);
                    }

                    $pl = json_encode($list_definition,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_PRESERVE_ZERO_FRACTION);
                } else {
                    //list
                    //$pl = '{"'.implode('","',$list).'"}';
                    /*{
                        "list": {
                                "val1": ["label1", "label2"]
                        },
                        "optionsTable": "",
                        "valueColumn": "",
                        "labelColumn": "",
                        "filterColumn": "",
                        "pictures": {
                                "val1": "url-string"
                        },
                        "triggerTargetColumn": "",
                        "Function": "",
                        "disabled": ["val1"],
                        "preFilterColumn": "",
                        "preFilterValue": ""
                    }*/
                    $pl = $j->{'list'}[$n];
                }
            }
            //default value
            if ($j->{'default_value'}[$n]=='') $def_val = 'NULL';
            else $def_val = quote($j->{'default_value'}[$n]);

            //api params
            $api_params = $j->{'api_params'}[$n];
            if (is_array($api_params))
                $ap = '["'.implode('","',$api_params).'"]';
            else
                $ap = '[]';

            //if($j->{'api_params'}[$n]=='') $api_params = 'NULL';
            //else $api_params = quote($j->{'api_params'}[$n]);
            
            if ($j->{'relation'}[$n]=='') $relation = 'NULL';
            else $relation = quote($j->{'relation'}[$n]);

            $regexp = NULL;
            $custom_fun = NULL;
            $spatial = quote('');
            $pseudo_columns = quote('');
            //control
            if ($j->{'length'}[$n] == 'regexp') {
                $regexp = $j->{'count'}[$n];
                $j->{'count'}[$n] = NULL;
            }
            elseif ($j->{'length'}[$n] == 'spatial' and $j->{'count'}[$n] !='' ) {
                $spatial = sprintf("ST_GeomFromText('%s')",$j->{'count'}[$n]);
                $j->{'count'}[$n] = NULL;
            }
            elseif ($j->{'length'}[$n] == 'custom_check' and $j->{'count'}[$n] !='' ) {
                $custom_fun = $j->{'count'}[$n];
                $j->{'count'}[$n] = NULL;
            }
            
            if ($j->{'pseudo_columns'}[$n] == '') $pseudo_columns = 'NULL';
            else $pseudo_columns = quote($j->{'pseudo_columns'}[$n]);

            $j->{'count'}[$n] = '{'.preg_replace('/:/',',',$j->{'count'}[$n]).'}';

            $column_label = ($j->{'column_label'}[$n] === $dbcolist[$id]) ? '' : $j->{'column_label'}[$n];
            
            //SQL INSERT
            $cmd = sprintf("INSERT INTO project_forms_data (form_id,\"column\",description,type,control,list_definition,
                                                    count,regexp,obl,fullist,default_value,
                                                    api_params,relation,spatial,pseudo_columns,custom_function,position_order,column_label) 
                                            VALUES (%d,%s,%s,%s,%s,%s,
                                                    %s,%s,%d,%d,%s,
                                                    %s,%s,%s,%s,%s,%s,%s)",
            $form_id,quote($id),quote($j->{'description'}[$n]),quote($j->{'type'}[$n]),quote($j->{'length'}[$n]),quote($pl),
            quote($j->{'count'}[$n]),quote($regexp),$j->{'obl'}[$n],$j->{'fullist'}[$n],$def_val,
            quote($ap),$relation,$spatial,$pseudo_columns,quote($custom_fun),quote($j->{'position_order'}[$n]),quote($column_label));

            $res = pg_query($BID,$cmd);
            if (!$res) {
                $error++;
                log_action('From edit action failed',__FILE__,__LINE__);
                log_action($cmd,__FILE__,__LINE__);
            }
            $n++;
        }
    }

    if ( $error>0 ) {
        $result = pg_query($BID,"ROLLBACK");
        echo common_message('error',"Failed to edit form. See logs...".pg_last_error($BID));
    } else {
        $result = pg_query($BID,"COMMIT");
        echo common_message('ok','done');
    }
    exit;
    #print $_SESSION['upl_form_listfile'];
}


/* modules
 *
 *
 * */
if (isset($_POST['update_modules'])) {
    if (!has_access('modules')) exit;
    
    $mtable = isset($_POST['mtable']) ? $_POST['mtable'] : PROJECTTABLE;
    $sp = preg_split('/\./',$mtable);
    if (count($sp) == 2) {
        $mtable = preg_replace("/[^a-z0-9_]/i","",$sp[1]);
        $mtable_schema = preg_replace("/[^a-z0-9_]/i","",$sp[0]);
    }

    if (isset($_POST['module_type']) and $_POST['module_type'] == 'project_modules') {
        $m = new x_modules();
        $module_type = 'project';
    
    } else {
        $m = new modules($mtable);
        $module_type = 'table';
    }

    $id = preg_replace("/[^0-9new]/","",$_POST['update_modules']);
    $name = preg_replace("/[^A-Za-z0-9-_]/","",$_POST['name']);
    // preg_match test for legal directories??
    //
    $path = $_POST['file'];
    if ($path == 'system') $path = 'includes/modules/';
    elseif ($path == 'local') $path = 'local/includes/modules/';

    $enab = preg_replace("/[^tf]/i","",$_POST['enab']);
    $enab = preg_replace("/t/i","TRUE",$enab);
    $enab = preg_replace("/f/i","FALSE",$enab);
    $access = preg_replace("/[^0-2]/","",$_POST['access']);
    $gaccess = (isset($_POST['gaccess'])) ? preg_replace("/[^0-9,]/","",$_POST['gaccess']) : '';
    if ($gaccess == '') $gaccess = array(0);

    if (!is_array($gaccess)) $gaccess = array($gaccess);

    $formatted_params = "";

    // new_params formatting
    if (!preg_match('/^[[{]/',$_POST['params'])) {
        // NON JSON
        $pa = array();
        $p = preg_split("/\n/",$_POST['params']); 
        foreach ($p as $param) {
            if ($param == "") continue;
            $param = preg_replace("/,/","**",$param);
            $param = preg_replace('/"/','\"',$param);
            $pa[] = $param;
        }

        # processing "modszer:shunt","hely:geom" ...
        # Ezt egyelőre nem lehet általánosítani, mert az egyes modulokban egyéni feldolgozás van az összetett struktúrákra....
        
        $p = array();
        $pa_update = 0;
        foreach ($pa as $param) {
            $c = preg_split('/[:=]/',$param);
            if (count($c) > 1) {
                $p[$c[0]] = $c[1];
                $pa_update++;
            }
            else {
                $p[] = $param;
            }
        }
        if ($pa_update == count($pa))
            $pa = $p;
        

        $new_params = json_encode($pa);
    } else {
        $new_params = $_POST['params'];

        if (trim($name)!='') { 
            
            $e = is_json($new_params,true);
            if ($e!==true) {
                echo common_message('error',$e);
                exit;
            } 
        }
    }

    ##### Old params
    #$params = array();
    #if (!is_numeric($_POST['params']) && !is_null(json_decode($_POST['params']))) {
    #    $params[] = 'JSON:'.base64_encode(json_encode(json_decode($_POST['params'])));
    #}
    #else {
    #    $p = preg_split("/\n/",$_POST['params']); 
    #    foreach ($p as $param) {
    #        if ($param == "") continue;
    #        $param = preg_replace("/,/","**",$param);
    #        $param = preg_replace('/"/','\"',$param);
    #        $params[] = $param;
    #    }
    #}
    #$params = implode(',',$params);
    #$params = "{".preg_replace("/\*\*/","\,",$params)."}";
    #####


    if ($id=='new') {
        $content = file(getenv('PROJECT_DIR').$path.$name.'.php');
        $process = 0;
        $yaml = "";
        foreach ($content as $rows) {
            if (($process === 0 or $process == 2 ) and preg_match("/^#'/",$rows)) {
                $process = 2;
            } elseif ($process){
                $process = 1;
                continue;
            }
            if ($process == 2) {
                $yaml .= preg_replace("/^#' /","",$rows);
            }
        }
        if ($yaml != '') {
            $parsed = yaml_parse($yaml);
            $methods = $parsed['Methods'];
            $examples = isset($parsed['Examples']) ? $parsed['Examples'] : '';
            $module_type = $parsed['Module-type'];
            $dependencies = isset($parsed['Dependencies']) ? $parsed['Dependencies'] : '';
        } else {
            echo common_message('error', "$name: Bad module format, yaml header missing.");
            log_action("$name: Bad module format, yaml header missing.",__FILE__,__LINE__);
            return;
        }
        if ($_POST['module_type'] == 'modules' and $module_type == 'both') {
            $module_type = 'table';
        } 
        if ($_POST['module_type'] == 'project_modules' and $module_type == 'both') {
            $module_type = 'project';
        }

        $row = array(
            'path' => $path,
            'module_name' => $name,
            'enabled' => $enab,
            'main_table' => $mtable,
            'new_params' => $new_params,
            'methods' => $methods,
            'examples' => $examples,
            'module_type' => $module_type
        );
        $res = $m->module_registration($row,'insert');

        if ($res) {
            if (isset($_POST['module_type']) and $_POST['module_type'] == 'project_modules') {
                $m = new x_modules();
            } else {
                $m = new modules($mtable);
            }
            $result = $m->_include($name,'init', compact('mtable', 'id'));
            if (isset($result['error'])) {
                echo common_message('error', $result['error']);
            }
            else {
                echo common_message('ok',"NEW:$mtable");
                if ($dependencies != '') {
                    // register dependencies
                    //$res = $m->module_registration($row,'insert');
                }
            }
        }
        else {
            echo common_message('error',pg_last_error($BID));
        }

    } else {
        
        $action = (trim($name)=='') ? 'delete' : 'update';

        $cmd = ($action == 'delete') 
            ? sprintf('DELETE FROM modules WHERE id=\'%d\' AND project_table=%s AND main_table=%s',$id,quote(PROJECTTABLE),quote($mtable))
            : sprintf('UPDATE modules SET params=NULL,module_name=%1$s,enabled=%2$s,module_access=%3$d,group_access=%4$s,new_params=%5$s
                    WHERE id=\'%6$d\' AND project_table=%7$s AND main_table=%8$s',
                quote($name),
                quote($enab),
                $access,
                quote('{'.implode(',',$gaccess).'}'),
                quote($new_params),
                $id,
                quote(PROJECTTABLE),
                quote($mtable),
            );

        //running the destroy method of the module if exists
        if ($action == 'delete') {
            preg_match('/([a-zA-Z0-9_.-]+).php$/', $path, $n);
            $destroy = $m->_include($n[1],'destroy',['mtable'=> $mtable],true);
        }

        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res)) {
            if (isset($_POST['module_type']) and $_POST['module_type'] == 'project_modules') {
                $m = new x_modules();
            } else {
                $m = new modules($mtable);
            }
            //module initialization
            if ($action == 'update' && $enab) {
                $result = $m->_include($name,'init', compact('mtable', 'id'));
                if (isset($result['error'])) {
                    echo common_message('error', $result['error']);
                }
                else {
                    echo common_message('ok',"UPDATE:$mtable");
                }
            }
            else {
                echo common_message('ok',"DROP:$mtable");
            }
        }
        else {
            echo common_message('error',pg_last_error($BID));
            log_action('module update failed',__FILE__,__LINE__);
        }
    }
    exit; 
}
/* database
 * New table from csv file
 *
 * */
if (isset($_POST['csv_file_to_table'])) {

    # Checking csvkit installed

    # Insert data into existing table
    # csvsql --db "postgresql://xxx_admin:password@localhost:port/gisdata" --insert file.csv
    #
    # Create table on the file
    # csvsql --db "postgresql://xxx_admin:password@localhost:port/gisdata" --insert --create-if-not-exists --db-schema "xxx" file.csv
}

/*Project admin -> map settings - set map centre */

if (isset($_POST['map_centre_set'])) {
    $cmd = sprintf("UPDATE project_variables SET map_center=ST_SetSRID(ST_GeomFromText('%s'),4326),map_zoom=%d WHERE project_table='%s'",
        preg_replace('/[^point()0-9. ]/i','',$_POST['map_centre_set']),
        $_POST['map_zoom_set'],
        PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    echo common_message('ok',"set");
}
?>
