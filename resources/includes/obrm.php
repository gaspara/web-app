<?php
/**
* 
*/
abstract class Entity {
    protected $tableSchema = 'public';
    protected $tableName;
    protected $pk_col = 'id';
    protected $unique_cols = null;
    protected $db = 'gisdata';
    protected $db_conn;
    protected $identify_record = "";
    protected $mode;
    
    private static $relations = ['=', '>', '>=', '<', '<=', '!=', 'LIKE', 'ILIKE', 'IN', 'NOT IN', 'IS', 'IS NOT'];
    private $data_types;
    
    public function __construct() {
        if ($this->db === 'gisdata') {
            if (!$this->db_conn = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) {
                die("Unsuccessful connect to GIS databases.");
            }
        }
        elseif ($this->db === 'biomaps') {
            if (!$this->db_conn = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host)) {
                die("Unsuccessful connect to UI databases.");
            }
        }
        elseif ($this->db === 'gisadmin') {
            if (!$this->db_conn = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))  {
                die("Unsuccessful connect to UI databases.");
            }
        }
        else {
            die("Invalid database: {$this->db}");
        }
        if (get_called_class() !== 'DbTableColumns') {
            $data_types = obm_cache('get',"DbTableColumns.{$this->tableSchema}.{$this->tableName}");
            if ($data_types !== FALSE) {
                $this->data_types = $data_types;
            }
            else {
                $columns = DbTableColumns::find([
                    ['table_schema', $this->tableSchema],
                    ['table_name', $this->tableName]
                ]);
                
                $this->data_types = array_combine(
                    array_column($columns, 'column_name'),
                    array_column($columns, 'data_type')
                );
                
                obm_cache('set', "DbTableColumns.{$this->tableSchema}.{$this->tableName}", $this->data_types, 300);
            }
        }

        
    }
    
    public function save() {
        $class = new \ReflectionClass($this);
        
        $propsToImplode = [];
        
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            $propName = $property->getName();
            
            if ($this->$propName === null) {
                continue;
            }
            if ($propName === $this->pk_col) {
                continue;
            }

            switch ($this->data_types[$propName]) {
                case 'integer':
                    $value = (int)$this->{$propName};
                    break;
                case 'json':
                case 'jsonb':
                    $value = json_encode($this->{$propName});
                    break;
                case 'ARRAY':
                    $val = (!is_array($this->{$propName})) ? [$this->{$propName}] : $this->{$propName};
                    $value = "{" . implode(',', $this->quArr($val)) . "}";
                    break;
                default:
                    $value = $this->{$propName};
                    break;
            }
            
            $propsToImplode['set'][] = "\"$propName\" = " . quote($value);
            $propsToImplode['names'][] = $propName;
            $propsToImplode['values'][] = $value;
        }
        
        if ($this->mode === 'update') {
            // update
            $setClause = implode(',',$propsToImplode['set']); // glue all key value pairs together
            $cmd = sprintf($this->updateStatement(), $this->tableSchema, $this->tableName, $setClause);
        }
        elseif ($this->mode === 'insert') {
            // insert
            $columnsClause = implode(',', $this->quArr($propsToImplode['names']));
            $valuesClause = implode(',', $this->quArr($propsToImplode['values'], 'safe'));
            $cmd = sprintf($this->insertStatement(), $this->tableSchema, $this->tableName, $columnsClause, $valuesClause);
        }
        else {
            // upsert for tables with combined unique indexes
            $setClause = implode(',',$propsToImplode['set']); // glue all key value pairs together
            $columnsClause = implode(',', $this->quArr($propsToImplode['names']));
            $valuesClause = implode(',', $this->quArr($propsToImplode['values'], 'safe'));
            $cmd = sprintf($this->upsertStatement(), $this->tableSchema, $this->tableName, $columnsClause, $valuesClause, $setClause);
        }
        
        if (method_exists($this, 'before_save')) {
            $this->before_save();
        }
        if (!$res = pg_query($this->db_conn, $cmd)) {
            log_action("QUERY ERROR: $cmd", __FILE__, __LINE__);
            throw new \Exception("query error", 1);
        }
        
        if (method_exists($this, 'after_save')) {
            $this->after_save();
        }
        $results = pg_fetch_assoc($res);
        return $results;
    }
    
    protected function upsertStatement() {
        return sprintf('INSERT INTO %%s.%%s (%%s) VALUES (%%s) ON CONFLICT (%1$s) DO UPDATE SET %%s RETURNING %1$s;',
            implode(',', $this->quArr($this->unique_cols))
        );
    }
    
    protected function updateStatement() {
        return sprintf("UPDATE %%s.%%s SET %%s WHERE %s RETURNING %s;", $this->identify_record, $this->pk_col);
    }
    
    protected function insertStatement() {
        return sprintf("INSERT INTO %%s.%%s (%%s) VALUES (%%s) RETURNING %s;", $this->pk_col);
    }
    
    public function delete() {
        if ($this->mode === 'insert') {
            throw new \Exception("New records can not be deleted", 1);
        }
        if (method_exists($this, 'before_delete')) {
            $this->before_delete();
        }
        $cmd = sprintf("DELETE FROM %s.%s WHERE %s;", $this->tableSchema, $this->tableName, $this->identify_record);
        if (!$res = pg_query($this->db_conn, $cmd)) {
            throw new \Exception(pg_last_error($res));
        }
        if (method_exists($this, 'after_delete')) {
            $this->after_delete();
        }
        return true;
    }
    
    /** STATIC FUNCTIONS FOR GETTING THE MODELS FROM THE DB **/
    
    public static function morph(array $object, $initialize_options = []) {
        $class = new \ReflectionClass(get_called_class()); // this is static method that's why i use get_called_class
        $entity = $class->newInstance();
        
        foreach($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $prop) {
            $propName = $prop->getName();
            if (isset($object[$propName])) {
                switch ($entity->data_types[$propName]) {
                    case 'integer':
                        $value = (int)($object[$propName]);
                        break;
                    case 'ARRAY':
                        $value = (is_array($object[$propName])) ? $object[$propName] : json_decode($object[$propName]);
                        break;
                    case 'json':
                    case 'jsonb':
                        $value = (is_array($object[$propName])) ? $object[$propName] : json_decode($object[$propName], true);
                        break;
                    default:
                        $value = $object[$propName];
                        break;
                }
                $prop->setValue($entity, $value);
            }
            else {
                $prop->setValue($entity, NULL);
            }
        }
        
        $entity->initialize0();
        $entity->initialize($initialize_options);
        
        return $entity;
    }
    
    private function initialize0() {
        if (is_array($this->unique_cols) && count($this->unique_cols)) {
            $this->mode = 'upsert';
            $this->identify_record = implode(' AND ', array_map(function ($col) {
                return sprintf("%s = %s", $col, quote($this->$col));
            }, $this->unique_cols));
        }
        else if (is_string($this->pk_col) && isset($this->{$this->pk_col})) {
            if ($this->{$this->pk_col} > 0) {
                $this->mode = 'update';
                $this->identify_record =  sprintf("%s = %s", $this->pk_col, quote($this->{$this->pk_col}));
            }
            else {
                $this->mode = 'insert';
            }
        }
        else {
            $this->mode = 'read-only';
        }
    }
    
    public function initialize($options) {
        return;
    }
    
    public static function filter_results($results) {
        return $results;
    }
    
    public static function find($options = [], $order = []) {
        if (empty($options)) {
            throw new \Exception('query options missing');
        }
        
        // if there is only one condition no need for double array
        if (is_string($options[0])) {
            $options = [$options];
        }
        
        $class = new \ReflectionClass(get_called_class());
        $entity = $class->newInstance();
        
        $selectList = implode(', ', array_map(function ($prop) use ($entity) {
            $propName = $prop->getName();
            return (isset($entity->data_types[$propName]) && $entity->data_types[$propName] === 'ARRAY') ? "array_to_json($propName) as $propName" : $propName;
        }, $class->getProperties(\ReflectionProperty::IS_PUBLIC)));
        
        
        $cmd = sprintf('SELECT %s FROM %s.%s', $selectList, $entity->tableSchema, $entity->tableName);
        
        $whereClause = " WHERE ".implode(' AND ', array_map(function ($option) {
            return self::prepareFilter($option);
        }, $options));


        $orderByClause = "";
        $propNames = array_map(function ($prop) {
            return $prop->getName();
        }, $class->getProperties(\ReflectionProperty::IS_PUBLIC));
        
        if (!empty($order) && in_array($order[0], $propNames)) {
            $dir = (isset($order[1]) && in_array(strtolower($order[1]), ['asc', 'desc'])) ? strtoupper($order[1]) : "ASC";
            $orderByClause = " ORDER BY {$order[0]} {$dir}";
        }
        
        $cmd = $cmd . $whereClause . $orderByClause;
        
        if (!$res = pg_query($entity->db_conn, $cmd)) {
            throw new \Exception(pg_last_error($entity->db_conn));
        }
        
        $result = [];
        while ($resRow = pg_fetch_assoc($res)) {
            $result[] = self::morph($resRow);
        }
        
        // filtering the results if there is a filter_results method in the child class
        $result = static::filter_results($result);
        
        return $result;
    }
    
    public static function first($options) {
        $results = self::find($options);
        return (count($results)) ? $results[0] : false;
    }
    
    public static function all() {
        return self::find([['id', 'NULL', 'IS NOT']]);
    }
    
    /** HELPER FUNCTIONS **/
    
    private static function prepareFilter($option) {
        if (count($option) < 2){
            throw new \Exception('column and value missing');
        }
        $col = $option[0];
        $val = $option[1];
        $rel = null;
        if (isset($option[2])) {
            if (!in_array($option[2], self::$relations)) {
                throw new \Exception("Invalid relation: " . $option[2]);
            }
            $rel = $option[2];
        }
        
        if (!is_string($col) && !preg_match('/^\w+$/', $col)) {
            throw new \Exception("column must be a string, containing only letters, numbers, underscore");
        }
        
        $whereCondition = "";
        if (!is_array($val)) {
            if (strtolower($val) === 'null') {
                $relation = $rel ?? 'IS';
                $value = 'NULL';
            }
            else {
                $relation = $rel ?? '=';
                $value = quote($val);
            }
        }
        else {
            $relation = $rel ?? 'IN';
            $value = "(".implode(',',array_map('quote',$val)) . ")";
        }
        
        return sprintf("%s %s %s", $col, $relation, $value);
    }
    
    public function qu($val, $q = '"') {
        if ($q == 'safe') {
            return quote($val);
        }
        return "$q{$val}$q";
    }
    
    public function quArr($arr, $q = '"') {
        $qArr = array_fill(0, count($arr), $q);
        return array_map([$this, 'qu'], $arr, $qArr);
    }
}

class DbTableColumns extends Entity {
    protected $tableSchema = 'information_schema';
    protected $tableName = 'columns';
    protected $data_types = [
        "column_name" => "character varying",
        "data_type" => "character varying",
    ];
    
    public $column_name;
    public $data_type;
}
?>
