<?php 
/**
 * 
 */
class DataExport {
    public $id = null;
    public $filename = null;
    public $user_id = null;
    public $status = null;
    public $message = null;
    public $downloaded = null;
    public $requested = null;
    public $valid_until = null;
    public $colnames = null;
    public $maxlength = 0;
    public $export_options = null;
    
    private $props = ['id', 'filename', 'user_id', 'status', 'message', 'downloaded', 'requested', 'valid_until', 'colnames', 'maxlength', 'export_options'];
    private $error = '';
    private $exists = false;

    public $data_header;
    
    function __construct($id = null) {
        if (is_null($id)) {
            return;
        }
        
        if (!is_int($id)) {
            $this->filename = $id;
            $de_data = $this->request_by_filename();
        }
        else {
            $this->id = $id;
            $de_data = $this->request_by_id();
        }
        foreach ($de_data as $key => $value) {
            if (property_exists($this, $key)) {
                if (in_array($key, ['id', 'user_id', 'downloaded', 'maxlength'])) {
                    $value = (int)$value;
                }
                if (in_array($key, ['colnames', 'export_options'])) {
                    $value = json_decode($value, true);
                }
                $this->$key = $value;
            }
        }
        
    }
    
    public function has_error() {
        return ($this->error !== '') ? $this->error : false;
    }
    
    public function is_downloadable() {
        if (!is_int($this->id)) {
            return 'Can\'t download an unsaved request';
        }
        
        $now = new DateTime();
        $i = $now->diff(new DateTime($this->valid_until));
        if ($i->format('%R') === '-') {
            return "Data export expired";
        }
        if (!file_exists($this->tmpfile())) {
            return 'The file does not exist!';
        }
        return true;
    }
    
    private function request_by_id() {
        global $ID;
        
        $cmd = sprintf("SELECT * FROM system.%s_data_exports WHERE id = %d;", constant('PROJECTTABLE'), $this->id);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        if (pg_num_rows($res) === 0) {
            $this->id = null;
            return [];
        }
        $this->exists = true;
        return pg_fetch_assoc($res);
    }
    
    private function request_by_filename() {
        global $ID;
        
        $cmd = sprintf("SELECT * FROM system.%s_data_exports WHERE filename = %s;", constant('PROJECTTABLE'), quote($this->filename));
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        if (pg_num_rows($res) === 0) {
            $this->filename = null;
            return [];
        }
        $this->exists = true;
        return pg_fetch_assoc($res);
    }
    
    public function exists() {
        return $this->exists;
    }
    
    public function increment_dl_count() {
        $this->downloaded++;
        $this->save();
    }
    
    public function set($key, $value) {
        if (!in_array($key, $this->props)) {
            return false;
        }
        $this->$key = $value;
        return true;
    }
    
    public function setArr($values) {
        foreach ($this->props as $key) {
            if (!isset($values[$key])) {
                continue;
            }
            $this->$key = (in_array($key, ['id', 'user_id', 'downloaded', 'maxlength'])) ? (int)$values[$key] : $values[$key];
        }
    }
    
    /**
     * This function creates a temporary csv file from the results of the query.
     * @param array $run_opts parameters passed when run as background process
     *
     * @return string 'status:id'
     */
    public function create_tmpfile($run_opts = null) {
        global $ID, $modules;
        
        $called_as_bgproc = (isset($run_opts['data_header']));

        $has_master_access = has_access('master');
        
        // this data transformation is because of passing data to the backgroud process
        $session = json_decode(json_encode($_SESSION), true);
        $data_header = ($called_as_bgproc) ? $run_opts['data_header'] : json_decode(json_encode($this->data_header), true);
        
        $session_id = $session['token']['session_id'];
        
        // TODO: ez a rendezés nem működik meg kell nézni
        $orderby = (isset($session['orderby'])) ? sprintf('ORDER BY %1$s %2$s',$session['orderby'],$session['orderascd']) : 'ORDER BY obm_id';        
            
        $cmd = (isset($session['ignore_joined_tables']) and $session['ignore_joined_tables'])
            ? sprintf('SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s',constant('PROJECTTABLE'), $session_id)
            : sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s %3$s',constant('PROJECTTABLE'), $session_id, $orderby);
        
        if (!$res = pg_query($ID, $cmd)) {
            $this->error = 'DataExport::create_tmpfile query error';
            $this->status = 'failed';
            $this->save();
            
            return $this->status . ":" . $this->id;
        }
        
        $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
        $tgroups = $u->roles ?? 0;
        $st_col = st_col($session['current_query_table'], 'array');
        
        // visible columns
        extract(allowed_columns($modules,$session['current_query_table']));

        //exclude national taxon_names in other languages than the user's defined 
        foreach($data_header['csv_header'] as $key => $cell) {
            if (in_array($key, $u->excluded_taxon_columns)) {
              unset($data_header['csv_header'][$key]);
              continue;
            }
        }
        
        $handle = fopen($this->tmpfile(), "w+");
        if (!$handle) {
            log_action("Failed to create tmp file in tmp folder. The ".constant('OB_TMP')." directory should exists and be writable for webserver!");
            $this->error = "Failed to create tmp file in tmp folder. The ".constant('OB_TMP')." directory should exists and be writable for webserver!";
            
            $this->status = 'failed';
            $this->save();
            
            return $this->status . ":" . $this->id;
        }

        
        while ($line = pg_fetch_assoc($res)) {
            //add meta columns
            $cell = '';
            
            // ski0pping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($line)==1) {
                if ($line[0]==$chk) continue;
                else $chk = $line[0];
            }
            
            $acc = rst('acc',$line['obm_id'], $session['current_query_table'], $has_master_access) ? true : false; 
            extract(data_access_check($acc,$st_col['USE_RULES'],$allowed_cols,$tgroups,$allowed_cols_module,$line));

            // print cells
            $row = [];
            foreach($data_header['csv_header'] as $k => $v) {
                
                // to show joined table values
                $kspl = preg_split('/\./', $k);
                $k = (count($kspl) == 2) ? $kspl[1] : $kspl[0];
                                
                if ($column_restriction) {
                    if ( !in_array($k,$allowed_cols) ) {
                        $line[$k] = 'NA';
                    }
                }
                elseif ($geometry_restriction ) {
                    if ( $k == 'obm_geometry')
                        $line[$k] = 'NA';
                    if ( !in_array($k,$allowed_gcols) ) {
                        $line[$k] = 'NA';
                    }
                }
                if ($modules->is_enabled('transform_data') && $k !== 'obm_geometry') {
                    //default itegrated module
                    if (isset($line[$k]))
                        $line[$k] = $modules->_include('transform_data','text_transform',array($line[$k],$k,''),true);
                    else
                        $line[$k] = "";
                }
                $row[] = $line[$k] ?? "";
            }
            $l = fputcsv($handle,$row);
            if ($l > $this->maxlength) {
                $this->maxlength = $l;
            }
        }
        
        fclose($handle);
        $this->colnames = $data_header['csv_header'];
        $this->set('valid_until', date('Y-m-d h:i:s', strtotime("+1 months", strtotime("NOW")) ));
        $this->status = ($run_opts['dlr'] === 'yes') ? 'pending' : 'ready';
        $this->save();
        
        return $this->status . ":" . $this->id;
    }
    
    /**
     * This function starts the export_data background process to create the 
     * temporary csv file with the create_tmpfile method of this class 
     * @param array $run_opts
     *
     * @return string 'status:id'
     */
    public function create_tmpfile_as_bgproc($run_opts) {
        require_once(getenv('OB_LIB_DIR') . 'admin_pages/jobs.php');
        
        ob_start();
        $p = new job_module('export_data', constant('PROJECTTABLE'));
        $p->set_filename('export_data.php');
        $p->set_runOpts($run_opts);
        $p->runJob('export_data');
        $job_status = json_decode(ob_get_clean());
        
        $status = ($job_status->status === 'success') ? "running" : $job_status->status;
        
        return "$status:{$this->id}";
    }

    public function tmpfile() {
        return constant('OB_TMP') . $this->filename;
    }
    
    public function save() {
        global $ID;
        if (is_int($this->id)) {
            $set = [];
            foreach ($this->props as $key) {
                if ($key === 'id' || $this->$key === null) {
                    continue;
                }
                $value = (in_array($key, ['colnames', 'export_options'])) ? json_encode($this->$key) : $this->$key;
                
                $set[] = "\"$key\" = " . quote($value);
            }
            $cmd = sprintf("UPDATE system.%s_data_exports SET %s WHERE id = %d RETURNING id;", constant('PROJECTTABLE'), implode(', ', $set), $this->id);
        }
        else {
            $keys = [];
            $values = [];
            foreach ($this->props as $key) {
                if ($key === 'id' || $this->$key === null) {
                    continue;
                }
                $keys[] = "\"$key\"";
                $values[] = (in_array($key, ['colnames', 'export_options'])) ? quote(json_encode($this->$key)) : quote($this->$key);
            }
            $cmd = sprintf("INSERT INTO system.%s_data_exports (%s) VALUES (%s) RETURNING id;", constant('PROJECTTABLE'), implode(',', $keys), implode(',', $values));
        }
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        $results = pg_fetch_assoc($res);
        $this->id = (int)$results['id'];
        return $this->id;
    }
    
    public function send_notification() {
        if (!is_int($this->id) || $this->id === 0) {
            return false;
        }
        $M = new Messenger();
        $u = ($this->user_id === $_SESSION['Tid']) ? unserialize($_SESSION['Tuser']) : new User($this->user_id);
        
        if ($this->status === 'ready') {
            $M->send_system_message($u, 'data_export_ready', [
                'username' => $u->name,
                'hash' => $u->hash
            ], true);
        }
        elseif ($this->status === 'pending') {
            $M->send_system_message($u, 'dlr_request_registered', [
                'username' => $u->name,
            ], true);
            
            $x_modules = new x_modules();
            
            $dlr_params = $x_modules->_include('download_restricted', 'get_params');
            $data_admins = new Role($dlr_params['data_admin_group'] ?? 'project masters');
            foreach ($data_admins->get_members() as $admin) {
                $M->send_system_message($admin, 'dlr_new_request', [
                    'username' => $u->name,
                    'requestid' => $this->id,
                    'request_message' => $this->message
                ], true);
                sleep(2);
            }
        }
        elseif ($this->status === 'approved') {
            $M->send_system_message($u, 'dlr_request_approved', [
                'username' => $u->name,
                'hash' => $u->hash
            ], true);
        }
        elseif ($this->status === 'rejected') {
            $M->send_system_message($u, 'dlr_request_rejected', [
                'username' => $u->name,
            ], true);
        }
        elseif ($this->status === 'failed') {
            log_action('TODO: levélküldés sikertelen export esetén');
        }
    }
    
    public function approve() {
        if (!is_int($this->id)) {
            log_action('Can\'t approve an unsaved request');
            return false;
        }
        $this->set('status', 'approved');
        $this->set('valid_until', date('Y-m-d h:i:s', strtotime("+1 months", strtotime("NOW")) ));

        if (!$this->save()) {
            return false;
        }
        $this->send_notification();
    }
    
    public function reject() {
        if (!is_int($this->id)) {
            log_action('Can\'t reject an unsaved request');
            return false;
        }
        $this->set('status', 'rejected');
        $this->set('valid_until', date('Y-m-d h:i:s'));

        if (!$this->save()) {
            return false;
        }
        $this->send_notification();
    }

    /**
     * TODO: implement a function that deletes the export file and the record from the db table
     *
     * @return void
     */
    public function destroy()
    {
        return null;
    }

    /**
     * Reads the temporary csv file and returns its content as an associative array
     *
     * @return array
     */
    public function as_array() {
        $arr = [];
        if ($handle = fopen($this->tmpfile(), "r")) {
            $colnames = array_keys($this->colnames);
            while (($row = fgetcsv($handle, $this->maxlength)) !== FALSE) {
                $arr[] = array_combine($colnames, $row);
            }
            fclose($handle);
        }
        return $arr;
    }
    
}

 ?>
