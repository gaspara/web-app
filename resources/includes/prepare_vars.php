<?php
/* ***************************************************************************  /

This file contains the variable transformations and function calls for
REQUEST POST GET variables and some general variables definitions
 
****************************************************************************** */
if (session_status() == PHP_SESSION_NONE) {
    //Ajaxos kérés esetén futunk ide
    //$lifetime = 3600;
    session_set_cookie_params([
        'lifetime' => 0,
        'path' => '/',
        'samesite' => 'Lax'
    ]);
    session_start();
    //session_regenerate_id(TRUE);
    //setcookie(session_name(),session_id(),time()+$lifetime,"/");
}

# call oauth layer 
require('auth.php');
require('prepare_auth.php');

# prepare global session variables
require('prepare_session.php');



/* Processing GET variables  ******************************************************** */

// Project table variables
//$qtable=PROJECTTABLE;
//$qtable_history=$qtable."_history";
if (isset($_GET['qtable'])) {

    if (preg_match('/\./',$_GET['qtable']))
        list($qs,$qt) = preg_split('/\./',$_GET['qtable']);
    else
        $qt = $_GET['qtable'];

    if (!preg_match('/_/',$qt)) {
        //query table can't be in other schema,other prefix!!!
        $cmd = sprintf('SELECT 1 FROM header_names WHERE f_project_name=\'%1$s\' and f_project_table=%2$s',
            PROJECTTABLE,quote(PROJECTTABLE."_".$qt));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $_SESSION['current_query_table'] = PROJECTTABLE."_".$qt;
        
        } else {
            if (defined('DEFAULT_TABLE'))
                $_SESSION['current_query_table'] = DEFAULT_TABLE;
            else
                $_SESSION['current_query_table'] = PROJECTTABLE;
        }
    } else {
        $cmd = sprintf('SELECT 1 FROM header_names WHERE f_project_name=\'%1$s\' and f_project_table=%2$s',
            PROJECTTABLE,quote($qt));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $_SESSION['current_query_table'] = $qt;
        
        } else {
            if (defined('DEFAULT_TABLE'))
                $_SESSION['current_query_table'] = DEFAULT_TABLE;
            else
                $_SESSION['current_query_table'] = PROJECTTABLE;
        }
        unset($_GET['qtable']);
    }
    unset($_SESSION['orderby']);
    // reloading session variables
    st_col($_SESSION['current_query_table'],'session');
}

/* GET and POST variables processing  **************************************** */
// language variables
if (isset($_GET['lang'])) {
    //if ( !isset($_SESSION['LANG']) or (isset($_SESSION['LANG']) and $_SESSION['LANG'] != $_GET['lang']) ) {
    //    $_SESSION['LANG_SET_TIME'] = time();
    //}

    // force update dbcolist
    $_SESSION['update_lang'] = 1;

    $_SESSION['LANG'] = preg_replace('/[^a-z]/i','',substr($_GET['lang'],0,2));
    unset($_GET['lang']);
    if (isset($_SESSION['Tcrypt'])) {
      set_preferred_lang($_SESSION['LANG'], $_SESSION['Trole_id']);
      setcookie("obm_lang_" . $_SESSION['Tcrypt'], $_SESSION['LANG'], time()+2592000,"/");
    }
    else {
      setcookie("obm_lang", $_SESSION['LANG'], time()+2592000,"/");
    }
    if (isset($_SESSION['current_page'])) {
        unset($_SESSION['current_page']['rev']);
        $_GET = $_SESSION['current_page'];
    }
}
$_SESSION['current_page'] = $_GET;

# default load pages
$load = array('load_database'=>0,'load_login'=>0,'load_mappage'=>0,'load_map'=>0,'load_useagreement'=>0,'load_showbookmarks'=>0,'load_datahistory'=>0,'load_uploadhistory'=>0,'load_profile'=>0,'load_register'=>0,'load_validhistory'=>0,'load_specieslist'=>0,'load_metaname'=>0,'load_data'=>0,'load_message'=>'','load_showuploads'=>0,'load_doimetadata'=>0,'load_showapikeys'=>0,'load_lostpw'=>0,'load_mainpage'=>0,'load_orcid_profile_data'=>0,'load_getquery'=>0,'load_bookmark'=>0,'load_loadmeta'=>0,'load_loaddata'=>0,'load_loaddoi'=>0,'load_userspecieslist'=>0,'load_showiups'=>0,'load_showowngeoms'=>0,'load_showsharedgeoms'=>0,'load_showserverlogs'=>0, 'load_messages'=>0);

# Regisztrációs doboz
if (isset($_GET['registration']) and $_GET['registration']!='') {
    # unset the current login variables
    unset($_SESSION['Tid']);
    unset($_SESSION['Tcrypt']);
    unset($_SESSION['Tname']);
    unset($_SESSION['Tmail']);
    unset($_SESSION['Tgroups']);
    unset($_SESSION['Tproject']);
    //$registration_code=preg_replace("/[^a-zA-Z0-9]/","",$_GET['registration']);
    $GET_invcode = $_GET['registration'];
} elseif (isset($_GET['registration']) and $_GET['registration']=='') {
    # unset the current login variables
    unset($_SESSION['Tid']);
    unset($_SESSION['Tcrypt']);
    unset($_SESSION['Tname']);
    unset($_SESSION['Tmail']);
    unset($_SESSION['Tgroups']);
    unset($_SESSION['Tproject']);
    
    if (isset($_POST['action']) && $_POST['action'] === 'request_invitation') {
        $req_sent = send_invitation_request();
        $load['load_message'] = ($req_sent) ? t(str_thanks_for_your_interest) : t(str_captcha_does_not_match);
        if ($req_sent) {
            $load['load_mappage'] = 1;
            $load['load_mainpage'] = 1;
        }
        $load['load_register'] = 1;
    }
    else {
        $load['load_register'] = 1;
    }
}
/*
elseif (isset($_GET['registration']) && ) {
    // code...
}*/
#print_r($_SESSION);
# Invited user's registration/first-login process
# 
if (isset($GET_invcode)) {

    $ret = inventer($GET_invcode);
    if ($ret === true) {
        if (isset($_SESSION['register_um'])) {
            $ret = ObmAuth::login($_SESSION['register_um'],$_SESSION['register_upw'],true);
            if ($ret === true) {
                //helper variable on first login after accepting invitation
                unset($_SESSION['register_um']);
                $load['load_profile'] = 1;
                //create news stream
                //TRANSLATION to project default lang!!
                insertNews("{$_SESSION['Tname']} joined to OpenBioMaps on ".PROJECTTABLE." project.",'project');
            } else {
                $load['load_message'] = str_login_error;
                $load['load_login'] = 1;
            }
        } else {
            $load['load_message'] = t('str_invitation_success_but_use_your_existing_account_to_login');
        }
    } else {
        $load['load_message'] = $ret;
    }
}

/*Outh authentication */
if (isset($_POST['loginenter'])) {

    $ret = ObmAuth::check_expired_password($_POST['loginname'],$_POST['loginpass'] );
    if ($ret === true) {

        $remember_me = (isset($_POST['remember_me']) && $_POST['remember_me'] === 'on');
        $ret = ObmAuth::login($_POST['loginname'],$_POST['loginpass'], $remember_me);
    
        if ($ret === FALSE) { /* Handle error */ 
            $load['load_message'] = t('str_login_error');
            $lang = $_SESSION['LANG'];
            Auth::destroy_session();
            session_start();
            $_SESSION['LANG'] = $lang;
            $load['load_login'] = 1;
        }
        else {
            unset($_GET['login']);
            # Profile page variable
            if (defined('LOGINPAGE')) {
                if (LOGINPAGE=='profile') { 
                    $load['load_profile'] = 1;
                    $load['load_mappage'] = 0;
                    $load['load_mainpage'] = 0;
                }
                elseif (LOGINPAGE=='mainpage') {
                    $load['load_mainpage'] = 1;
                    $load['load_profile'] = 0;
                    $load['load_mappage'] = 0;
                } else {
                    $load['load_mappage'] = 1;  //default LOGINPAGE is mappage
                    $load['load_profile'] = 0;
                    $load['load_mainpage'] = 0;
                }
            } else {
                // default action mappage
                $load['load_mappage'] = 1;
                $load['load_profile'] = 0;
                $load['load_mainpage'] = 0;
            }

            $load['load_login'] = 0;
        }
    } else {
        // expired password
        $load['load_message'] = $ret;
        $lang = $_SESSION['LANG'];
        Auth::destroy_session();
        session_start();
        $_SESSION['LANG'] = $lang;
        $load['load_login'] = 1;
    }
    unset($_GET['logout']);
}
/* ?logout
 *
 * */
if (isset($_GET['logout'])) {
    $savepages = array();
    //save some special pages
    if (isset($_GET['loaddoi']))
        $savepages['doi'] = $_GET['loaddoi'];

    if (isset($_GET['metadata']))
        $savepages['metadata'] = $_GET['metadata'];

        Auth::logout();

        if (isset($savepages['doi']))
        $_GET['loaddoi'] = $savepages['doi'];

    if (isset($savepages['metadata']))
        $_GET['metadata'] = $savepages['metadata'];
}


# Lost password
if (isset($_POST['lostpasswd'])) {
    if (ObmAuth::LostPw($_POST['loginmail'])) {
        $notice = str_message_sent;
    } else {
        $notice = str_no_such_mail_address;
    }
    if ($notice != '') {
        $load['load_message'] = $notice;
    }
}
/* Reactivate password
 * */
if (isset($_GET['activate']) and isset($_GET['addr'])) {

    // ezt le kellene vizsgálni, hogy csak xx percenként lehessen egyet beírni...
    if (strlen($_GET['activate'])!=16) $get_act='';
    else $get_act = $_GET['activate'];
    $get_addr = $_GET['addr'];
    
    $ret = reactivate($get_addr,$get_act);
    if ($ret===true) {
        $ret = ObmAuth::login($_SESSION['register_uuu'],$_SESSION['register_upw'],true);
        if ($ret===true) {
            unset($_SESSION['register_uuu']);
            $load['load_profile'] = 1;
        } else {
            $load['load_message'] = $ret;
            $load['load_login'] = 1;
        }
    } else {
        $load['load_message'] = $ret;
        $load['load_login'] = 1;

    }
    unset($_GET['activate']);
}


/* Server interconnect request handling 
 * API call, exit at end of call
 * */
if (isset($_GET['interconnect_request']) and isset($_GET['interconnect_server'])) {

    $ret = interconnect_request($_GET['interconnect_request'],$_GET['interconnect_server']);

    if ($ret===true) 
        echo common_message('OK','Request received');
    else
        echo common_message('failed',$ret);
    exit;
}
/* Local admin decision to accept or reject remote request
 * Link click call, load page with result
 */
if (isset($_GET['interconnect_request_key']) and isset($_GET['interconnect_accept_key']) and isset($_GET['decision']) and isset($_GET['interconnect_project'])) {

    $ret = interconnect_key_accept($_GET['decision'],$_GET['interconnect_accept_key'],$_GET['interconnect_request_key'],$_GET['interconnect_project']);
    if ($ret!==false) 
        $load['load_message'] = 'Interconnect key processed.';
    else
        $load['load_message'] = 'Interconnect key processing failed.';
}
/* Remote admin decision to accept or reject local request
 * API call, exit at end of call
 */
if (isset($_GET['interconnect_request_decision']) and isset($_GET['accept_key']) and isset($_GET['request_key'])) {
    $ret = interconnect_got_decision($_GET['interconnect_request_decision'],$_GET['accept_key'],$_GET['request_key']);
    if ($ret!==false) 
        echo common_message('OK',$ret);
    else
        echo common_message('failed','Decision processing failed');

    exit;
}
# general login variables 
# it should be after the logout vars! 
if (isset($_SESSION['Tid'])) {
    # Bejelentkezett felhasználó nem tudja betölteni a regisztrációs ablakot
    # Bejelentkezett felhasználó nem tudja betölteni a login ablakot
    $load['load_register']=0;
    $load['load_login']=0;
} else {
    $_SESSION['RST_LEVEL'] = 0;
}

# general _GET vars
#$get_urlvars = '';
#foreach($_GET as $key=>$value) {
#    if ($key=='lang') continue;
#    $get_urlvars.="$key=$value&";
#}

/* GET id
 * clean getid
 * */
if (!isset($_GET['id']) and isset($_SESSION['getid'])) { 
    unset($_SESSION['getid']);
    if (isset($_GET['ids']))
        unset($_SESSION['getids']);
}
// set getid (numeric)
if (isset($_GET['id']) and !is_array($_GET['id'])) {
    unset($_SESSION['getids']);
    $_SESSION['getid'] = preg_replace("/[^0-9]/", "", $_GET['id']);
    #simple handling of complex ids
    #$_SESSION['getid'] = preg_replace("/([0-9])/", "$1", $_GET['id']);
    if ($_SESSION['getid'] == '') return;
} elseif (isset($_GET['id']) and is_array($_GET['id']) and count($_GET['id'])) {
    $_SESSION['getids'] = array(); 
    foreach($_GET['id'] as $i) {
        $_SESSION['getids'][] = preg_replace("/[^0-9]/", "", $i);
    }
    array_filter($_SESSION['getids']);
}
// set getuid (alphanumeric)
if (isset($_GET['uid'])) {
    #simple handling of complex ids
    #$_SESSION['getuid'] = preg_replace("/([a-z0-9])/", "$1", $_GET['uid']);
    $_SESSION['getuid'] = preg_replace("/[^a-z0-9]/i", "", $_GET['uid']);
    if ($_SESSION['getuid'] == '') return;
}
#should be enabled???
#if (!isset($_GET['uid']) and isset($_SESSION['getuid'])) 
#    unset($_SESSION['getuid']);

if (isset($_GET['history']) and $_GET['history']=='upload')
    $load['load_uploadhistory'] = 1;

if (isset($_GET['data']))
    $load['load_data'] = 1;

if (isset($_GET['history']) and $_GET['history']=='data')
    $load['load_datahistory'] = 1;

if (isset($_GET['table']) and isset($_GET['history']) and $_GET['history']=='validation')
    $load['load_validhistory'] = $_GET['table'];

if (isset($_GET['termsandconditions']))
    $load['load_useagreement'] = 1;

//species metaname query
if (isset($_GET['metaname'])) 
    $load['load_metaname'] = 1;

// project metadata query
if (isset($_GET['metadata'])) {
    $load['load_doimetadata'] = 1;
    $_SESSION['LANG'] = 'en';
}
# login page
if (isset($_GET['login'])) {
    $load['load_login'] = 1;
}
# profile specific variables, these settings let people to access info pages with GET requests
if(isset($_GET['profile']) and  isset($_SESSION['Tid'])) {
    # profile page - subitems
    if (isset($_GET['showbookmarks']))
        $load['load_showbookmarks'] = 1;
    elseif (isset($_GET['showiups']))
        $load['load_showiups'] = 1;
    elseif (isset($_GET['showowngeoms']))
        $load['load_showowngeoms'] = 1;
    elseif (isset($_GET['showsharedgeoms']))
        $load['load_showsharedgeoms'] = 1;
    elseif (isset($_GET['showuploads']))
        $load['load_showuploads'] = 1;
    elseif (isset($_GET['showapikeys']))
        $load['load_showapikeys'] = 1;
    # set ORCID profile data
    elseif (isset($_GET['sopd'])) {
        $load['load_profile'] = 1;
        $load['load_orcid_profile_data'] = 1;

    } else {
        // user's hash id
        $load['load_profile'] = preg_replace("/[^a-z0-9]/i", "", $_GET['profile']);
        $_GET['get_own_profile'] = 2;
        if ($_GET['profile'] != 'profile' and $_SESSION['Tcrypt'] != $_GET['profile'])
            $_GET['get_own_profile'] = 0;
    }
}

if (isset($_GET['messages'])) { 
    $load['load_messages'] = 1;
    $load['load_profile'] = 0;
}

if (isset($_GET['admin'])) {
    if (isset($_GET['showserverlogs'])) {
        $load['load_showserverlogs'] = 1;
    }
}

if (isset($_GET['user_specieslist'])) { 
    $load['load_userspecieslist'] = $_GET['user_specieslist'];
    $load['load_profile'] = 0;
}

# Load LoadQuery meta page
if(isset($_GET['loaddata'])) {
    $load['load_loaddata'] = $_GET['loaddata'];
}
# Load LoadQuery meta page
if(isset($_GET['loadmeta'])) {
    $load['load_loadmeta'] = $_GET['loadmeta'];
}
# Load DOI page
if (isset($_GET['loaddoi'])) {
    $load['load_loadmeta'] = '__DOI__';
    $load['load_loaddoi'] = $_GET['loaddoi'];
}
# upload page
if (isset($_GET['upload']))
    $load['load_upload'] = 1;
# load queries
if (isset($_GET['Share'])) { 
    $load['load_loadmeta'] = $_GET['Share'];
}
if (isset($_GET['Share']) and isset($_GET['map'])) { 
    $load['load_loadmeta'] = 0;
    $load['load_loadquery'] = $_GET['Share'];
    $_GET['boxes'] = false;
}
# map page
# map=on coming from index.php if there is any GET parameter and no other path in the URL!!
if (isset($_GET['map'])) {
    $load['load_mappage'] = 1;
    if (($load['load_login'] || $load['load_register']) and !isset($_SESSION['Tid']))
        $load['load_mappage'] = 0;
}

if (isset($_GET['specieslist'])) {
    $load['load_specieslist'] = 1;
    $load['load_map'] = 0;
    $load['load_mappage'] = 0;
}

# load database page
if (isset($_GET['database'])) {
    $load['load_database'] = 1;
    $load['load_mappage'] = 0;
}
# lost password
if (isset($_GET['lostpw'])) {
    if (!isset($_SESSION['Tid'])) {
        $load['load_lostpw'] = 1;
        $load['load_mappage'] = 0;
    }
    else {
        $load['load_mainpage'] = 1;
    }
}
//debug($_GET);

# not used in the code, but let the possibility to call the mainpage
if (isset($_GET['mainpage'])) { 
    $load['load_map'] = 1;
    $load['load_mainpage'] = 1;
    $load['load_mappage'] = 0;
}
# profile: change own email address
if (isset($_GET['changeemail'])) {
    $code = preg_replace('/[^a-zA-Z0-9]/','',$_GET['changeemail']);
    if(strlen($code)==32) {
        $_SESSION['change_email_response'] = change_email($code);
    }
    $load['load_profile'] = 1;
}
# profile: drop profile
if (isset($_GET['drop_my_profile'])) {
    $code = preg_replace('/[^a-zA-Z0-9]/','',$_GET['drop_my_profile']);
    if(strlen($code)==32) {
        if (drop_profile($code))
            include(getenv('OB_LIB_DIR').'logout.php');
    }
}
# Set filter for text filter
if (isset($_GET['set_filter'])) {
    if (isset($_GET['column']) and $_GET['value']) {
        if ($_GET['value'] == '*') {
            unset($_SESSION['predefined_text_filter_column-'.$_SESSION['current_query_table']]);
            unset($_SESSION['predefined_text_filter_value-'.$_SESSION['current_query_table']]);
        } else {
            $_SESSION['predefined_text_filter_column-'.$_SESSION['current_query_table']] = $_GET['column']; 
            $_SESSION['predefined_text_filter_value-'.$_SESSION['current_query_table']] = preg_split("/,/",$_GET['value']);
        }
    }
}
# index: upload history
# other?
# The most frequently used API call by mobil client:
# http://openbiomaps.org/projects/openbiomaps_network/?query_api={"available":"up"}&orderby=institute&output=json&filename=
#
# Normal text filter like syntax for web output:
#       ?query=obm_observation_list_id:510fedb9-2dfe-44c0-baaa-09c69e5cf6b9&mousezoom=off&...
#
# JSON input if output is not web
#       ?query={"obm_observation_list_id":"510fedb9-2dfe-44c0-baaa-09c69e5cf6b9"}&output=json&filename=
#
# output: csv, json, ...?
if (isset($_GET['query_api'])) {
    // backward compatibility
    $_GET['query'] = $_GET['query_api'];
    // backward compatibility hack
    if (isset($_GET['filename'])) unset($_GET['filename']);
}
# GET queries using query_builder syntax
if (isset($_GET['query'])) {

    if (!isset($_GET['api_version'])) $api_version = 2.0;

    if (!isset($_GET['boxes']))
        $_GET['boxes'] = false;

    if (!isset($_GET['fixheader']))
        $_GET['fixheader'] = true;

    $load['load_getquery'] = 1;
    $load['load_mappage'] = 1;
    
    if (isset($_GET['output'])) {
        
        if (isset($_GET['orderby']))
            $_SESSION['orderby'] = preg_replace('/[^a-zA-Z0-9_]/','',$_GET['orderby']);

        if (isset($_GET['order']))
            $_SESSION['orderascd'] = ($_GET['order'] == 'desc') ? 'DESC' : 'ASC';


        $output = preg_replace('/[^a-z]+/','',strtolower($_GET['output']));
        if (!$modules->is_enabled('results_as'.strtoupper($output))) {
            echo "No $output module enabled";
            exit;
        }

        if (isset($_GET['filename']))
            $filename = basename($_GET['filename']);
        else
            $filename = $_SESSION['current_query_table'].'_['.date('Y-m-d').']_query.' . $output;

        // If output present, only JSON input allowed. Why? 
        $qs = (is_json($_GET['query'])) ? json_decode($_GET['query'],TRUE) : '';
        $m = array();
        if ($qs != '' and count($qs))
            foreach( $qs as $k=>$v) {
                if (!is_array($v)) {
                    $m["qids_$k"] = json_encode(array($v));
                } else {
                    $m["qids_$k"] = json_encode($v);
                }
            }
        if (isset($_GET['geom_selection'])) {
           $_REQUEST = array_merge($_REQUEST,$m);
        } else {
           $_REQUEST = $m;
        }

        $_SESSION['show_results']['type'] = $output;
        
        // create query string
        include(getenv('OB_LIB_DIR').'query_builder.php');
        
        // execute query
        include(getenv('OB_LIB_DIR').'results_query.php');

        // read and send results
        include_once(getenv('OB_LIB_DIR').'results_builder.php');
        $r = new results_builder(['method'=>$output,'filename'=>$filename]);
        if( $r->results_query() ) {
            if ($r->rq['results_count']) {
                $dl_status = $r->export();
                if (preg_match('/^ready:?(\d+)?$/', $dl_status, $m)) {
                    $print_data_params = ['de_id' => $m[1]];
                    if ($filename=='') $print_data_params['no_file'] = true;
                    $modules->_include('results_as'.strtoupper($output), 'print_data', $print_data_params);
                    unset($_SESSION['show_results']);
                    exit;
                }
            } 
            unset($_SESSION['show_results']);
            exit;
        }
        unset($_SESSION['show_results']);
        exit;
    } else {
        ## Load map page without right side query boxes and show selected points and load result area
        #
        // if parameters coming as json
        if (preg_match('/^{/',$_GET['query'])) {
            $qs = json_decode($_GET['query'],TRUE);
            $m = array();
            if ($qs != '' and count($qs))
                foreach( $qs as $k=>$v) {
                    if (!is_array($v)) {
                        $m["qids_$k"] = json_encode(array($v));
                    } else {
                        $m["qids_$k"] = json_encode($v);
                    }
                }
            if (isset($_GET['geom_selection'])) {
                $m['geom_selection'] = $_GET['geom_selection'];
                $m['geometry'] = isset($_GET['geometry']) ? $_GET['geometry'] : '';
            }
            $load['load_getquery_text'] = json_encode($m);
        } else {
            // if parameters coming as plain text
            // example:
            // index.php?query=faj:Pica pica;...
            $qs = preg_split('/;/',$_GET['query']);
            $m = array();
            $qs = array_filter($qs);
            if ($qs != '' and count($qs))
                foreach( $qs as $q) {
                    $kv = preg_split('/:/',$q);
                    $m["qids_$kv[0]"] = json_encode(array($kv[1]));
                }
            if (isset($_GET['geom_selection'])) {
                $m['geom_selection'] = $_GET['geom_selection'];
                $m['geometry'] = isset($_GET['geometry']) ? $_GET['geometry'] : '';
            }
            $load['load_getquery_text'] = json_encode($m);
        }
    }
}

# GET Report / stored queries
if (isset($_GET['bookmark'])) {

    unset($_SESSION['show_results']);
    $_SESSION['load_bookmark'] = 1;
    $load['load_bookmark_label'] = $_GET['bookmark'];
    $load['load_bookmark'] = 1;
    $load['load_mappage'] = 1;

    if (isset($_GET['output']) and in_array(strtolower($_GET['output']), array('csv','json'))) {

        $_SESSION['show_results']['type'] = strtolower($_GET['output']);
        $output = $_GET['output'];
        
        // create query string
        include(getenv('OB_LIB_DIR').'query_builder.php');
        
        // execute query
        include(getenv('OB_LIB_DIR').'results_query.php');

        // read and send results
        include_once(getenv('OB_LIB_DIR').'results_builder.php');
        $r = new results_builder(['method'=>$output,'filename'=>$filename]);
        if( $r->results_query() ) {
            if ($r->rq['results_count']) {
                $dl_status = $r->export();
                if (preg_match('/^ready:?(\d+)?$/', $dl_status, $m)) {
                    $print_data_params = ['de_id' => $m[1]];
                    if ($filename=='') $print_data_params['no_file'] = true;
                    $modules->_include('results_as'.strtoupper($output), 'print_data', $print_data_params);
                    unset($_SESSION['show_results']);
                    exit;
                }
            } 
            unset($_SESSION['show_results']);
            exit;
        }
        unset($_SESSION['show_results']);
        exit;
    }
}
# start page
# ezen még finomítani kell esetelg...
if(count($_GET)==0 and $load['load_profile']==0) { 
    $load['load_mainpage'] = 1;
    $load['load_map'] = 1;
    $load['load_mappage'] = 0;
}
/* backward compatibility
 * deault entry point is the mappage
 * */
if ($load['load_mainpage'] and $load['load_map']) {
    if (!defined('MAINPAGE')) {
        $load['load_mainpage'] = 0;
        $load['load_mappage'] = 1;
    }


    if (defined('LOGINPAGE') and isset($_POST['loginenter'])) {
        if (LOGINPAGE=='map') {
            $load['load_mainpage'] = 0;
        }
        else {
            $load['load_mappage'] = 0;
        }
    }
}
# mappage allways use the map
if (isset($load['load_mappage']) and $load['load_mappage']) 
    $load['load_map'] = 1;

$load['load_cookies'] = (isset($_GET['p']) && $_GET['p'] == 'cookies') ? 1 : 0;
$load['load_privacy'] = (isset($_GET['p']) && $_GET['p'] == 'privacy') ? 1 : 0;
$load['load_terms'] = (isset($_GET['p']) && $_GET['p'] == 'terms') ? 1 : 0;
$load['load_technical'] = (isset($_GET['p']) && $_GET['p'] == 'technical') ? 1 : 0;
$load['load_whatsnew'] = (isset($_GET['p']) && $_GET['p'] == 'whatsnew') ? 1 : 0;
$load['load_usage'] = (isset($_GET['p']) && $_GET['p'] == 'usage') ? 1 : 0;
$load['load_useagreement'] = (isset($_GET['p']) && $_GET['p'] == 'datausage') ? 1 : 0;

//debug($load);

if (!defined('STYLE_PATH')) {
    $style_def = constant('STYLE');
    $style_name = $style_def['template'];
    
    if (file_exists(getenv('PROJECT_DIR').'local/styles/app/'.$style_name))
        define('STYLE_PATH',"/local/styles/app/".$style_name);
    else
        define('STYLE_PATH',"/styles/app/".$style_name);
}

$i = 0;
foreach ($load as $key=>$value) {
    $$key = $value;
    /*
     * if ($value!=0 and $value!=-1) {
        $i++;
        if ($i>1) {
            $$key = 0;
            #log_action($$key);
        }
    }*/
}

?>
